package com.kuaishou.locallife.open.api.domain.locallife_third_code;


/**
 * auto generate code
 */
public class MeituanUserInfoApi {
    private String mtaccesstoken;
    private String ksuuid;
    private String fingerprint;
    private String ip;
    private Integer os;
    private String version;

    public String getMtaccesstoken() {
        return mtaccesstoken;
    }

    public void setMtaccesstoken(String mtaccesstoken) {
        this.mtaccesstoken = mtaccesstoken;
    }

    public String getKsuuid() {
        return ksuuid;
    }

    public void setKsuuid(String ksuuid) {
        this.ksuuid = ksuuid;
    }

    public String getFingerprint() {
        return fingerprint;
    }

    public void setFingerprint(String fingerprint) {
        this.fingerprint = fingerprint;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Integer getOs() {
        return os;
    }

    public void setOs(Integer os) {
        this.os = os;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

}
