package com.kuaishou.locallife.open.api.domain.settle_deal;


/**
 * auto generate code
 */
public class OrderBillByCertificateAmount {
    private Long original;
    private Long pay;
    private Long merchant_ticket;
    private Long ticket_amount;

    public Long getOriginal() {
        return original;
    }

    public void setOriginal(Long original) {
        this.original = original;
    }

    public Long getPay() {
        return pay;
    }

    public void setPay(Long pay) {
        this.pay = pay;
    }

    public Long getMerchant_ticket() {
        return merchant_ticket;
    }

    public void setMerchant_ticket(Long merchant_ticket) {
        this.merchant_ticket = merchant_ticket;
    }

    public Long getTicket_amount() {
        return ticket_amount;
    }

    public void setTicket_amount(Long ticket_amount) {
        this.ticket_amount = ticket_amount;
    }

}
