package com.kuaishou.locallife.open.api.request.settle_deal;

import java.util.HashMap;
import java.util.Map;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.response.settle_deal.GoodlifeV1FulfilmentCertificateHistoryResponse;

/**
 * auto generate code
 */
public class GoodlifeV1FulfilmentCertificateHistoryRequest extends AbstractKsLocalLifeRequest<GoodlifeV1FulfilmentCertificateHistoryResponse> {

    private Integer size;
    private Integer cursor;
    private String poi_ids;
    private Long start_time;
    private Long end_time;

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }
    public Integer getCursor() {
        return cursor;
    }

    public void setCursor(Integer cursor) {
        this.cursor = cursor;
    }
    public String getPoi_ids() {
        return poi_ids;
    }

    public void setPoi_ids(String poi_ids) {
        this.poi_ids = poi_ids;
    }
    public Long getStart_time() {
        return start_time;
    }

    public void setStart_time(Long start_time) {
        this.start_time = start_time;
    }
    public Long getEnd_time() {
        return end_time;
    }

    public void setEnd_time(Long end_time) {
        this.end_time = end_time;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        
        bizParams.put("size", this.getSize() == null ? "" : this.getSize().toString());
        
        bizParams.put("cursor", this.getCursor() == null ? "" : this.getCursor().toString());
        bizParams.put("poi_ids", this.getPoi_ids());
        
        
        bizParams.put("start_time", this.getStart_time() == null ? "" : this.getStart_time().toString());
        
        bizParams.put("end_time", this.getEnd_time() == null ? "" : this.getEnd_time().toString());
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private Integer size;
        private Integer cursor;
        private String poi_ids;
        private Long start_time;
        private Long end_time;

        public Integer getSize() {
            return size;
        }

        public void setSize(Integer size) {
            this.size = size;
        }

        public Integer getCursor() {
            return cursor;
        }

        public void setCursor(Integer cursor) {
            this.cursor = cursor;
        }

        public String getPoi_ids() {
            return poi_ids;
        }

        public void setPoi_ids(String poi_ids) {
            this.poi_ids = poi_ids;
        }

        public Long getStart_time() {
            return start_time;
        }

        public void setStart_time(Long start_time) {
            this.start_time = start_time;
        }

        public Long getEnd_time() {
            return end_time;
        }

        public void setEnd_time(Long end_time) {
            this.end_time = end_time;
        }

    }

    @Override
    public String getApiMethodName() {
        return "goodlife.v1.fulfilment.certificate.history";
    }

    @Override
    public Class<GoodlifeV1FulfilmentCertificateHistoryResponse> getResponseClass() {
        return GoodlifeV1FulfilmentCertificateHistoryResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.GET;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/goodlife/v1/fulfilment/certificate/history";
    }
}
