package com.kuaishou.locallife.open.api.domain.locallife_trade;


/**
 * auto generate code
 */
public class Verify {
    private String verify_id;
    private String certificate_id;
    private Integer verify_type;
    private Long verify_time;
    private Boolean can_cancel;
    private String verifier_unique_id;

    public String getVerify_id() {
        return verify_id;
    }

    public void setVerify_id(String verify_id) {
        this.verify_id = verify_id;
    }

    public String getCertificate_id() {
        return certificate_id;
    }

    public void setCertificate_id(String certificate_id) {
        this.certificate_id = certificate_id;
    }

    public Integer getVerify_type() {
        return verify_type;
    }

    public void setVerify_type(Integer verify_type) {
        this.verify_type = verify_type;
    }

    public Long getVerify_time() {
        return verify_time;
    }

    public void setVerify_time(Long verify_time) {
        this.verify_time = verify_time;
    }

    public Boolean getCan_cancel() {
        return can_cancel;
    }

    public void setCan_cancel(Boolean can_cancel) {
        this.can_cancel = can_cancel;
    }

    public String getVerifier_unique_id() {
        return verifier_unique_id;
    }

    public void setVerifier_unique_id(String verifier_unique_id) {
        this.verifier_unique_id = verifier_unique_id;
    }

}
