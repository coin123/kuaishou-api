package com.kuaishou.locallife.open.api.domain.settle_deal;


/**
 * auto generate code
 */
public class OrderBillByCertificateRecord {
    private String cursor;
    private String ledger_id;
    private String verify_id;
    private String certificate_id;
    private String order_id;
    private Long verify_time;
    private Integer status;
    private String code;
    private OrderBillByCertificateAmount amount;
    private OrderBillByCertificateFund fund_amount;
    private OrderBillByCertificateSku sku;

    public String getCursor() {
        return cursor;
    }

    public void setCursor(String cursor) {
        this.cursor = cursor;
    }

    public String getLedger_id() {
        return ledger_id;
    }

    public void setLedger_id(String ledger_id) {
        this.ledger_id = ledger_id;
    }

    public String getVerify_id() {
        return verify_id;
    }

    public void setVerify_id(String verify_id) {
        this.verify_id = verify_id;
    }

    public String getCertificate_id() {
        return certificate_id;
    }

    public void setCertificate_id(String certificate_id) {
        this.certificate_id = certificate_id;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public Long getVerify_time() {
        return verify_time;
    }

    public void setVerify_time(Long verify_time) {
        this.verify_time = verify_time;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public OrderBillByCertificateAmount getAmount() {
        return amount;
    }

    public void setAmount(OrderBillByCertificateAmount amount) {
        this.amount = amount;
    }

    public OrderBillByCertificateFund getFund_amount() {
        return fund_amount;
    }

    public void setFund_amount(OrderBillByCertificateFund fund_amount) {
        this.fund_amount = fund_amount;
    }

    public OrderBillByCertificateSku getSku() {
        return sku;
    }

    public void setSku(OrderBillByCertificateSku sku) {
        this.sku = sku;
    }

}
