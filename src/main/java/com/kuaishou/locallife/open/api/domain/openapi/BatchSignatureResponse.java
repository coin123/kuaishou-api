package com.kuaishou.locallife.open.api.domain.openapi;

import java.util.List;

/**
 * auto generate code
 */
public class BatchSignatureResponse {
    private Integer error_code;
    private String description;
    private List<SignatureInfo> decrypt_infos;

    public Integer getError_code() {
        return error_code;
    }

    public void setError_code(Integer error_code) {
        this.error_code = error_code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<SignatureInfo> getDecrypt_infos() {
        return decrypt_infos;
    }

    public void setDecrypt_infos(List<SignatureInfo> decrypt_infos) {
        this.decrypt_infos = decrypt_infos;
    }

}
