package com.kuaishou.locallife.open.api.response.locallife_shop;


import com.kuaishou.locallife.open.api.domain.locallife_shop.QueryClaimTaskResData;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class GoodlifeV1PoiPoiSyncResponse extends KsLocalLifeResponse {

    private QueryClaimTaskResData data;

    public QueryClaimTaskResData getData() {
        return data;
    }

    public void setData(QueryClaimTaskResData data) {
        this.data = data;
    }

}