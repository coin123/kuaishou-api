package com.kuaishou.locallife.open.api.request.openapi;

import java.util.HashMap;
import java.util.Map;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.response.openapi.IntegrationRefreshQueryMeituanTokenResponse;

/**
 * auto generate code
 */
public class IntegrationRefreshQueryMeituanTokenRequest extends AbstractKsLocalLifeRequest<IntegrationRefreshQueryMeituanTokenResponse> {

    private String encrypt_mobile;
    private String client_id;
    private String encrypt_client_secret;
    private String access_token;
    private String encrypt_refresh_token;
    private String appKey;

    public String getEncrypt_mobile() {
        return encrypt_mobile;
    }

    public void setEncrypt_mobile(String encrypt_mobile) {
        this.encrypt_mobile = encrypt_mobile;
    }
    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }
    public String getEncrypt_client_secret() {
        return encrypt_client_secret;
    }

    public void setEncrypt_client_secret(String encrypt_client_secret) {
        this.encrypt_client_secret = encrypt_client_secret;
    }
    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }
    public String getEncrypt_refresh_token() {
        return encrypt_refresh_token;
    }

    public void setEncrypt_refresh_token(String encrypt_refresh_token) {
        this.encrypt_refresh_token = encrypt_refresh_token;
    }
    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private String encrypt_mobile;
        private String client_id;
        private String encrypt_client_secret;
        private String access_token;
        private String encrypt_refresh_token;
        private String appKey;

        public String getEncrypt_mobile() {
            return encrypt_mobile;
        }

        public void setEncrypt_mobile(String encrypt_mobile) {
            this.encrypt_mobile = encrypt_mobile;
        }

        public String getClient_id() {
            return client_id;
        }

        public void setClient_id(String client_id) {
            this.client_id = client_id;
        }

        public String getEncrypt_client_secret() {
            return encrypt_client_secret;
        }

        public void setEncrypt_client_secret(String encrypt_client_secret) {
            this.encrypt_client_secret = encrypt_client_secret;
        }

        public String getAccess_token() {
            return access_token;
        }

        public void setAccess_token(String access_token) {
            this.access_token = access_token;
        }

        public String getEncrypt_refresh_token() {
            return encrypt_refresh_token;
        }

        public void setEncrypt_refresh_token(String encrypt_refresh_token) {
            this.encrypt_refresh_token = encrypt_refresh_token;
        }

        public String getAppKey() {
            return appKey;
        }

        public void setAppKey(String appKey) {
            this.appKey = appKey;
        }

    }

    @Override
    public String getApiMethodName() {
        return "integration.refresh.query.meituan.token";
    }

    @Override
    public Class<IntegrationRefreshQueryMeituanTokenResponse> getResponseClass() {
        return IntegrationRefreshQueryMeituanTokenResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.POST;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/integration/refresh/query/meituan/token";
    }
}
