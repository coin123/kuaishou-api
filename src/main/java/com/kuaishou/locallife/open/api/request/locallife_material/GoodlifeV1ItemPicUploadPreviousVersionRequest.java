package com.kuaishou.locallife.open.api.request.locallife_material;

import java.util.HashMap;
import java.util.Map;

import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.response.locallife_material.GoodlifeV1ItemPicUploadPreviousVersionResponse;

/**
 * @author gaojiapei <gaojiapei@kuaishou.com>
 * Created on 2023-04-14
 * 兼容之前手动配置的版本
 */
public class GoodlifeV1ItemPicUploadPreviousVersionRequest extends AbstractKsLocalLifeRequest<GoodlifeV1ItemPicUploadPreviousVersionResponse> {
    private String third_img_byte;

    public String getThird_img_byte() {
        return third_img_byte;
    }

    public void setThird_img_byte(String third_img_byte) {
        this.third_img_byte = third_img_byte;
    }

    public GoodlifeV1ItemPicUploadPreviousVersionRequest() {
    }

    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap();
        bizParams.put("third_imgae_byte", this.getThird_img_byte());
        return bizParams;
    }

    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public String getApiMethodName() {
        return "goodlife.v1.item.pic.upload";
    }

    public Class<GoodlifeV1ItemPicUploadPreviousVersionResponse> getResponseClass() {
        return GoodlifeV1ItemPicUploadPreviousVersionResponse.class;
    }

    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.POST;
    }

    public String getRequestSpecialPath() {
        return "/goodlife/v1/item/pic/upload";
    }

    public static class ParamDTO {
        private String third_img_byte;

        public ParamDTO() {
        }

        public String getThird_img_byte() {
            return third_img_byte;
        }

        public void setThird_img_byte(String third_img_byte) {
            this.third_img_byte = third_img_byte;
        }
    }
}
