package com.kuaishou.locallife.open.api.domain.locallife_third_code;


/**
 * auto generate code
 */
public class GrantsVoucherCodeInfo {
    private String code;
    private String code_redirect;
    private VoucherPriceDetail price_detail;
    private ShareDiscountCost share_discount_cost;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode_redirect() {
        return code_redirect;
    }

    public void setCode_redirect(String code_redirect) {
        this.code_redirect = code_redirect;
    }

    public VoucherPriceDetail getPrice_detail() {
        return price_detail;
    }

    public void setPrice_detail(VoucherPriceDetail price_detail) {
        this.price_detail = price_detail;
    }

    public ShareDiscountCost getShare_discount_cost() {
        return share_discount_cost;
    }

    public void setShare_discount_cost(ShareDiscountCost share_discount_cost) {
        this.share_discount_cost = share_discount_cost;
    }

}
