package com.kuaishou.locallife.open.api.domain.open_message;


/**
 * auto generate code
 */
public class MessageRespDataTest {
    private Integer errorCode;
    private String description;
    private String error_code;

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getError_code() {
        return error_code;
    }

    public void setError_code(String error_code) {
        this.error_code = error_code;
    }

}
