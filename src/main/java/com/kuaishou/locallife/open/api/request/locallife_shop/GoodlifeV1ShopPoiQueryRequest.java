package com.kuaishou.locallife.open.api.request.locallife_shop;

import java.util.HashMap;
import java.util.Map;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.response.locallife_shop.GoodlifeV1ShopPoiQueryResponse;

/**
 * auto generate code
 */
public class GoodlifeV1ShopPoiQueryRequest extends AbstractKsLocalLifeRequest<GoodlifeV1ShopPoiQueryResponse> {

    private String third_id;
    private Long account_id;
    private Integer size;
    private Integer page;
    private Long poi_id;

    public String getThird_id() {
        return third_id;
    }

    public void setThird_id(String third_id) {
        this.third_id = third_id;
    }
    public Long getAccount_id() {
        return account_id;
    }

    public void setAccount_id(Long account_id) {
        this.account_id = account_id;
    }
    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }
    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }
    public Long getPoi_id() {
        return poi_id;
    }

    public void setPoi_id(Long poi_id) {
        this.poi_id = poi_id;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        bizParams.put("third_id", this.getThird_id());
        
        
        bizParams.put("account_id", this.getAccount_id() == null ? "" : this.getAccount_id().toString());
        
        bizParams.put("size", this.getSize() == null ? "" : this.getSize().toString());
        
        bizParams.put("page", this.getPage() == null ? "" : this.getPage().toString());
        
        bizParams.put("poi_id", this.getPoi_id() == null ? "" : this.getPoi_id().toString());
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private String third_id;
        private Long account_id;
        private Integer size;
        private Integer page;
        private Long poi_id;

        public String getThird_id() {
            return third_id;
        }

        public void setThird_id(String third_id) {
            this.third_id = third_id;
        }

        public Long getAccount_id() {
            return account_id;
        }

        public void setAccount_id(Long account_id) {
            this.account_id = account_id;
        }

        public Integer getSize() {
            return size;
        }

        public void setSize(Integer size) {
            this.size = size;
        }

        public Integer getPage() {
            return page;
        }

        public void setPage(Integer page) {
            this.page = page;
        }

        public Long getPoi_id() {
            return poi_id;
        }

        public void setPoi_id(Long poi_id) {
            this.poi_id = poi_id;
        }

    }

    @Override
    public String getApiMethodName() {
        return "goodlife.v1.shop.poi.query";
    }

    @Override
    public Class<GoodlifeV1ShopPoiQueryResponse> getResponseClass() {
        return GoodlifeV1ShopPoiQueryResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.GET;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/goodlife/v1/shop/poi/query";
    }
}
