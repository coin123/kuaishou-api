package com.kuaishou.locallife.open.api.request.locallife_trade;

import java.util.HashMap;
import java.util.Map;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.response.locallife_trade.GoodlifeV1FulfilmentCertificateGetResponse;

/**
 * auto generate code
 */
public class GoodlifeV1FulfilmentCertificateGetRequest extends AbstractKsLocalLifeRequest<GoodlifeV1FulfilmentCertificateGetResponse> {

    private String encrypted_code;

    public String getEncrypted_code() {
        return encrypted_code;
    }

    public void setEncrypted_code(String encrypted_code) {
        this.encrypted_code = encrypted_code;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        bizParams.put("encrypted_code", this.getEncrypted_code());
        
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private String encrypted_code;

        public String getEncrypted_code() {
            return encrypted_code;
        }

        public void setEncrypted_code(String encrypted_code) {
            this.encrypted_code = encrypted_code;
        }

    }

    @Override
    public String getApiMethodName() {
        return "goodlife.v1.fulfilment.certificate.get";
    }

    @Override
    public Class<GoodlifeV1FulfilmentCertificateGetResponse> getResponseClass() {
        return GoodlifeV1FulfilmentCertificateGetResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.GET;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/goodlife/v1/fulfilment/certificate/get";
    }
}
