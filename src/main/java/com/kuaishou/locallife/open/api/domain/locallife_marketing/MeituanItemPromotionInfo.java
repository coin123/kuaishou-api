package com.kuaishou.locallife.open.api.domain.locallife_marketing;


/**
 * auto generate code
 */
public class MeituanItemPromotionInfo {
    private MeituanItemCampaignInfo[] campaign_list;
    private MeituanItemCardInfo[] card_list;

    public MeituanItemCampaignInfo[] getCampaign_list() {
        return campaign_list;
    }

    public void setCampaign_list(MeituanItemCampaignInfo[] campaign_list) {
        this.campaign_list = campaign_list;
    }

    public MeituanItemCardInfo[] getCard_list() {
        return card_list;
    }

    public void setCard_list(MeituanItemCardInfo[] card_list) {
        this.card_list = card_list;
    }

}
