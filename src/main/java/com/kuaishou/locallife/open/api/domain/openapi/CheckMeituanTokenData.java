package com.kuaishou.locallife.open.api.domain.openapi;


/**
 * auto generate code
 */
public class CheckMeituanTokenData {
    private Integer error_code;
    private String description;
    private Integer logintype;
    private Boolean register_type;

    public Integer getError_code() {
        return error_code;
    }

    public void setError_code(Integer error_code) {
        this.error_code = error_code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getLogintype() {
        return logintype;
    }

    public void setLogintype(Integer logintype) {
        this.logintype = logintype;
    }

    public Boolean getRegister_type() {
        return register_type;
    }

    public void setRegister_type(Boolean register_type) {
        this.register_type = register_type;
    }

}
