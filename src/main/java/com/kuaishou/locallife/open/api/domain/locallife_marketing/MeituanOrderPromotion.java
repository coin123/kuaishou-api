package com.kuaishou.locallife.open.api.domain.locallife_marketing;


/**
 * auto generate code
 */
public class MeituanOrderPromotion {
    private MeituanOrderPromotionInfo[] promotions;

    public MeituanOrderPromotionInfo[] getPromotions() {
        return promotions;
    }

    public void setPromotions(MeituanOrderPromotionInfo[] promotions) {
        this.promotions = promotions;
    }

}
