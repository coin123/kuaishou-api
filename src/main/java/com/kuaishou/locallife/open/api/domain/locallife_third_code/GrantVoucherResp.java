package com.kuaishou.locallife.open.api.domain.locallife_third_code;


/**
 * auto generate code
 */
public class GrantVoucherResp {
    private Long errorCode;
    private String description;
    private Integer result;
    private String[] codes;
    private GrantVouchersCodeInfo[] codesList;
    private Long error_code;

    public Long getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Long errorCode) {
        this.errorCode = errorCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }

    public String[] getCodes() {
        return codes;
    }

    public void setCodes(String[] codes) {
        this.codes = codes;
    }

    public GrantVouchersCodeInfo[] getCodesList() {
        return codesList;
    }

    public void setCodesList(GrantVouchersCodeInfo[] codesList) {
        this.codesList = codesList;
    }

    public Long getError_code() {
        return error_code;
    }

    public void setError_code(Long error_code) {
        this.error_code = error_code;
    }

}
