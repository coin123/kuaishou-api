package com.kuaishou.locallife.open.api.domain.locallife_distribution;


/**
 * auto generate code
 */
public class ErrorDistributor {
    private Long distributor;
    private String distributor_kwai_id;
    private String msg;

    public Long getDistributor() {
        return distributor;
    }

    public void setDistributor(Long distributor) {
        this.distributor = distributor;
    }

    public String getDistributor_kwai_id() {
        return distributor_kwai_id;
    }

    public void setDistributor_kwai_id(String distributor_kwai_id) {
        this.distributor_kwai_id = distributor_kwai_id;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}
