package com.kuaishou.locallife.open.api.domain.settle_deal;


/**
 * auto generate code
 */
public class OpenApiBillQueryledgerOrderAttribute {
    private String source;
    private String video_id;
    private String room_id;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getVideo_id() {
        return video_id;
    }

    public void setVideo_id(String video_id) {
        this.video_id = video_id;
    }

    public String getRoom_id() {
        return room_id;
    }

    public void setRoom_id(String room_id) {
        this.room_id = room_id;
    }

}
