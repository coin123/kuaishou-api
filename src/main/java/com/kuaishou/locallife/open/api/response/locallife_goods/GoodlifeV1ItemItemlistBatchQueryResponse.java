package com.kuaishou.locallife.open.api.response.locallife_goods;


import com.kuaishou.locallife.open.api.domain.locallife_goods.CategoryDetailResponse;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class GoodlifeV1ItemItemlistBatchQueryResponse extends KsLocalLifeResponse {

    private CategoryDetailResponse data;

    public CategoryDetailResponse getData() {
        return data;
    }

    public void setData(CategoryDetailResponse data) {
        this.data = data;
    }

}