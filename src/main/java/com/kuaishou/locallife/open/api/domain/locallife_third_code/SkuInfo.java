package com.kuaishou.locallife.open.api.domain.locallife_third_code;


/**
 * auto generate code
 */
public class SkuInfo {
    private String skuName;
    private Long skuId;
    private String thirdSkuId;
    private Long itemId;
    private String outId;
    private String outSkuId;
    private Integer grouponType;
    private Integer timesCount;

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public String getThirdSkuId() {
        return thirdSkuId;
    }

    public void setThirdSkuId(String thirdSkuId) {
        this.thirdSkuId = thirdSkuId;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public String getOutId() {
        return outId;
    }

    public void setOutId(String outId) {
        this.outId = outId;
    }

    public String getOutSkuId() {
        return outSkuId;
    }

    public void setOutSkuId(String outSkuId) {
        this.outSkuId = outSkuId;
    }

    public Integer getGrouponType() {
        return grouponType;
    }

    public void setGrouponType(Integer grouponType) {
        this.grouponType = grouponType;
    }

    public Integer getTimesCount() {
        return timesCount;
    }

    public void setTimesCount(Integer timesCount) {
        this.timesCount = timesCount;
    }

}
