package com.kuaishou.locallife.open.api.common;

/**
 * http request method
 *
 * @author shuxiaohui <shuxiaohui@kuaishou.com>
 * Created on 2020-03-31
 */
public enum HttpRequestMethod {
    POST,
    GET,
    PUT,
    DELETE
}
