package com.kuaishou.locallife.open.api.response.locallife_shop;


import com.kuaishou.locallife.open.api.domain.locallife_shop.QueryTaskResData;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class GoodlifeV1PoiMatchTaskQueryResponse extends KsLocalLifeResponse {

    private QueryTaskResData data;

    public QueryTaskResData getData() {
        return data;
    }

    public void setData(QueryTaskResData data) {
        this.data = data;
    }

}