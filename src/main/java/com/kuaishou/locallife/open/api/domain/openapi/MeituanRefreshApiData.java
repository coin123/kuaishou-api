package com.kuaishou.locallife.open.api.domain.openapi;


/**
 * auto generate code
 */
public class MeituanRefreshApiData {
    private Integer error_code;
    private String description;
    private Integer login_type;
    private Boolean register_type;
    private LoginApiRespData login_data;

    public Integer getError_code() {
        return error_code;
    }

    public void setError_code(Integer error_code) {
        this.error_code = error_code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getLogin_type() {
        return login_type;
    }

    public void setLogin_type(Integer login_type) {
        this.login_type = login_type;
    }

    public Boolean getRegister_type() {
        return register_type;
    }

    public void setRegister_type(Boolean register_type) {
        this.register_type = register_type;
    }

    public LoginApiRespData getLogin_data() {
        return login_data;
    }

    public void setLogin_data(LoginApiRespData login_data) {
        this.login_data = login_data;
    }

}
