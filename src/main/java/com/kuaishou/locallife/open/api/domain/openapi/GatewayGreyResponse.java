package com.kuaishou.locallife.open.api.domain.openapi;


/**
 * auto generate code
 */
public class GatewayGreyResponse {
    private Long errorCode;
    private String message;

    public Long getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Long errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
