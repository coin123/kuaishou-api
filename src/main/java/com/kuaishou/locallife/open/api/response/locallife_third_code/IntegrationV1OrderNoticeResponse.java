package com.kuaishou.locallife.open.api.response.locallife_third_code;


import com.kuaishou.locallife.open.api.domain.locallife_third_code.OrderNoticeResp;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class IntegrationV1OrderNoticeResponse extends KsLocalLifeResponse {

    private OrderNoticeResp data;

    public OrderNoticeResp getData() {
        return data;
    }

    public void setData(OrderNoticeResp data) {
        this.data = data;
    }

}