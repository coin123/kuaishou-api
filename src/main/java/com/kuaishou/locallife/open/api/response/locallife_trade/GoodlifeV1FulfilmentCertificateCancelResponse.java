package com.kuaishou.locallife.open.api.response.locallife_trade;


import com.kuaishou.locallife.open.api.domain.locallife_trade.TradeOpenApiCancelVerifyResp;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class GoodlifeV1FulfilmentCertificateCancelResponse extends KsLocalLifeResponse {

    private TradeOpenApiCancelVerifyResp data;

    public TradeOpenApiCancelVerifyResp getData() {
        return data;
    }

    public void setData(TradeOpenApiCancelVerifyResp data) {
        this.data = data;
    }

}