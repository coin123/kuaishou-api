package com.kuaishou.locallife.open.api.response.locallife_third_code;


import com.kuaishou.locallife.open.api.domain.locallife_third_code.RefundResponseData;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class IntegrationNamekFulfilmentRefundApplyResponse extends KsLocalLifeResponse {

    private RefundResponseData data;

    public RefundResponseData getData() {
        return data;
    }

    public void setData(RefundResponseData data) {
        this.data = data;
    }

}