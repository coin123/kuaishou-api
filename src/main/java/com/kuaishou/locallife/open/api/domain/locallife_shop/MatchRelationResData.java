package com.kuaishou.locallife.open.api.domain.locallife_shop;

import java.util.List;

/**
 * auto generate code
 */
public class MatchRelationResData {
    private String description;
    private Integer error_code;
    private List<MatchRelationData> relations;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getError_code() {
        return error_code;
    }

    public void setError_code(Integer error_code) {
        this.error_code = error_code;
    }

    public List<MatchRelationData> getRelations() {
        return relations;
    }

    public void setRelations(List<MatchRelationData> relations) {
        this.relations = relations;
    }

}
