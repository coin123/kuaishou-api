package com.kuaishou.locallife.open.api.domain.locallife_third_code;


/**
 * auto generate code
 */
public class Data {
    private Long error_code;
    private String description;
    private Integer result;
    private Integer fail_code;
    private String other_fail_msg;

    public Long getError_code() {
        return error_code;
    }

    public void setError_code(Long error_code) {
        this.error_code = error_code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }

    public Integer getFail_code() {
        return fail_code;
    }

    public void setFail_code(Integer fail_code) {
        this.fail_code = fail_code;
    }

    public String getOther_fail_msg() {
        return other_fail_msg;
    }

    public void setOther_fail_msg(String other_fail_msg) {
        this.other_fail_msg = other_fail_msg;
    }

}
