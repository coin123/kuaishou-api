package com.kuaishou.locallife.open.api.response.locallife_shop;


import com.kuaishou.locallife.open.api.domain.locallife_shop.PoiStockResponseData;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class GoodlifeShopPoiStockResponse extends KsLocalLifeResponse {

    private PoiStockResponseData data;

    public PoiStockResponseData getData() {
        return data;
    }

    public void setData(PoiStockResponseData data) {
        this.data = data;
    }

}