package com.kuaishou.locallife.open.api.request.locallife_goods;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.domain.locallife_goods.ProductStruct;
import com.kuaishou.locallife.open.api.domain.locallife_goods.SkuStruct;
import com.kuaishou.locallife.open.api.response.locallife_goods.GoodlifeV1ItemSaveResponse;

/**
 * auto generate code
 */
public class GoodlifeV1ItemSaveRequest extends AbstractKsLocalLifeRequest<GoodlifeV1ItemSaveResponse> {

    private ProductStruct product;
    private List<SkuStruct> sku_list;

    public ProductStruct getProduct() {
        return product;
    }

    public void setProduct(ProductStruct product) {
        this.product = product;
    }
    public List<SkuStruct> getSku_list() {
        return sku_list;
    }

    public void setSku_list(List<SkuStruct> sku_list) {
        this.sku_list = sku_list;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private ProductStruct product;
        private List<SkuStruct> sku_list;

        public ProductStruct getProduct() {
            return product;
        }

        public void setProduct(ProductStruct product) {
            this.product = product;
        }

        public List<SkuStruct> getSku_list() {
            return sku_list;
        }

        public void setSku_list(List<SkuStruct> sku_list) {
            this.sku_list = sku_list;
        }

    }

    @Override
    public String getApiMethodName() {
        return "goodlife.v1.item.save";
    }

    @Override
    public Class<GoodlifeV1ItemSaveResponse> getResponseClass() {
        return GoodlifeV1ItemSaveResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.POST;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/goodlife/v1/item/save";
    }
}
