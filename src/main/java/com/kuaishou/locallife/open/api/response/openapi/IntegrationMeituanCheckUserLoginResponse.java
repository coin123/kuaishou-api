package com.kuaishou.locallife.open.api.response.openapi;


import com.kuaishou.locallife.open.api.domain.openapi.CheckMeituanTokenData;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class IntegrationMeituanCheckUserLoginResponse extends KsLocalLifeResponse {

    private CheckMeituanTokenData data;

    public CheckMeituanTokenData getData() {
        return data;
    }

    public void setData(CheckMeituanTokenData data) {
        this.data = data;
    }

}