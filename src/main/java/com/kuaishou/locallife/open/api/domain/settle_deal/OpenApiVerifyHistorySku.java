package com.kuaishou.locallife.open.api.domain.settle_deal;


/**
 * auto generate code
 */
public class OpenApiVerifyHistorySku {
    private String sku_id;
    private String title;
    private Integer groupon_type;
    private Long market_price;
    private Long sold_start_time;
    private String third_sku_id;

    public String getSku_id() {
        return sku_id;
    }

    public void setSku_id(String sku_id) {
        this.sku_id = sku_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getGroupon_type() {
        return groupon_type;
    }

    public void setGroupon_type(Integer groupon_type) {
        this.groupon_type = groupon_type;
    }

    public Long getMarket_price() {
        return market_price;
    }

    public void setMarket_price(Long market_price) {
        this.market_price = market_price;
    }

    public Long getSold_start_time() {
        return sold_start_time;
    }

    public void setSold_start_time(Long sold_start_time) {
        this.sold_start_time = sold_start_time;
    }

    public String getThird_sku_id() {
        return third_sku_id;
    }

    public void setThird_sku_id(String third_sku_id) {
        this.third_sku_id = third_sku_id;
    }

}
