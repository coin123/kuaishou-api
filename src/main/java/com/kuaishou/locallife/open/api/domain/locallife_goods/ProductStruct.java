package com.kuaishou.locallife.open.api.domain.locallife_goods;

import java.util.Map;
import java.util.List;

/**
 * auto generate code
 */
public class ProductStruct {
    private Integer product_type;
    private Integer category_id;
    private String product_id;
    private String out_id;
    private String product_name;
    private List<String> pois;
    private Map attr_key_value_map;
    private String change_text;
    private String account_name;
    private Long category_leaf_id;
    private String category_full_name;
    private Long item_start_time;
    private Long item_end_time;
    private List<String> image_list;
    private String on_shelf_time;
    private String off_shelf_time;
    private Boolean expire_auto_delay;

    public Integer getProduct_type() {
        return product_type;
    }

    public void setProduct_type(Integer product_type) {
        this.product_type = product_type;
    }

    public Integer getCategory_id() {
        return category_id;
    }

    public void setCategory_id(Integer category_id) {
        this.category_id = category_id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getOut_id() {
        return out_id;
    }

    public void setOut_id(String out_id) {
        this.out_id = out_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public List<String> getPois() {
        return pois;
    }

    public void setPois(List<String> pois) {
        this.pois = pois;
    }

    public Map getAttr_key_value_map() {
        return attr_key_value_map;
    }

    public void setAttr_key_value_map(Map attr_key_value_map) {
        this.attr_key_value_map = attr_key_value_map;
    }

    public String getChange_text() {
        return change_text;
    }

    public void setChange_text(String change_text) {
        this.change_text = change_text;
    }

    public String getAccount_name() {
        return account_name;
    }

    public void setAccount_name(String account_name) {
        this.account_name = account_name;
    }

    public Long getCategory_leaf_id() {
        return category_leaf_id;
    }

    public void setCategory_leaf_id(Long category_leaf_id) {
        this.category_leaf_id = category_leaf_id;
    }

    public String getCategory_full_name() {
        return category_full_name;
    }

    public void setCategory_full_name(String category_full_name) {
        this.category_full_name = category_full_name;
    }

    public Long getItem_start_time() {
        return item_start_time;
    }

    public void setItem_start_time(Long item_start_time) {
        this.item_start_time = item_start_time;
    }

    public Long getItem_end_time() {
        return item_end_time;
    }

    public void setItem_end_time(Long item_end_time) {
        this.item_end_time = item_end_time;
    }

    public List<String> getImage_list() {
        return image_list;
    }

    public void setImage_list(List<String> image_list) {
        this.image_list = image_list;
    }

    public String getOn_shelf_time() {
        return on_shelf_time;
    }

    public void setOn_shelf_time(String on_shelf_time) {
        this.on_shelf_time = on_shelf_time;
    }

    public String getOff_shelf_time() {
        return off_shelf_time;
    }

    public void setOff_shelf_time(String off_shelf_time) {
        this.off_shelf_time = off_shelf_time;
    }

    public Boolean getExpire_auto_delay() {
        return expire_auto_delay;
    }

    public void setExpire_auto_delay(Boolean expire_auto_delay) {
        this.expire_auto_delay = expire_auto_delay;
    }

}
