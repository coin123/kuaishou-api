package com.kuaishou.locallife.open.api.domain.locallife_trade;

import java.util.List;

/**
 * auto generate code
 */
public class CodeWithTime {
    private String code;
    private Long verify_time;
    private List<String> verify_sign_list;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getVerify_time() {
        return verify_time;
    }

    public void setVerify_time(Long verify_time) {
        this.verify_time = verify_time;
    }

    public List<String> getVerify_sign_list() {
        return verify_sign_list;
    }

    public void setVerify_sign_list(List<String> verify_sign_list) {
        this.verify_sign_list = verify_sign_list;
    }

}
