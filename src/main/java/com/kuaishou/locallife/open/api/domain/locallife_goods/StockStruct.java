package com.kuaishou.locallife.open.api.domain.locallife_goods;


/**
 * auto generate code
 */
public class StockStruct {
    private Integer limit_type;
    private Integer stock;

    public Integer getLimit_type() {
        return limit_type;
    }

    public void setLimit_type(Integer limit_type) {
        this.limit_type = limit_type;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

}
