package com.kuaishou.locallife.open.api.response.settle_deal;


import com.kuaishou.locallife.open.api.domain.settle_deal.OrderBillByCertificateStruct;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class GoodlifeV1BillQueryByCertificateResponse extends KsLocalLifeResponse {

    private OrderBillByCertificateStruct data;

    public OrderBillByCertificateStruct getData() {
        return data;
    }

    public void setData(OrderBillByCertificateStruct data) {
        this.data = data;
    }

}