package com.kuaishou.locallife.open.api.domain.locallife_third_code;


/**
 * auto generate code
 */
public class RefundAuditBatchRespData {
    private Long error_code;
    private String description;

    public Long getError_code() {
        return error_code;
    }

    public void setError_code(Long error_code) {
        this.error_code = error_code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
