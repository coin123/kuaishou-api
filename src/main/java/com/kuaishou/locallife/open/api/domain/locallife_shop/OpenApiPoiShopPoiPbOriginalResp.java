package com.kuaishou.locallife.open.api.domain.locallife_shop;

import java.util.List;

/**
 * auto generate code
 */
public class OpenApiPoiShopPoiPbOriginalResp {
    private Integer total;
    private String biz_msg;
    private Integer biz_code;
    private List<Poi> pois;

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public String getBiz_msg() {
        return biz_msg;
    }

    public void setBiz_msg(String biz_msg) {
        this.biz_msg = biz_msg;
    }

    public Integer getBiz_code() {
        return biz_code;
    }

    public void setBiz_code(Integer biz_code) {
        this.biz_code = biz_code;
    }

    public List<Poi> getPois() {
        return pois;
    }

    public void setPois(List<Poi> pois) {
        this.pois = pois;
    }

}
