package com.kuaishou.locallife.open.api.request.locallife_shop;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.response.locallife_shop.GoodlifeCapacityPoiMergeResponse;

/**
 * auto generate code
 */
public class GoodlifeCapacityPoiMergeRequest extends AbstractKsLocalLifeRequest<GoodlifeCapacityPoiMergeResponse> {

    private String poi_address;
    private String poi_phone;
    private String saas_app_id;
    private String city;
    private List<String> poi_shop_labels;
    private Integer source;
    private String lng_lat;
    private String poi_pic_url;
    private String poi_name;
    private String province;
    private String district;
    private String poi_opening_time;
    private String poi_type;
    private String ext_poi_id;

    public String getPoi_address() {
        return poi_address;
    }

    public void setPoi_address(String poi_address) {
        this.poi_address = poi_address;
    }
    public String getPoi_phone() {
        return poi_phone;
    }

    public void setPoi_phone(String poi_phone) {
        this.poi_phone = poi_phone;
    }
    public String getSaas_app_id() {
        return saas_app_id;
    }

    public void setSaas_app_id(String saas_app_id) {
        this.saas_app_id = saas_app_id;
    }
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
    public List<String> getPoi_shop_labels() {
        return poi_shop_labels;
    }

    public void setPoi_shop_labels(List<String> poi_shop_labels) {
        this.poi_shop_labels = poi_shop_labels;
    }
    public Integer getSource() {
        return source;
    }

    public void setSource(Integer source) {
        this.source = source;
    }
    public String getLng_lat() {
        return lng_lat;
    }

    public void setLng_lat(String lng_lat) {
        this.lng_lat = lng_lat;
    }
    public String getPoi_pic_url() {
        return poi_pic_url;
    }

    public void setPoi_pic_url(String poi_pic_url) {
        this.poi_pic_url = poi_pic_url;
    }
    public String getPoi_name() {
        return poi_name;
    }

    public void setPoi_name(String poi_name) {
        this.poi_name = poi_name;
    }
    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }
    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }
    public String getPoi_opening_time() {
        return poi_opening_time;
    }

    public void setPoi_opening_time(String poi_opening_time) {
        this.poi_opening_time = poi_opening_time;
    }
    public String getPoi_type() {
        return poi_type;
    }

    public void setPoi_type(String poi_type) {
        this.poi_type = poi_type;
    }
    public String getExt_poi_id() {
        return ext_poi_id;
    }

    public void setExt_poi_id(String ext_poi_id) {
        this.ext_poi_id = ext_poi_id;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private String poi_address;
        private String poi_phone;
        private String saas_app_id;
        private String city;
        private List<String> poi_shop_labels;
        private Integer source;
        private String lng_lat;
        private String poi_pic_url;
        private String poi_name;
        private String province;
        private String district;
        private String poi_opening_time;
        private String poi_type;
        private String ext_poi_id;

        public String getPoi_address() {
            return poi_address;
        }

        public void setPoi_address(String poi_address) {
            this.poi_address = poi_address;
        }

        public String getPoi_phone() {
            return poi_phone;
        }

        public void setPoi_phone(String poi_phone) {
            this.poi_phone = poi_phone;
        }

        public String getSaas_app_id() {
            return saas_app_id;
        }

        public void setSaas_app_id(String saas_app_id) {
            this.saas_app_id = saas_app_id;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public List<String> getPoi_shop_labels() {
            return poi_shop_labels;
        }

        public void setPoi_shop_labels(List<String> poi_shop_labels) {
            this.poi_shop_labels = poi_shop_labels;
        }

        public Integer getSource() {
            return source;
        }

        public void setSource(Integer source) {
            this.source = source;
        }

        public String getLng_lat() {
            return lng_lat;
        }

        public void setLng_lat(String lng_lat) {
            this.lng_lat = lng_lat;
        }

        public String getPoi_pic_url() {
            return poi_pic_url;
        }

        public void setPoi_pic_url(String poi_pic_url) {
            this.poi_pic_url = poi_pic_url;
        }

        public String getPoi_name() {
            return poi_name;
        }

        public void setPoi_name(String poi_name) {
            this.poi_name = poi_name;
        }

        public String getProvince() {
            return province;
        }

        public void setProvince(String province) {
            this.province = province;
        }

        public String getDistrict() {
            return district;
        }

        public void setDistrict(String district) {
            this.district = district;
        }

        public String getPoi_opening_time() {
            return poi_opening_time;
        }

        public void setPoi_opening_time(String poi_opening_time) {
            this.poi_opening_time = poi_opening_time;
        }

        public String getPoi_type() {
            return poi_type;
        }

        public void setPoi_type(String poi_type) {
            this.poi_type = poi_type;
        }

        public String getExt_poi_id() {
            return ext_poi_id;
        }

        public void setExt_poi_id(String ext_poi_id) {
            this.ext_poi_id = ext_poi_id;
        }

    }

    @Override
    public String getApiMethodName() {
        return "goodlife.capacity.poi.merge";
    }

    @Override
    public Class<GoodlifeCapacityPoiMergeResponse> getResponseClass() {
        return GoodlifeCapacityPoiMergeResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.POST;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/goodlife/capacity/poi/merge";
    }
}
