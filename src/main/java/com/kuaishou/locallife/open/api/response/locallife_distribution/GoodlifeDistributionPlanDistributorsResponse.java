package com.kuaishou.locallife.open.api.response.locallife_distribution;


import com.kuaishou.locallife.open.api.domain.locallife_distribution.DistributorsData;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class GoodlifeDistributionPlanDistributorsResponse extends KsLocalLifeResponse {

    private DistributorsData data;

    public DistributorsData getData() {
        return data;
    }

    public void setData(DistributorsData data) {
        this.data = data;
    }

}