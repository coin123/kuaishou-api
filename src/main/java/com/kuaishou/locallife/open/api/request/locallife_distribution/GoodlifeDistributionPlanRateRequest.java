package com.kuaishou.locallife.open.api.request.locallife_distribution;

import java.util.HashMap;
import java.util.Map;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.response.locallife_distribution.GoodlifeDistributionPlanRateResponse;

/**
 * auto generate code
 */
public class GoodlifeDistributionPlanRateRequest extends AbstractKsLocalLifeRequest<GoodlifeDistributionPlanRateResponse> {

    private Integer plan_type;
    private Long plan_id;
    private Integer distributor_rate;

    public Integer getPlan_type() {
        return plan_type;
    }

    public void setPlan_type(Integer plan_type) {
        this.plan_type = plan_type;
    }
    public Long getPlan_id() {
        return plan_id;
    }

    public void setPlan_id(Long plan_id) {
        this.plan_id = plan_id;
    }
    public Integer getDistributor_rate() {
        return distributor_rate;
    }

    public void setDistributor_rate(Integer distributor_rate) {
        this.distributor_rate = distributor_rate;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private Integer plan_type;
        private Long plan_id;
        private Integer distributor_rate;

        public Integer getPlan_type() {
            return plan_type;
        }

        public void setPlan_type(Integer plan_type) {
            this.plan_type = plan_type;
        }

        public Long getPlan_id() {
            return plan_id;
        }

        public void setPlan_id(Long plan_id) {
            this.plan_id = plan_id;
        }

        public Integer getDistributor_rate() {
            return distributor_rate;
        }

        public void setDistributor_rate(Integer distributor_rate) {
            this.distributor_rate = distributor_rate;
        }

    }

    @Override
    public String getApiMethodName() {
        return "goodlife.distribution.plan.rate";
    }

    @Override
    public Class<GoodlifeDistributionPlanRateResponse> getResponseClass() {
        return GoodlifeDistributionPlanRateResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.POST;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/goodlife/distribution/plan/rate";
    }
}
