package com.kuaishou.locallife.open.api.domain.openapi;


/**
 * auto generate code
 */
public class PlainTextInfo {
    private String plain_text;
    private Integer encrypt_type;

    public String getPlain_text() {
        return plain_text;
    }

    public void setPlain_text(String plain_text) {
        this.plain_text = plain_text;
    }

    public Integer getEncrypt_type() {
        return encrypt_type;
    }

    public void setEncrypt_type(Integer encrypt_type) {
        this.encrypt_type = encrypt_type;
    }

}
