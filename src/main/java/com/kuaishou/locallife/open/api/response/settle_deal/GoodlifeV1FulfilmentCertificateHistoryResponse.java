package com.kuaishou.locallife.open.api.response.settle_deal;


import com.kuaishou.locallife.open.api.domain.settle_deal.OpenApiVerifyHistoryData;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class GoodlifeV1FulfilmentCertificateHistoryResponse extends KsLocalLifeResponse {

    private OpenApiVerifyHistoryData data;

    public OpenApiVerifyHistoryData getData() {
        return data;
    }

    public void setData(OpenApiVerifyHistoryData data) {
        this.data = data;
    }

}