package com.kuaishou.locallife.open.api.request.ksoptest;

import java.util.HashMap;
import java.util.Map;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.response.ksoptest.GoodlifeTestPublishGjpResponse;

/**
 * auto generate code
 */
public class GoodlifeTestPublishGjpRequest extends AbstractKsLocalLifeRequest<GoodlifeTestPublishGjpResponse> {

    private String appKey;
    private Long out_id;

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }
    public Long getOut_id() {
        return out_id;
    }

    public void setOut_id(Long out_id) {
        this.out_id = out_id;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        bizParams.put("appKey", this.getAppKey());
        
        
        bizParams.put("out_id", this.getOut_id() == null ? "" : this.getOut_id().toString());
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private String appKey;
        private Long out_id;

        public String getAppKey() {
            return appKey;
        }

        public void setAppKey(String appKey) {
            this.appKey = appKey;
        }

        public Long getOut_id() {
            return out_id;
        }

        public void setOut_id(Long out_id) {
            this.out_id = out_id;
        }

    }

    @Override
    public String getApiMethodName() {
        return "goodlife.test.publish.gjp";
    }

    @Override
    public Class<GoodlifeTestPublishGjpResponse> getResponseClass() {
        return GoodlifeTestPublishGjpResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.GET;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/goodlife/test/publish/gjp";
    }
}
