package com.kuaishou.locallife.open.api.domain.locallife_shop;


/**
 * auto generate code
 */
public class TaskResult {
    private ClaimResult claim_result;
    private Long task_id;

    public ClaimResult getClaim_result() {
        return claim_result;
    }

    public void setClaim_result(ClaimResult claim_result) {
        this.claim_result = claim_result;
    }

    public Long getTask_id() {
        return task_id;
    }

    public void setTask_id(Long task_id) {
        this.task_id = task_id;
    }

}
