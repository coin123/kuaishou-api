package com.kuaishou.locallife.open.api.request.locallife_third_code;

import java.util.HashMap;
import java.util.Map;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.domain.locallife_third_code.OrderNoticeReq;
import com.kuaishou.locallife.open.api.response.locallife_third_code.IntegrationV1OrderNoticeResponse;

/**
 * auto generate code
 */
public class IntegrationV1OrderNoticeRequest extends AbstractKsLocalLifeRequest<IntegrationV1OrderNoticeResponse> {

    private OrderNoticeReq data;
    private String appKey;

    public OrderNoticeReq getData() {
        return data;
    }

    public void setData(OrderNoticeReq data) {
        this.data = data;
    }
    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private OrderNoticeReq data;
        private String appKey;

        public OrderNoticeReq getData() {
            return data;
        }

        public void setData(OrderNoticeReq data) {
            this.data = data;
        }

        public String getAppKey() {
            return appKey;
        }

        public void setAppKey(String appKey) {
            this.appKey = appKey;
        }

    }

    @Override
    public String getApiMethodName() {
        return "integration.v1.order.notice";
    }

    @Override
    public Class<IntegrationV1OrderNoticeResponse> getResponseClass() {
        return IntegrationV1OrderNoticeResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.POST;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/integration/v1/order/notice";
    }
}
