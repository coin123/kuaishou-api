package com.kuaishou.locallife.open.api.request.locallife_marketing;

import java.util.HashMap;
import java.util.Map;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.domain.locallife_marketing.MeituanOrderQuoteReq;
import com.kuaishou.locallife.open.api.response.locallife_marketing.IntegrationMtOrderQuoteQueryResponse;

/**
 * auto generate code
 */
public class IntegrationMtOrderQuoteQueryRequest extends AbstractKsLocalLifeRequest<IntegrationMtOrderQuoteQueryResponse> {

    private MeituanOrderQuoteReq data;
    private String appkey;

    public MeituanOrderQuoteReq getData() {
        return data;
    }

    public void setData(MeituanOrderQuoteReq data) {
        this.data = data;
    }
    public String getAppkey() {
        return appkey;
    }

    public void setAppkey(String appkey) {
        this.appkey = appkey;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private MeituanOrderQuoteReq data;
        private String appkey;

        public MeituanOrderQuoteReq getData() {
            return data;
        }

        public void setData(MeituanOrderQuoteReq data) {
            this.data = data;
        }

        public String getAppkey() {
            return appkey;
        }

        public void setAppkey(String appkey) {
            this.appkey = appkey;
        }

    }

    @Override
    public String getApiMethodName() {
        return "integration.mt.order.quote.query";
    }

    @Override
    public Class<IntegrationMtOrderQuoteQueryResponse> getResponseClass() {
        return IntegrationMtOrderQuoteQueryResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.POST;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/integration/mt/order/quote/query";
    }
}
