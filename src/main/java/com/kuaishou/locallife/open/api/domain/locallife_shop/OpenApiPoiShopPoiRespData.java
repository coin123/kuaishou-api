package com.kuaishou.locallife.open.api.domain.locallife_shop;

import java.util.List;

/**
 * auto generate code
 */
public class OpenApiPoiShopPoiRespData {
    private Integer total;
    private String description;
    private Long error_code;
    private List<OpenApiPoisData> pois;

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getError_code() {
        return error_code;
    }

    public void setError_code(Long error_code) {
        this.error_code = error_code;
    }

    public List<OpenApiPoisData> getPois() {
        return pois;
    }

    public void setPois(List<OpenApiPoisData> pois) {
        this.pois = pois;
    }

}
