package com.kuaishou.locallife.open.api.request.locallife_shop;

import java.util.HashMap;
import java.util.Map;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.response.locallife_shop.GoodlifeV1PoiMatchTaskQueryResponse;

/**
 * auto generate code
 */
public class GoodlifeV1PoiMatchTaskQueryRequest extends AbstractKsLocalLifeRequest<GoodlifeV1PoiMatchTaskQueryResponse> {

    private Long task_id;

    public Long getTask_id() {
        return task_id;
    }

    public void setTask_id(Long task_id) {
        this.task_id = task_id;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        
        bizParams.put("task_id", this.getTask_id() == null ? "" : this.getTask_id().toString());
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private Long task_id;

        public Long getTask_id() {
            return task_id;
        }

        public void setTask_id(Long task_id) {
            this.task_id = task_id;
        }

    }

    @Override
    public String getApiMethodName() {
        return "goodlife.v1.poi.match.task.query";
    }

    @Override
    public Class<GoodlifeV1PoiMatchTaskQueryResponse> getResponseClass() {
        return GoodlifeV1PoiMatchTaskQueryResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.GET;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/goodlife/v1/poi/match/task/query";
    }
}
