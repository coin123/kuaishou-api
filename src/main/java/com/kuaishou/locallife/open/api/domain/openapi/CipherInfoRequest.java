package com.kuaishou.locallife.open.api.domain.openapi;


/**
 * auto generate code
 */
public class CipherInfoRequest {
    private String cipher_text;

    public String getCipher_text() {
        return cipher_text;
    }

    public void setCipher_text(String cipher_text) {
        this.cipher_text = cipher_text;
    }

}
