package com.kuaishou.locallife.open.api.common.http;

import java.io.IOException;
import java.net.Proxy;
import java.util.Map;

import com.kuaishou.locallife.open.api.common.dto.KsHttpResponseDTO;

/**
 * @author gaojiapei <gaojiapei@kuaishou.com>
 * Created on 2023-02-03
 */
public interface HttpClient {

    KsHttpResponseDTO doGet(String url, Map<String, String> headers, Map<String, String> bizParams, Proxy proxy) throws
            IOException;

    KsHttpResponseDTO doPost(String url, Map<String, String> headers, String reqJson, Proxy proxy) throws IOException;

}
