package com.kuaishou.locallife.open.api;

import java.util.Map;

import com.kuaishou.locallife.open.api.common.HttpRequestMethod;

/**
 * @author shuxiaohui <shuxiaohui@kuaishou.com>
 * Created on 2020-03-30
 */
public interface KsLocalLifeRequest<T extends KsLocalLifeResponse> {
    /**
     * api method名称
     */
    String getApiMethodName();

    /**
     * 调用请求头
     */
    Map<String, String> getHeaderParams();

    /**
     * api 对应业务参数
     */
    Map<String, String> getApiParams();

    /**
     * 获取Post请求body中的Json串
     * @return
     */
    String getApiReqJson();

    /**
     * api 对应resp实体class
     */
    Class<T> getResponseClass();

    /**
     * api http请求方法
     */
    HttpRequestMethod getHttpRequestMethod();

    /**
     * 特殊处理目前快手open api通过path来区分，没有统一api网关的能力
     */
    String getRequestSpecialPath();
}
