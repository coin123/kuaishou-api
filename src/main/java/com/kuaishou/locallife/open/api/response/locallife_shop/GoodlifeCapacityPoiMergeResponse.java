package com.kuaishou.locallife.open.api.response.locallife_shop;


import com.kuaishou.locallife.open.api.domain.locallife_shop.ExternalPoiSearchResOpenData;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class GoodlifeCapacityPoiMergeResponse extends KsLocalLifeResponse {

    private ExternalPoiSearchResOpenData data;

    public ExternalPoiSearchResOpenData getData() {
        return data;
    }

    public void setData(ExternalPoiSearchResOpenData data) {
        this.data = data;
    }

}