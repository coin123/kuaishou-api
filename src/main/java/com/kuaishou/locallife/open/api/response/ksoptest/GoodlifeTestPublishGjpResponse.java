package com.kuaishou.locallife.open.api.response.ksoptest;


import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class GoodlifeTestPublishGjpResponse extends KsLocalLifeResponse {

    private String res;
    private Long res_code;

    public String getRes() {
        return res;
    }

    public void setRes(String res) {
        this.res = res;
    }

    public Long getRes_code() {
        return res_code;
    }

    public void setRes_code(Long res_code) {
        this.res_code = res_code;
    }

}