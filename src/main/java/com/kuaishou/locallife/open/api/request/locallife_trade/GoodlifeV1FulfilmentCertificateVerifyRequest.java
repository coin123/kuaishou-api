package com.kuaishou.locallife.open.api.request.locallife_trade;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.domain.locallife_trade.CodeWithTime;
import com.kuaishou.locallife.open.api.response.locallife_trade.GoodlifeV1FulfilmentCertificateVerifyResponse;

/**
 * auto generate code
 */
public class GoodlifeV1FulfilmentCertificateVerifyRequest extends AbstractKsLocalLifeRequest<GoodlifeV1FulfilmentCertificateVerifyResponse> {

    private String verify_token;
    private String poi_id;
    private List<String> encrypted_codes;
    private List<String> codes;
    private String order_id;
    private List<CodeWithTime> code_with_time_list;
    private String out_order_id;

    public String getVerify_token() {
        return verify_token;
    }

    public void setVerify_token(String verify_token) {
        this.verify_token = verify_token;
    }
    public String getPoi_id() {
        return poi_id;
    }

    public void setPoi_id(String poi_id) {
        this.poi_id = poi_id;
    }
    public List<String> getEncrypted_codes() {
        return encrypted_codes;
    }

    public void setEncrypted_codes(List<String> encrypted_codes) {
        this.encrypted_codes = encrypted_codes;
    }
    public List<String> getCodes() {
        return codes;
    }

    public void setCodes(List<String> codes) {
        this.codes = codes;
    }
    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }
    public List<CodeWithTime> getCode_with_time_list() {
        return code_with_time_list;
    }

    public void setCode_with_time_list(List<CodeWithTime> code_with_time_list) {
        this.code_with_time_list = code_with_time_list;
    }
    public String getOut_order_id() {
        return out_order_id;
    }

    public void setOut_order_id(String out_order_id) {
        this.out_order_id = out_order_id;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private String verify_token;
        private String poi_id;
        private List<String> encrypted_codes;
        private List<String> codes;
        private String order_id;
        private List<CodeWithTime> code_with_time_list;
        private String out_order_id;

        public String getVerify_token() {
            return verify_token;
        }

        public void setVerify_token(String verify_token) {
            this.verify_token = verify_token;
        }

        public String getPoi_id() {
            return poi_id;
        }

        public void setPoi_id(String poi_id) {
            this.poi_id = poi_id;
        }

        public List<String> getEncrypted_codes() {
            return encrypted_codes;
        }

        public void setEncrypted_codes(List<String> encrypted_codes) {
            this.encrypted_codes = encrypted_codes;
        }

        public List<String> getCodes() {
            return codes;
        }

        public void setCodes(List<String> codes) {
            this.codes = codes;
        }

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public List<CodeWithTime> getCode_with_time_list() {
            return code_with_time_list;
        }

        public void setCode_with_time_list(List<CodeWithTime> code_with_time_list) {
            this.code_with_time_list = code_with_time_list;
        }

        public String getOut_order_id() {
            return out_order_id;
        }

        public void setOut_order_id(String out_order_id) {
            this.out_order_id = out_order_id;
        }

    }

    @Override
    public String getApiMethodName() {
        return "goodlife.v1.fulfilment.certificate.verify";
    }

    @Override
    public Class<GoodlifeV1FulfilmentCertificateVerifyResponse> getResponseClass() {
        return GoodlifeV1FulfilmentCertificateVerifyResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.POST;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/goodlife/v1/fulfilment/certificate/verify";
    }
}
