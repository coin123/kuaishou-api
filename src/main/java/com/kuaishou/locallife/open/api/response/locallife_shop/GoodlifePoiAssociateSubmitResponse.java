package com.kuaishou.locallife.open.api.response.locallife_shop;


import com.kuaishou.locallife.open.api.domain.locallife_shop.StoreAssociationResData;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class GoodlifePoiAssociateSubmitResponse extends KsLocalLifeResponse {

    private StoreAssociationResData data;

    public StoreAssociationResData getData() {
        return data;
    }

    public void setData(StoreAssociationResData data) {
        this.data = data;
    }

}