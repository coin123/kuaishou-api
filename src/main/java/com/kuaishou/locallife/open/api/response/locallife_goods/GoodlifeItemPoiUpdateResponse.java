package com.kuaishou.locallife.open.api.response.locallife_goods;


import com.kuaishou.locallife.open.api.domain.locallife_goods.ErrorInfoPb;
import com.kuaishou.locallife.open.api.domain.locallife_goods.ResponseData;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class GoodlifeItemPoiUpdateResponse extends KsLocalLifeResponse {

    private ResponseData data;
    private ErrorInfoPb error_info;

    public ResponseData getData() {
        return data;
    }

    public void setData(ResponseData data) {
        this.data = data;
    }

    public ErrorInfoPb getError_info() {
        return error_info;
    }

    public void setError_info(ErrorInfoPb error_info) {
        this.error_info = error_info;
    }

}