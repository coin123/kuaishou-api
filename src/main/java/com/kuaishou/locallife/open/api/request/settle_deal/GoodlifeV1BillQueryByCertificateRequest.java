package com.kuaishou.locallife.open.api.request.settle_deal;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.response.settle_deal.GoodlifeV1BillQueryByCertificateResponse;

/**
 * auto generate code
 */
public class GoodlifeV1BillQueryByCertificateRequest extends AbstractKsLocalLifeRequest<GoodlifeV1BillQueryByCertificateResponse> {

    private List<String> certificate_ids;
    private String cursor;
    private Integer size;
    private String bill_date;

    public List<String> getCertificate_ids() {
        return certificate_ids;
    }

    public void setCertificate_ids(List<String> certificate_ids) {
        this.certificate_ids = certificate_ids;
    }
    public String getCursor() {
        return cursor;
    }

    public void setCursor(String cursor) {
        this.cursor = cursor;
    }
    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }
    public String getBill_date() {
        return bill_date;
    }

    public void setBill_date(String bill_date) {
        this.bill_date = bill_date;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        
        bizParams.put("certificate_ids", this.getCertificate_ids() == null ? "" : this.getCertificate_ids().toString());
        bizParams.put("cursor", this.getCursor());
        
        
        bizParams.put("size", this.getSize() == null ? "" : this.getSize().toString());
        bizParams.put("bill_date", this.getBill_date());
        
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private List<String> certificate_ids;
        private String cursor;
        private Integer size;
        private String bill_date;

        public List<String> getCertificate_ids() {
            return certificate_ids;
        }

        public void setCertificate_ids(List<String> certificate_ids) {
            this.certificate_ids = certificate_ids;
        }

        public String getCursor() {
            return cursor;
        }

        public void setCursor(String cursor) {
            this.cursor = cursor;
        }

        public Integer getSize() {
            return size;
        }

        public void setSize(Integer size) {
            this.size = size;
        }

        public String getBill_date() {
            return bill_date;
        }

        public void setBill_date(String bill_date) {
            this.bill_date = bill_date;
        }

    }

    @Override
    public String getApiMethodName() {
        return "goodlife.v1.bill.query.by.certificate";
    }

    @Override
    public Class<GoodlifeV1BillQueryByCertificateResponse> getResponseClass() {
        return GoodlifeV1BillQueryByCertificateResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.GET;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/goodlife/v1/bill/query/by/certificate";
    }
}
