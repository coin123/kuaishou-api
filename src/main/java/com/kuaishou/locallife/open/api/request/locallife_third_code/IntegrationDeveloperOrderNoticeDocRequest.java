package com.kuaishou.locallife.open.api.request.locallife_third_code;

import java.util.HashMap;
import java.util.Map;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.domain.locallife_third_code.Amount;
import com.kuaishou.locallife.open.api.domain.locallife_third_code.Contact;
import com.kuaishou.locallife.open.api.domain.locallife_third_code.Tourists;
import com.kuaishou.locallife.open.api.response.locallife_third_code.IntegrationDeveloperOrderNoticeDocResponse;

/**
 * auto generate code
 */
public class IntegrationDeveloperOrderNoticeDocRequest extends AbstractKsLocalLifeRequest<IntegrationDeveloperOrderNoticeDocResponse> {

    private Integer count;
    private String sku_id;
    private String third_sku_id;
    private Long product_id;
    private String out_id;
    private String out_sku_id;
    private Amount amount;
    private Long account_id;
    private String idempotent_id;
    private Contact contact;
    private Tourists[] tourists;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
    public String getSku_id() {
        return sku_id;
    }

    public void setSku_id(String sku_id) {
        this.sku_id = sku_id;
    }
    public String getThird_sku_id() {
        return third_sku_id;
    }

    public void setThird_sku_id(String third_sku_id) {
        this.third_sku_id = third_sku_id;
    }
    public Long getProduct_id() {
        return product_id;
    }

    public void setProduct_id(Long product_id) {
        this.product_id = product_id;
    }
    public String getOut_id() {
        return out_id;
    }

    public void setOut_id(String out_id) {
        this.out_id = out_id;
    }
    public String getOut_sku_id() {
        return out_sku_id;
    }

    public void setOut_sku_id(String out_sku_id) {
        this.out_sku_id = out_sku_id;
    }
    public Amount getAmount() {
        return amount;
    }

    public void setAmount(Amount amount) {
        this.amount = amount;
    }
    public Long getAccount_id() {
        return account_id;
    }

    public void setAccount_id(Long account_id) {
        this.account_id = account_id;
    }
    public String getIdempotent_id() {
        return idempotent_id;
    }

    public void setIdempotent_id(String idempotent_id) {
        this.idempotent_id = idempotent_id;
    }
    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }
    public Tourists[] getTourists() {
        return tourists;
    }

    public void setTourists(Tourists[] tourists) {
        this.tourists = tourists;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private Integer count;
        private String sku_id;
        private String third_sku_id;
        private Long product_id;
        private String out_id;
        private String out_sku_id;
        private Amount amount;
        private Long account_id;
        private String idempotent_id;
        private Contact contact;
        private Tourists[] tourists;

        public Integer getCount() {
            return count;
        }

        public void setCount(Integer count) {
            this.count = count;
        }

        public String getSku_id() {
            return sku_id;
        }

        public void setSku_id(String sku_id) {
            this.sku_id = sku_id;
        }

        public String getThird_sku_id() {
            return third_sku_id;
        }

        public void setThird_sku_id(String third_sku_id) {
            this.third_sku_id = third_sku_id;
        }

        public Long getProduct_id() {
            return product_id;
        }

        public void setProduct_id(Long product_id) {
            this.product_id = product_id;
        }

        public String getOut_id() {
            return out_id;
        }

        public void setOut_id(String out_id) {
            this.out_id = out_id;
        }

        public String getOut_sku_id() {
            return out_sku_id;
        }

        public void setOut_sku_id(String out_sku_id) {
            this.out_sku_id = out_sku_id;
        }

        public Amount getAmount() {
            return amount;
        }

        public void setAmount(Amount amount) {
            this.amount = amount;
        }

        public Long getAccount_id() {
            return account_id;
        }

        public void setAccount_id(Long account_id) {
            this.account_id = account_id;
        }

        public String getIdempotent_id() {
            return idempotent_id;
        }

        public void setIdempotent_id(String idempotent_id) {
            this.idempotent_id = idempotent_id;
        }

        public Contact getContact() {
            return contact;
        }

        public void setContact(Contact contact) {
            this.contact = contact;
        }

        public Tourists[] getTourists() {
            return tourists;
        }

        public void setTourists(Tourists[] tourists) {
            this.tourists = tourists;
        }

    }

    @Override
    public String getApiMethodName() {
        return "integration.developer.order.notice.doc";
    }

    @Override
    public Class<IntegrationDeveloperOrderNoticeDocResponse> getResponseClass() {
        return IntegrationDeveloperOrderNoticeDocResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.POST;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/integration/developer/order/notice/doc";
    }
}
