package com.kuaishou.locallife.open.api.request.locallife_third_code;

import java.util.HashMap;
import java.util.Map;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.response.locallife_third_code.GoodlifeV1FulfilmentRefundAuditBatchResponse;

/**
 * auto generate code
 */
public class GoodlifeV1FulfilmentRefundAuditBatchRequest extends AbstractKsLocalLifeRequest<GoodlifeV1FulfilmentRefundAuditBatchResponse> {

    private String trade_no;
    private String out_order_id;
    private Integer result;
    private String reason;
    private String order_id;

    public String getTrade_no() {
        return trade_no;
    }

    public void setTrade_no(String trade_no) {
        this.trade_no = trade_no;
    }
    public String getOut_order_id() {
        return out_order_id;
    }

    public void setOut_order_id(String out_order_id) {
        this.out_order_id = out_order_id;
    }
    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }
    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private String trade_no;
        private String out_order_id;
        private Integer result;
        private String reason;
        private String order_id;

        public String getTrade_no() {
            return trade_no;
        }

        public void setTrade_no(String trade_no) {
            this.trade_no = trade_no;
        }

        public String getOut_order_id() {
            return out_order_id;
        }

        public void setOut_order_id(String out_order_id) {
            this.out_order_id = out_order_id;
        }

        public Integer getResult() {
            return result;
        }

        public void setResult(Integer result) {
            this.result = result;
        }

        public String getReason() {
            return reason;
        }

        public void setReason(String reason) {
            this.reason = reason;
        }

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

    }

    @Override
    public String getApiMethodName() {
        return "goodlife.v1.fulfilment.refund.audit.batch";
    }

    @Override
    public Class<GoodlifeV1FulfilmentRefundAuditBatchResponse> getResponseClass() {
        return GoodlifeV1FulfilmentRefundAuditBatchResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.POST;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/goodlife/v1/fulfilment/refund/audit/batch";
    }
}
