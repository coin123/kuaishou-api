package com.kuaishou.locallife.open.api.common;

/**
 * 常量相关类
 *
 * @author shuxiaohui <shuxiaohui@kuaishou.com>
 * Created on 2020-03-30
 */
public interface Constants {

    /**
     * 开放平台SDK版本，每次Release都需修改此版本
     */
    String LOCALLIFE_OPEN_SDK_VERSION = "20230201-1.0.0";

    /**
     * Client相关常量
     */
    interface Client {
        String DEFAULT_SERVER_REST_URL = "https://lbs-open.kuaishou.com";
    }

    /**
     * Oauth授权相关常量
     */
    interface Oauth {
        String OAUTH_REST_URL = "https://lbs-open.kuaishou.com";
        String OAUTH_ACCESS_TOKEN_URL = OAUTH_REST_URL + "/oauth2/access_token";
        String OAUTH_REFRESH_TOKEN_URL = OAUTH_REST_URL + "/oauth2/refresh_token";

        // 授权码类型
        String OAUTH_GRANT_TYPE_CODE = "code";
        // 刷新token类型
        String OAUTH_GRANT_TYPE_REFRESH_TOKEN = "refresh_token";
    }

    /**
     * 本地错误码(与server保持一致)
     */
    interface LocalErrorCode {
        int ERROR_CODE_PARAM_BLANK = 12;
        int ERROR_CODE_PARAM_INVALID = 13;
    }

    interface Headers {
        String ACCESS_TOKEN = "access-token";
    }

    interface ContentType {

        String X_WWW_FORM_URLENCODED = "application/x-www-form-urlencoded; charset=UTF-8";

        String JSON = "application/json; charset=UTF-8";

    }

    // 快手PRT测试环境泳道
    interface TraceContext {
        // 默认PRT基准泳道
        String DEFAULT_PRT_TRACE_CONTEXT = "{\"laneId\": \"PRT.isv.test\"}";

        String LANEID = "{\"laneId\": ";

        String LEFT_QUOTATION = "\"";

        String RIGHT_QUOTATION = "\"}";
    }
}
