package com.kuaishou.locallife.open.api.domain.settle_deal;


/**
 * auto generate code
 */
public class OpenApiVerifyHistoryData {
    private Long error_code;
    private String description;
    private Integer total;
    private OpenApiVerifyHistoryRecord[] records;

    public Long getError_code() {
        return error_code;
    }

    public void setError_code(Long error_code) {
        this.error_code = error_code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public OpenApiVerifyHistoryRecord[] getRecords() {
        return records;
    }

    public void setRecords(OpenApiVerifyHistoryRecord[] records) {
        this.records = records;
    }

}
