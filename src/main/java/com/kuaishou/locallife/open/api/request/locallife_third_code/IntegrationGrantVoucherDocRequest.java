package com.kuaishou.locallife.open.api.request.locallife_third_code;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.domain.locallife_third_code.Contact;
import com.kuaishou.locallife.open.api.domain.locallife_third_code.GranVoucherAmount;
import com.kuaishou.locallife.open.api.domain.locallife_third_code.Sku;
import com.kuaishou.locallife.open.api.domain.locallife_third_code.Tourist;
import com.kuaishou.locallife.open.api.response.locallife_third_code.IntegrationGrantVoucherDocResponse;

/**
 * auto generate code
 */
public class IntegrationGrantVoucherDocRequest extends AbstractKsLocalLifeRequest<IntegrationGrantVoucherDocResponse> {

    private Long order_id;
    private Integer count;
    private Integer start_time;
    private Integer expire_time;
    private String sku_id;
    private String third_sku_id;
    private Sku sku;
    private GranVoucherAmount amount;
    private List<Tourist> tourists;
    private Long account_id;
    private Contact contact;

    public Long getOrder_id() {
        return order_id;
    }

    public void setOrder_id(Long order_id) {
        this.order_id = order_id;
    }
    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
    public Integer getStart_time() {
        return start_time;
    }

    public void setStart_time(Integer start_time) {
        this.start_time = start_time;
    }
    public Integer getExpire_time() {
        return expire_time;
    }

    public void setExpire_time(Integer expire_time) {
        this.expire_time = expire_time;
    }
    public String getSku_id() {
        return sku_id;
    }

    public void setSku_id(String sku_id) {
        this.sku_id = sku_id;
    }
    public String getThird_sku_id() {
        return third_sku_id;
    }

    public void setThird_sku_id(String third_sku_id) {
        this.third_sku_id = third_sku_id;
    }
    public Sku getSku() {
        return sku;
    }

    public void setSku(Sku sku) {
        this.sku = sku;
    }
    public GranVoucherAmount getAmount() {
        return amount;
    }

    public void setAmount(GranVoucherAmount amount) {
        this.amount = amount;
    }
    public List<Tourist> getTourists() {
        return tourists;
    }

    public void setTourists(List<Tourist> tourists) {
        this.tourists = tourists;
    }
    public Long getAccount_id() {
        return account_id;
    }

    public void setAccount_id(Long account_id) {
        this.account_id = account_id;
    }
    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private Long order_id;
        private Integer count;
        private Integer start_time;
        private Integer expire_time;
        private String sku_id;
        private String third_sku_id;
        private Sku sku;
        private GranVoucherAmount amount;
        private List<Tourist> tourists;
        private Long account_id;
        private Contact contact;

        public Long getOrder_id() {
            return order_id;
        }

        public void setOrder_id(Long order_id) {
            this.order_id = order_id;
        }

        public Integer getCount() {
            return count;
        }

        public void setCount(Integer count) {
            this.count = count;
        }

        public Integer getStart_time() {
            return start_time;
        }

        public void setStart_time(Integer start_time) {
            this.start_time = start_time;
        }

        public Integer getExpire_time() {
            return expire_time;
        }

        public void setExpire_time(Integer expire_time) {
            this.expire_time = expire_time;
        }

        public String getSku_id() {
            return sku_id;
        }

        public void setSku_id(String sku_id) {
            this.sku_id = sku_id;
        }

        public String getThird_sku_id() {
            return third_sku_id;
        }

        public void setThird_sku_id(String third_sku_id) {
            this.third_sku_id = third_sku_id;
        }

        public Sku getSku() {
            return sku;
        }

        public void setSku(Sku sku) {
            this.sku = sku;
        }

        public GranVoucherAmount getAmount() {
            return amount;
        }

        public void setAmount(GranVoucherAmount amount) {
            this.amount = amount;
        }

        public List<Tourist> getTourists() {
            return tourists;
        }

        public void setTourists(List<Tourist> tourists) {
            this.tourists = tourists;
        }

        public Long getAccount_id() {
            return account_id;
        }

        public void setAccount_id(Long account_id) {
            this.account_id = account_id;
        }

        public Contact getContact() {
            return contact;
        }

        public void setContact(Contact contact) {
            this.contact = contact;
        }

    }

    @Override
    public String getApiMethodName() {
        return "integration.grant.voucher.doc";
    }

    @Override
    public Class<IntegrationGrantVoucherDocResponse> getResponseClass() {
        return IntegrationGrantVoucherDocResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.POST;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/integration/grant/voucher/doc";
    }
}
