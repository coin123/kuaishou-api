package com.kuaishou.locallife.open.api.response.locallife_goods;


import com.kuaishou.locallife.open.api.domain.locallife_goods.Result;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class GoodlifeV1ItemShelfChangestatusResponse extends KsLocalLifeResponse {

    private Result data;

    public Result getData() {
        return data;
    }

    public void setData(Result data) {
        this.data = data;
    }

}