package com.kuaishou.locallife.open.api.response.settle_deal;


import com.kuaishou.locallife.open.api.domain.settle_deal.OpenApiBillQueryData;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class GoodlifeLbsBillQueryResponse extends KsLocalLifeResponse {

    private OpenApiBillQueryData data;

    public OpenApiBillQueryData getData() {
        return data;
    }

    public void setData(OpenApiBillQueryData data) {
        this.data = data;
    }

}