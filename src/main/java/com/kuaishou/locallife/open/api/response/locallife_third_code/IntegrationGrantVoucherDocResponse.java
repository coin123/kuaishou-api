package com.kuaishou.locallife.open.api.response.locallife_third_code;


import com.kuaishou.locallife.open.api.domain.locallife_third_code.Resp;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class IntegrationGrantVoucherDocResponse extends KsLocalLifeResponse {

    private Resp data;

    public Resp getData() {
        return data;
    }

    public void setData(Resp data) {
        this.data = data;
    }

}