package com.kuaishou.locallife.open.api.domain.locallife_goods;

import java.util.List;

/**
 * auto generate code
 */
public class GetItemTemplateResponse {
    private Long error_code;
    private String description;
    private List<AttributeInfo> sku_attr_info;
    private List<AttributeInfo> item_attr_info;

    public Long getError_code() {
        return error_code;
    }

    public void setError_code(Long error_code) {
        this.error_code = error_code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<AttributeInfo> getSku_attr_info() {
        return sku_attr_info;
    }

    public void setSku_attr_info(List<AttributeInfo> sku_attr_info) {
        this.sku_attr_info = sku_attr_info;
    }

    public List<AttributeInfo> getItem_attr_info() {
        return item_attr_info;
    }

    public void setItem_attr_info(List<AttributeInfo> item_attr_info) {
        this.item_attr_info = item_attr_info;
    }

}
