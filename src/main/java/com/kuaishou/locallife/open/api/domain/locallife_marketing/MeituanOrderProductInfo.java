package com.kuaishou.locallife.open.api.domain.locallife_marketing;


/**
 * auto generate code
 */
public class MeituanOrderProductInfo {
    private String brandname;
    private String dealid;
    private String dealname;
    private String[] labels;
    private String[] refundlabels;
    private String originprice;
    private String price;
    private String limitNotice;
    private Long maxcount;
    private Long mincount;
    private Boolean canbuy;
    private Integer remain;

    public String getBrandname() {
        return brandname;
    }

    public void setBrandname(String brandname) {
        this.brandname = brandname;
    }

    public String getDealid() {
        return dealid;
    }

    public void setDealid(String dealid) {
        this.dealid = dealid;
    }

    public String getDealname() {
        return dealname;
    }

    public void setDealname(String dealname) {
        this.dealname = dealname;
    }

    public String[] getLabels() {
        return labels;
    }

    public void setLabels(String[] labels) {
        this.labels = labels;
    }

    public String[] getRefundlabels() {
        return refundlabels;
    }

    public void setRefundlabels(String[] refundlabels) {
        this.refundlabels = refundlabels;
    }

    public String getOriginprice() {
        return originprice;
    }

    public void setOriginprice(String originprice) {
        this.originprice = originprice;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getLimitNotice() {
        return limitNotice;
    }

    public void setLimitNotice(String limitNotice) {
        this.limitNotice = limitNotice;
    }

    public Long getMaxcount() {
        return maxcount;
    }

    public void setMaxcount(Long maxcount) {
        this.maxcount = maxcount;
    }

    public Long getMincount() {
        return mincount;
    }

    public void setMincount(Long mincount) {
        this.mincount = mincount;
    }

    public Boolean getCanbuy() {
        return canbuy;
    }

    public void setCanbuy(Boolean canbuy) {
        this.canbuy = canbuy;
    }

    public Integer getRemain() {
        return remain;
    }

    public void setRemain(Integer remain) {
        this.remain = remain;
    }

}
