package com.kuaishou.locallife.open.api.response.openapi;


import com.kuaishou.locallife.open.api.domain.openapi.MeituanTokenData;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class IntegrationMeituanRefreshTokenResponse extends KsLocalLifeResponse {

    private MeituanTokenData data;

    public MeituanTokenData getData() {
        return data;
    }

    public void setData(MeituanTokenData data) {
        this.data = data;
    }

}