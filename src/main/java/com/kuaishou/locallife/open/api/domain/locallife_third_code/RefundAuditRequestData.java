package com.kuaishou.locallife.open.api.domain.locallife_third_code;

import java.util.List;

/**
 * auto generate code
 */
public class RefundAuditRequestData {
    private String order_id;
    private List<RefundAuditCertificate> certificates;
    private String out_order_id;
    private String trade_no;
    private Integer verify_status;
    private String refund_reason;
    private String refund_amount;
    private String refund_ext;
    private Long account_id;

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public List<RefundAuditCertificate> getCertificates() {
        return certificates;
    }

    public void setCertificates(List<RefundAuditCertificate> certificates) {
        this.certificates = certificates;
    }

    public String getOut_order_id() {
        return out_order_id;
    }

    public void setOut_order_id(String out_order_id) {
        this.out_order_id = out_order_id;
    }

    public String getTrade_no() {
        return trade_no;
    }

    public void setTrade_no(String trade_no) {
        this.trade_no = trade_no;
    }

    public Integer getVerify_status() {
        return verify_status;
    }

    public void setVerify_status(Integer verify_status) {
        this.verify_status = verify_status;
    }

    public String getRefund_reason() {
        return refund_reason;
    }

    public void setRefund_reason(String refund_reason) {
        this.refund_reason = refund_reason;
    }

    public String getRefund_amount() {
        return refund_amount;
    }

    public void setRefund_amount(String refund_amount) {
        this.refund_amount = refund_amount;
    }

    public String getRefund_ext() {
        return refund_ext;
    }

    public void setRefund_ext(String refund_ext) {
        this.refund_ext = refund_ext;
    }

    public Long getAccount_id() {
        return account_id;
    }

    public void setAccount_id(Long account_id) {
        this.account_id = account_id;
    }

}
