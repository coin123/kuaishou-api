package com.kuaishou.locallife.open.api.request.locallife_shop;

import java.util.HashMap;
import java.util.Map;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.response.locallife_shop.GoodlifeV1PoiPoiSyncResponse;

/**
 * auto generate code
 */
public class GoodlifeV1PoiPoiSyncRequest extends AbstractKsLocalLifeRequest<GoodlifeV1PoiPoiSyncResponse> {

    private String task_ids;

    public String getTask_ids() {
        return task_ids;
    }

    public void setTask_ids(String task_ids) {
        this.task_ids = task_ids;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private String task_ids;

        public String getTask_ids() {
            return task_ids;
        }

        public void setTask_ids(String task_ids) {
            this.task_ids = task_ids;
        }

    }

    @Override
    public String getApiMethodName() {
        return "goodlife.v1.poi.poi.sync";
    }

    @Override
    public Class<GoodlifeV1PoiPoiSyncResponse> getResponseClass() {
        return GoodlifeV1PoiPoiSyncResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.POST;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/goodlife/v1/poi/poi/sync";
    }
}
