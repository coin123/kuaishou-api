package com.kuaishou.locallife.open.api.response.locallife_trade;


import com.kuaishou.locallife.open.api.domain.locallife_trade.OldQueryCertificateStateRespData;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class NamekFulfilmentQueryCertificateResponse extends KsLocalLifeResponse {

    private OldQueryCertificateStateRespData data;

    public OldQueryCertificateStateRespData getData() {
        return data;
    }

    public void setData(OldQueryCertificateStateRespData data) {
        this.data = data;
    }

}