package com.kuaishou.locallife.open.api.response.locallife_material;

import com.kuaishou.locallife.open.api.KsLocalLifeResponse;
import com.kuaishou.locallife.open.api.domain.locallife_material.PreviousVersionResult;

/**
 * @author gaojiapei <gaojiapei@kuaishou.com>
 * Created on 2023-04-14
 * 兼容之前手动配置的版本
 */
public class GoodlifeV1ItemPicUploadPreviousVersionResponse extends KsLocalLifeResponse {
    private PreviousVersionResult data;

    public GoodlifeV1ItemPicUploadPreviousVersionResponse() {
    }

    public PreviousVersionResult getData() {
        return this.data;
    }

    public void setData(PreviousVersionResult data) {
        this.data = data;
    }
}
