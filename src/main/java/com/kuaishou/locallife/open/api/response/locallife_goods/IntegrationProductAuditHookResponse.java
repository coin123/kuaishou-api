package com.kuaishou.locallife.open.api.response.locallife_goods;


import com.kuaishou.locallife.open.api.domain.locallife_goods.AuditData;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class IntegrationProductAuditHookResponse extends KsLocalLifeResponse {

    private AuditData data;

    public AuditData getData() {
        return data;
    }

    public void setData(AuditData data) {
        this.data = data;
    }

}