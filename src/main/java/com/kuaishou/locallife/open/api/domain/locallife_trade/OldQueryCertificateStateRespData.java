package com.kuaishou.locallife.open.api.domain.locallife_trade;

import java.util.List;

/**
 * auto generate code
 */
public class OldQueryCertificateStateRespData {
    private Long error_code;
    private String description;
    private List<Certificate> certificates;
    private Certificate certificate;

    public Long getError_code() {
        return error_code;
    }

    public void setError_code(Long error_code) {
        this.error_code = error_code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Certificate> getCertificates() {
        return certificates;
    }

    public void setCertificates(List<Certificate> certificates) {
        this.certificates = certificates;
    }

    public Certificate getCertificate() {
        return certificate;
    }

    public void setCertificate(Certificate certificate) {
        this.certificate = certificate;
    }

}
