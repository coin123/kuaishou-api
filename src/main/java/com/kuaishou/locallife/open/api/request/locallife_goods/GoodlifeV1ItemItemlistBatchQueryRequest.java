package com.kuaishou.locallife.open.api.request.locallife_goods;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.response.locallife_goods.GoodlifeV1ItemItemlistBatchQueryResponse;

/**
 * auto generate code
 */
public class GoodlifeV1ItemItemlistBatchQueryRequest extends AbstractKsLocalLifeRequest<GoodlifeV1ItemItemlistBatchQueryResponse> {

    private List<Long> product_id;
    private Long start_time;
    private Long end_time;
    private Long cursor;
    private Long size;

    public List<Long> getProduct_id() {
        return product_id;
    }

    public void setProduct_id(List<Long> product_id) {
        this.product_id = product_id;
    }
    public Long getStart_time() {
        return start_time;
    }

    public void setStart_time(Long start_time) {
        this.start_time = start_time;
    }
    public Long getEnd_time() {
        return end_time;
    }

    public void setEnd_time(Long end_time) {
        this.end_time = end_time;
    }
    public Long getCursor() {
        return cursor;
    }

    public void setCursor(Long cursor) {
        this.cursor = cursor;
    }
    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private List<Long> product_id;
        private Long start_time;
        private Long end_time;
        private Long cursor;
        private Long size;

        public List<Long> getProduct_id() {
            return product_id;
        }

        public void setProduct_id(List<Long> product_id) {
            this.product_id = product_id;
        }

        public Long getStart_time() {
            return start_time;
        }

        public void setStart_time(Long start_time) {
            this.start_time = start_time;
        }

        public Long getEnd_time() {
            return end_time;
        }

        public void setEnd_time(Long end_time) {
            this.end_time = end_time;
        }

        public Long getCursor() {
            return cursor;
        }

        public void setCursor(Long cursor) {
            this.cursor = cursor;
        }

        public Long getSize() {
            return size;
        }

        public void setSize(Long size) {
            this.size = size;
        }

    }

    @Override
    public String getApiMethodName() {
        return "goodlife.v1.item.itemlist.batch.query";
    }

    @Override
    public Class<GoodlifeV1ItemItemlistBatchQueryResponse> getResponseClass() {
        return GoodlifeV1ItemItemlistBatchQueryResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.POST;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/goodlife/v1/item/itemlist/batch/query";
    }
}
