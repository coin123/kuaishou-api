package com.kuaishou.locallife.open.api.domain.locallife_distribution;

import java.util.List;

/**
 * auto generate code
 */
public class FailReason {
    private Integer error_code;
    private String description;
    private Long item_id;
    private List<ErrorDistributor> distributors;

    public Integer getError_code() {
        return error_code;
    }

    public void setError_code(Integer error_code) {
        this.error_code = error_code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getItem_id() {
        return item_id;
    }

    public void setItem_id(Long item_id) {
        this.item_id = item_id;
    }

    public List<ErrorDistributor> getDistributors() {
        return distributors;
    }

    public void setDistributors(List<ErrorDistributor> distributors) {
        this.distributors = distributors;
    }

}
