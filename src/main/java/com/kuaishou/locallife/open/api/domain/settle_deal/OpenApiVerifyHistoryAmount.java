package com.kuaishou.locallife.open.api.domain.settle_deal;


/**
 * auto generate code
 */
public class OpenApiVerifyHistoryAmount {
    private Integer original_amount;
    private Integer pay_amount;
    private Integer merchant_ticket_amount;

    public Integer getOriginal_amount() {
        return original_amount;
    }

    public void setOriginal_amount(Integer original_amount) {
        this.original_amount = original_amount;
    }

    public Integer getPay_amount() {
        return pay_amount;
    }

    public void setPay_amount(Integer pay_amount) {
        this.pay_amount = pay_amount;
    }

    public Integer getMerchant_ticket_amount() {
        return merchant_ticket_amount;
    }

    public void setMerchant_ticket_amount(Integer merchant_ticket_amount) {
        this.merchant_ticket_amount = merchant_ticket_amount;
    }

}
