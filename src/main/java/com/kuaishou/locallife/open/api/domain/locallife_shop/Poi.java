package com.kuaishou.locallife.open.api.domain.locallife_shop;


/**
 * auto generate code
 */
public class Poi {
    private String third_id;
    private String poi_name;
    private String address;
    private String latitude;
    private String longtitude;
    private Integer rel_type;
    private String poi_id;

    public String getThird_id() {
        return third_id;
    }

    public void setThird_id(String third_id) {
        this.third_id = third_id;
    }

    public String getPoi_name() {
        return poi_name;
    }

    public void setPoi_name(String poi_name) {
        this.poi_name = poi_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(String longtitude) {
        this.longtitude = longtitude;
    }

    public Integer getRel_type() {
        return rel_type;
    }

    public void setRel_type(Integer rel_type) {
        this.rel_type = rel_type;
    }

    public String getPoi_id() {
        return poi_id;
    }

    public void setPoi_id(String poi_id) {
        this.poi_id = poi_id;
    }

}
