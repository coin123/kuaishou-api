package com.kuaishou.locallife.open.api.request.locallife_third_code;

import java.util.HashMap;
import java.util.Map;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.domain.locallife_third_code.GrantVoucherReq;
import com.kuaishou.locallife.open.api.response.locallife_third_code.IntegrationV1GrantVoucherResponse;

/**
 * auto generate code
 */
public class IntegrationV1GrantVoucherRequest extends AbstractKsLocalLifeRequest<IntegrationV1GrantVoucherResponse> {

    private String appKey;
    private GrantVoucherReq data;

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }
    public GrantVoucherReq getData() {
        return data;
    }

    public void setData(GrantVoucherReq data) {
        this.data = data;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private String appKey;
        private GrantVoucherReq data;

        public String getAppKey() {
            return appKey;
        }

        public void setAppKey(String appKey) {
            this.appKey = appKey;
        }

        public GrantVoucherReq getData() {
            return data;
        }

        public void setData(GrantVoucherReq data) {
            this.data = data;
        }

    }

    @Override
    public String getApiMethodName() {
        return "integration.v1.grant.voucher";
    }

    @Override
    public Class<IntegrationV1GrantVoucherResponse> getResponseClass() {
        return IntegrationV1GrantVoucherResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.POST;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/integration/v1/grant/voucher";
    }
}
