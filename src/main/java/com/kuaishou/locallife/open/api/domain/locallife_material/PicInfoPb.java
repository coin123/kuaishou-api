package com.kuaishou.locallife.open.api.domain.locallife_material;


/**
 * auto generate code
 */
public class PicInfoPb {
    private Integer error_code;
    private String error_msg;
    private String full_path_pic;
    private String relative_path_pic;

    public Integer getError_code() {
        return error_code;
    }

    public void setError_code(Integer error_code) {
        this.error_code = error_code;
    }

    public String getError_msg() {
        return error_msg;
    }

    public void setError_msg(String error_msg) {
        this.error_msg = error_msg;
    }

    public String getFull_path_pic() {
        return full_path_pic;
    }

    public void setFull_path_pic(String full_path_pic) {
        this.full_path_pic = full_path_pic;
    }

    public String getRelative_path_pic() {
        return relative_path_pic;
    }

    public void setRelative_path_pic(String relative_path_pic) {
        this.relative_path_pic = relative_path_pic;
    }

}
