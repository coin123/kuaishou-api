package com.kuaishou.locallife.open.api.response.locallife_shop;


import com.kuaishou.locallife.open.api.domain.locallife_shop.QueryStoreAssociationTaskResData;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class GoodlifePoiAssociateTaskQueryResponse extends KsLocalLifeResponse {

    private QueryStoreAssociationTaskResData data;

    public QueryStoreAssociationTaskResData getData() {
        return data;
    }

    public void setData(QueryStoreAssociationTaskResData data) {
        this.data = data;
    }

}