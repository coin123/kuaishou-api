package com.kuaishou.locallife.open.api.response.locallife_shop;


import com.kuaishou.locallife.open.api.domain.locallife_shop.PoiClaimResData;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class GoodlifeV1PoiPoiClaimResponse extends KsLocalLifeResponse {

    private PoiClaimResData data;

    public PoiClaimResData getData() {
        return data;
    }

    public void setData(PoiClaimResData data) {
        this.data = data;
    }

}