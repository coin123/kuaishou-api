package com.kuaishou.locallife.open.api.domain.locallife_marketing;


/**
 * auto generate code
 */
public class MeituanOrderQuoteReq {
    private String lat;
    private String lng;
    private String out_product_id;
    private Integer count;
    private Integer os;
    private String mtaccesstoken;

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getOut_product_id() {
        return out_product_id;
    }

    public void setOut_product_id(String out_product_id) {
        this.out_product_id = out_product_id;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getOs() {
        return os;
    }

    public void setOs(Integer os) {
        this.os = os;
    }

    public String getMtaccesstoken() {
        return mtaccesstoken;
    }

    public void setMtaccesstoken(String mtaccesstoken) {
        this.mtaccesstoken = mtaccesstoken;
    }

}
