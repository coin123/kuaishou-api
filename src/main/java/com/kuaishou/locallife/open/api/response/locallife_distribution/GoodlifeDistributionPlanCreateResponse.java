package com.kuaishou.locallife.open.api.response.locallife_distribution;


import com.kuaishou.locallife.open.api.domain.locallife_distribution.DataPlanIdList;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class GoodlifeDistributionPlanCreateResponse extends KsLocalLifeResponse {

    private DataPlanIdList data;

    public DataPlanIdList getData() {
        return data;
    }

    public void setData(DataPlanIdList data) {
        this.data = data;
    }

}