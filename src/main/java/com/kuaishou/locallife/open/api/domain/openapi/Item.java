package com.kuaishou.locallife.open.api.domain.openapi;

import java.util.List;

/**
 * auto generate code
 */
public class Item {
    private Long productId;
    private String outId;
    private List<Sku> skuList;

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getOutId() {
        return outId;
    }

    public void setOutId(String outId) {
        this.outId = outId;
    }

    public List<Sku> getSkuList() {
        return skuList;
    }

    public void setSkuList(List<Sku> skuList) {
        this.skuList = skuList;
    }

}
