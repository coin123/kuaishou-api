package com.kuaishou.locallife.open.api.request.locallife_third_code;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.domain.locallife_third_code.RefundapplyCertificate;
import com.kuaishou.locallife.open.api.response.locallife_third_code.IntegrationIntegrationV1RefundApplyDocResponse;

/**
 * auto generate code
 */
public class IntegrationIntegrationV1RefundApplyDocRequest extends AbstractKsLocalLifeRequest<IntegrationIntegrationV1RefundApplyDocResponse> {

    private String order_id;
    private List<RefundapplyCertificate> certificates;
    private String trade_no;
    private Long account_id;

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }
    public List<RefundapplyCertificate> getCertificates() {
        return certificates;
    }

    public void setCertificates(List<RefundapplyCertificate> certificates) {
        this.certificates = certificates;
    }
    public String getTrade_no() {
        return trade_no;
    }

    public void setTrade_no(String trade_no) {
        this.trade_no = trade_no;
    }
    public Long getAccount_id() {
        return account_id;
    }

    public void setAccount_id(Long account_id) {
        this.account_id = account_id;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private String order_id;
        private List<RefundapplyCertificate> certificates;
        private String trade_no;
        private Long account_id;

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public List<RefundapplyCertificate> getCertificates() {
            return certificates;
        }

        public void setCertificates(List<RefundapplyCertificate> certificates) {
            this.certificates = certificates;
        }

        public String getTrade_no() {
            return trade_no;
        }

        public void setTrade_no(String trade_no) {
            this.trade_no = trade_no;
        }

        public Long getAccount_id() {
            return account_id;
        }

        public void setAccount_id(Long account_id) {
            this.account_id = account_id;
        }

    }

    @Override
    public String getApiMethodName() {
        return "integration.integration.v1.refund.apply.doc";
    }

    @Override
    public Class<IntegrationIntegrationV1RefundApplyDocResponse> getResponseClass() {
        return IntegrationIntegrationV1RefundApplyDocResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.POST;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/integration/integration/v1/refund/apply/doc";
    }
}
