package com.kuaishou.locallife.open.api.domain.locallife_third_code;

import java.util.List;

/**
 * auto generate code
 */
public class Resp {
    private Long error_code;
    private String description;
    private Integer result;
    private List<String> codes;
    private CodeInfo[] codes_list;

    public Long getError_code() {
        return error_code;
    }

    public void setError_code(Long error_code) {
        this.error_code = error_code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }

    public List<String> getCodes() {
        return codes;
    }

    public void setCodes(List<String> codes) {
        this.codes = codes;
    }

    public CodeInfo[] getCodes_list() {
        return codes_list;
    }

    public void setCodes_list(CodeInfo[] codes_list) {
        this.codes_list = codes_list;
    }

}
