package com.kuaishou.locallife.open.api.domain.locallife_shop;

import java.util.List;

/**
 * auto generate code
 */
public class Industry {
    private List<NewQualification> qualifications;

    public List<NewQualification> getQualifications() {
        return qualifications;
    }

    public void setQualifications(List<NewQualification> qualifications) {
        this.qualifications = qualifications;
    }

}
