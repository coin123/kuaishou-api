package com.kuaishou.locallife.open.api.domain.settle_deal;

import java.util.List;

/**
 * auto generate code
 */
public class OrderBillByCertificateStruct {
    private Long error_code;
    private String description;
    private List<OrderBillByCertificateRecord> records;
    private Integer cursor;
    private Boolean has_more;

    public Long getError_code() {
        return error_code;
    }

    public void setError_code(Long error_code) {
        this.error_code = error_code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<OrderBillByCertificateRecord> getRecords() {
        return records;
    }

    public void setRecords(List<OrderBillByCertificateRecord> records) {
        this.records = records;
    }

    public Integer getCursor() {
        return cursor;
    }

    public void setCursor(Integer cursor) {
        this.cursor = cursor;
    }

    public Boolean getHas_more() {
        return has_more;
    }

    public void setHas_more(Boolean has_more) {
        this.has_more = has_more;
    }

}
