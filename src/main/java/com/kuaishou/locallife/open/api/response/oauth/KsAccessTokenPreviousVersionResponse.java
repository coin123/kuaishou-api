package com.kuaishou.locallife.open.api.response.oauth;

import java.util.List;

import com.google.gson.annotations.SerializedName;

/**
 * @author shuxiaohui <shuxiaohui@kuaishou.com>
 * Created on 2020-04-09
 * 兼容之前手动配置的版本
 */
public class KsAccessTokenPreviousVersionResponse {
    // api状态码 (1: 成功)
    private int result;

    // 错误标识
    private String error;

    // 错误信息
    @SerializedName("error_msg")
    private String errorMsg;

    // 授权token
    @SerializedName("access_token")
    private String accessToken;

    // 刷新token
    @SerializedName("refresh_token")
    private String refreshToken;

    // 用户对开发者唯一身份标识
    @SerializedName("open_id")
    private String openId;

    // 过期时间(秒)
    @SerializedName("expires_in")
    private long expiresIn;

    /**
     * refresh_token 的过期时间，单位秒，默认为2592000，即30天
     */
    @SerializedName("refresh_token_expires_in")
    private long refreshTokenExpiresIn;

    /**
     * access_token包含的scope
     */
    private List<String> scopes;

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public long getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(long expiresIn) {
        this.expiresIn = expiresIn;
    }

    public long getRefreshTokenExpiresIn() {
        return refreshTokenExpiresIn;
    }

    public void setRefreshTokenExpiresIn(long refreshTokenExpiresIn) {
        this.refreshTokenExpiresIn = refreshTokenExpiresIn;
    }

    public List<String> getScopes() {
        return scopes;
    }

    public void setScopes(List<String> scopes) {
        this.scopes = scopes;
    }
}
