package com.kuaishou.locallife.open.api.common.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 通用http resp dto
 *
 * @author shuxiaohui <shuxiaohui@kuaishou.com>
 * Created on 2020-03-31
 */
public class KsHttpResponseDTO implements Serializable {
    // api返回信息
    private String body;
    // api返回头信息
    private Map<String, List<String>> headers;

    public KsHttpResponseDTO() {
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Map<String, List<String>> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, List<String>> headers) {
        this.headers = headers;
    }
}
