package com.kuaishou.locallife.open.api.request.locallife_distribution;

import java.util.HashMap;
import java.util.Map;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.response.locallife_distribution.GoodlifeDistributionPlanQueryResponse;

/**
 * auto generate code
 */
public class GoodlifeDistributionPlanQueryRequest extends AbstractKsLocalLifeRequest<GoodlifeDistributionPlanQueryResponse> {

    private Long item_id;
    private Integer plan_type;
    private Integer page;
    private Integer size;

    public Long getItem_id() {
        return item_id;
    }

    public void setItem_id(Long item_id) {
        this.item_id = item_id;
    }
    public Integer getPlan_type() {
        return plan_type;
    }

    public void setPlan_type(Integer plan_type) {
        this.plan_type = plan_type;
    }
    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }
    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private Long item_id;
        private Integer plan_type;
        private Integer page;
        private Integer size;

        public Long getItem_id() {
            return item_id;
        }

        public void setItem_id(Long item_id) {
            this.item_id = item_id;
        }

        public Integer getPlan_type() {
            return plan_type;
        }

        public void setPlan_type(Integer plan_type) {
            this.plan_type = plan_type;
        }

        public Integer getPage() {
            return page;
        }

        public void setPage(Integer page) {
            this.page = page;
        }

        public Integer getSize() {
            return size;
        }

        public void setSize(Integer size) {
            this.size = size;
        }

    }

    @Override
    public String getApiMethodName() {
        return "goodlife.distribution.plan.query";
    }

    @Override
    public Class<GoodlifeDistributionPlanQueryResponse> getResponseClass() {
        return GoodlifeDistributionPlanQueryResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.POST;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/goodlife/distribution/plan/query";
    }
}
