package com.kuaishou.locallife.open.api.domain.openapi;

import java.util.List;

/**
 * auto generate code
 */
public class ItemResponse {
    private Long productId;
    private Long outId;
    private Boolean onSale;
    private List<SkuResponse> skuList;

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getOutId() {
        return outId;
    }

    public void setOutId(Long outId) {
        this.outId = outId;
    }

    public Boolean getOnSale() {
        return onSale;
    }

    public void setOnSale(Boolean onSale) {
        this.onSale = onSale;
    }

    public List<SkuResponse> getSkuList() {
        return skuList;
    }

    public void setSkuList(List<SkuResponse> skuList) {
        this.skuList = skuList;
    }

}
