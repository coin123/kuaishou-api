package com.kuaishou.locallife.open.api.response.locallife_trade;


import com.kuaishou.locallife.open.api.domain.locallife_trade.VerifyPrepareDataNew;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class GoodlifeV1FulfilmentCertificatePrepareResponse extends KsLocalLifeResponse {

    private VerifyPrepareDataNew data;

    public VerifyPrepareDataNew getData() {
        return data;
    }

    public void setData(VerifyPrepareDataNew data) {
        this.data = data;
    }

}