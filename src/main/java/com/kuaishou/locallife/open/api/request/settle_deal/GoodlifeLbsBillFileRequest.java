package com.kuaishou.locallife.open.api.request.settle_deal;

import java.util.HashMap;
import java.util.Map;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.response.settle_deal.GoodlifeLbsBillFileResponse;

/**
 * auto generate code
 */
public class GoodlifeLbsBillFileRequest extends AbstractKsLocalLifeRequest<GoodlifeLbsBillFileResponse> {

    private String bill_date;

    public String getBill_date() {
        return bill_date;
    }

    public void setBill_date(String bill_date) {
        this.bill_date = bill_date;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        bizParams.put("bill_date", this.getBill_date());
        
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private String bill_date;

        public String getBill_date() {
            return bill_date;
        }

        public void setBill_date(String bill_date) {
            this.bill_date = bill_date;
        }

    }

    @Override
    public String getApiMethodName() {
        return "goodlife.lbs.bill.file";
    }

    @Override
    public Class<GoodlifeLbsBillFileResponse> getResponseClass() {
        return GoodlifeLbsBillFileResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.GET;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/goodlife/lbs/bill/file";
    }
}
