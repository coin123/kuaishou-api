package com.kuaishou.locallife.open.api.domain.locallife_distribution;


/**
 * auto generate code
 */
public class User {
    private Long user_id;
    private String name;

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
