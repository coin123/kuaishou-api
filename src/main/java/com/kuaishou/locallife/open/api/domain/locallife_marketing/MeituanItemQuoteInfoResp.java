package com.kuaishou.locallife.open.api.domain.locallife_marketing;


/**
 * auto generate code
 */
public class MeituanItemQuoteInfoResp {
    private Long error_code;
    private String description;
    private MeituanItemQuoteInfo[] product_list;

    public Long getError_code() {
        return error_code;
    }

    public void setError_code(Long error_code) {
        this.error_code = error_code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public MeituanItemQuoteInfo[] getProduct_list() {
        return product_list;
    }

    public void setProduct_list(MeituanItemQuoteInfo[] product_list) {
        this.product_list = product_list;
    }

}
