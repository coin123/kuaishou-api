package com.kuaishou.locallife.open.api.response.locallife_shop;


import com.kuaishou.locallife.open.api.domain.locallife_shop.OpenApiPoiShopPoiPbOriginalResp;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class NamekPoiQueryResponse extends KsLocalLifeResponse {

    private OpenApiPoiShopPoiPbOriginalResp data;

    public OpenApiPoiShopPoiPbOriginalResp getData() {
        return data;
    }

    public void setData(OpenApiPoiShopPoiPbOriginalResp data) {
        this.data = data;
    }

}