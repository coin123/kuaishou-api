package com.kuaishou.locallife.open.api.domain.openapi;


/**
 * auto generate code
 */
public class MeituanTokenData {
    private Integer error_code;
    private String description;
    private String access_token;
    private Integer expires_in;
    private String refresh_token;
    private String openid;
    private Integer meituan_code;

    public Integer getError_code() {
        return error_code;
    }

    public void setError_code(Integer error_code) {
        this.error_code = error_code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public Integer getExpires_in() {
        return expires_in;
    }

    public void setExpires_in(Integer expires_in) {
        this.expires_in = expires_in;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public Integer getMeituan_code() {
        return meituan_code;
    }

    public void setMeituan_code(Integer meituan_code) {
        this.meituan_code = meituan_code;
    }

}
