package com.kuaishou.locallife.open.api.domain.locallife_shop;


/**
 * auto generate code
 */
public class ExternalPoiSearchResData {
    private PoiMergeInfoVo poi_info;
    private String ks_poi_id;
    private String ext_poi_id;

    public PoiMergeInfoVo getPoi_info() {
        return poi_info;
    }

    public void setPoi_info(PoiMergeInfoVo poi_info) {
        this.poi_info = poi_info;
    }

    public String getKs_poi_id() {
        return ks_poi_id;
    }

    public void setKs_poi_id(String ks_poi_id) {
        this.ks_poi_id = ks_poi_id;
    }

    public String getExt_poi_id() {
        return ext_poi_id;
    }

    public void setExt_poi_id(String ext_poi_id) {
        this.ext_poi_id = ext_poi_id;
    }

}
