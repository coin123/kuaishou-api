package com.kuaishou.locallife.open.api.domain.settle_deal;


/**
 * auto generate code
 */
public class OpenApiBillQueryledgerAnount {
    private Integer pay;
    private Integer platform_ticket;
    private Integer merchant_ticket;
    private Integer original;
    private Integer ledger_total;
    private Integer total_agent_merchant;
    private Integer total_merchant_platform_service;
    private Integer total_operation_agent;
    private Integer coupon_pay;
    private Integer talent_commission;
    private Integer goods;

    public Integer getPay() {
        return pay;
    }

    public void setPay(Integer pay) {
        this.pay = pay;
    }

    public Integer getPlatform_ticket() {
        return platform_ticket;
    }

    public void setPlatform_ticket(Integer platform_ticket) {
        this.platform_ticket = platform_ticket;
    }

    public Integer getMerchant_ticket() {
        return merchant_ticket;
    }

    public void setMerchant_ticket(Integer merchant_ticket) {
        this.merchant_ticket = merchant_ticket;
    }

    public Integer getOriginal() {
        return original;
    }

    public void setOriginal(Integer original) {
        this.original = original;
    }

    public Integer getLedger_total() {
        return ledger_total;
    }

    public void setLedger_total(Integer ledger_total) {
        this.ledger_total = ledger_total;
    }

    public Integer getTotal_agent_merchant() {
        return total_agent_merchant;
    }

    public void setTotal_agent_merchant(Integer total_agent_merchant) {
        this.total_agent_merchant = total_agent_merchant;
    }

    public Integer getTotal_merchant_platform_service() {
        return total_merchant_platform_service;
    }

    public void setTotal_merchant_platform_service(Integer total_merchant_platform_service) {
        this.total_merchant_platform_service = total_merchant_platform_service;
    }

    public Integer getTotal_operation_agent() {
        return total_operation_agent;
    }

    public void setTotal_operation_agent(Integer total_operation_agent) {
        this.total_operation_agent = total_operation_agent;
    }

    public Integer getCoupon_pay() {
        return coupon_pay;
    }

    public void setCoupon_pay(Integer coupon_pay) {
        this.coupon_pay = coupon_pay;
    }

    public Integer getTalent_commission() {
        return talent_commission;
    }

    public void setTalent_commission(Integer talent_commission) {
        this.talent_commission = talent_commission;
    }

    public Integer getGoods() {
        return goods;
    }

    public void setGoods(Integer goods) {
        this.goods = goods;
    }

}
