package com.kuaishou.locallife.open.api.domain.locallife_material;


/**
 * auto generate code
 */
public class ImageReq {
    private String image_id;
    private String url;

    public String getImage_id() {
        return image_id;
    }

    public void setImage_id(String image_id) {
        this.image_id = image_id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
