package com.kuaishou.locallife.open.api.domain.locallife_goods;


/**
 * auto generate code
 */
public class CategoryDetailBO {
    private CategoryDetail category_detail;
    private Long product_id;
    private String out_product_id;
    private Integer status;
    private Integer check_status;

    public CategoryDetail getCategory_detail() {
        return category_detail;
    }

    public void setCategory_detail(CategoryDetail category_detail) {
        this.category_detail = category_detail;
    }

    public Long getProduct_id() {
        return product_id;
    }

    public void setProduct_id(Long product_id) {
        this.product_id = product_id;
    }

    public String getOut_product_id() {
        return out_product_id;
    }

    public void setOut_product_id(String out_product_id) {
        this.out_product_id = out_product_id;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getCheck_status() {
        return check_status;
    }

    public void setCheck_status(Integer check_status) {
        this.check_status = check_status;
    }

}
