package com.kuaishou.locallife.open.api.response.locallife_third_code;


import com.kuaishou.locallife.open.api.domain.locallife_third_code.ResponseData;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class NamekFulfilmentRefundAuditResponse extends KsLocalLifeResponse {

    private ResponseData data;

    public ResponseData getData() {
        return data;
    }

    public void setData(ResponseData data) {
        this.data = data;
    }

}