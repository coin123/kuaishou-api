package com.kuaishou.locallife.open.api.response.locallife_third_code;


import com.kuaishou.locallife.open.api.domain.locallife_third_code.OrderRefundResultResponse;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class IntegrationLocallifeRefundAuditResultResponse extends KsLocalLifeResponse {

    private OrderRefundResultResponse data;

    public OrderRefundResultResponse getData() {
        return data;
    }

    public void setData(OrderRefundResultResponse data) {
        this.data = data;
    }

}