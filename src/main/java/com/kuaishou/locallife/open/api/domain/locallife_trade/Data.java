package com.kuaishou.locallife.open.api.domain.locallife_trade;

import java.util.List;

/**
 * auto generate code
 */
public class Data {
    private Long error_code;
    private String description;
    private List<VerifyResult> verify_results;

    public Long getError_code() {
        return error_code;
    }

    public void setError_code(Long error_code) {
        this.error_code = error_code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<VerifyResult> getVerify_results() {
        return verify_results;
    }

    public void setVerify_results(List<VerifyResult> verify_results) {
        this.verify_results = verify_results;
    }

}
