package com.kuaishou.locallife.open.api.domain.locallife_third_code;


/**
 * auto generate code
 */
public class Amount {
    private Integer original_amount;
    private Integer pay_amount;
    private Integer order_discount_amount;

    public Integer getOriginal_amount() {
        return original_amount;
    }

    public void setOriginal_amount(Integer original_amount) {
        this.original_amount = original_amount;
    }

    public Integer getPay_amount() {
        return pay_amount;
    }

    public void setPay_amount(Integer pay_amount) {
        this.pay_amount = pay_amount;
    }

    public Integer getOrder_discount_amount() {
        return order_discount_amount;
    }

    public void setOrder_discount_amount(Integer order_discount_amount) {
        this.order_discount_amount = order_discount_amount;
    }

}
