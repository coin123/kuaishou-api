package com.kuaishou.locallife.open.api.domain.locallife_goods;

import java.util.Map;

/**
 * auto generate code
 */
public class BatchGetCategoryRateInfoResponse {
    private Long error_code;
    private String description;
    private Map category_rate;

    public Long getError_code() {
        return error_code;
    }

    public void setError_code(Long error_code) {
        this.error_code = error_code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Map getCategory_rate() {
        return category_rate;
    }

    public void setCategory_rate(Map category_rate) {
        this.category_rate = category_rate;
    }

}
