package com.kuaishou.locallife.open.api.domain.locallife_third_code;


/**
 * auto generate code
 */
public class Tourist {
    private String name;
    private String phone;
    private String id_card;
    private String mask_name;
    private String mask_phone;
    private String encrypt_name;
    private String encrypt_phone;
    private String encrypt_id_card;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getId_card() {
        return id_card;
    }

    public void setId_card(String id_card) {
        this.id_card = id_card;
    }

    public String getMask_name() {
        return mask_name;
    }

    public void setMask_name(String mask_name) {
        this.mask_name = mask_name;
    }

    public String getMask_phone() {
        return mask_phone;
    }

    public void setMask_phone(String mask_phone) {
        this.mask_phone = mask_phone;
    }

    public String getEncrypt_name() {
        return encrypt_name;
    }

    public void setEncrypt_name(String encrypt_name) {
        this.encrypt_name = encrypt_name;
    }

    public String getEncrypt_phone() {
        return encrypt_phone;
    }

    public void setEncrypt_phone(String encrypt_phone) {
        this.encrypt_phone = encrypt_phone;
    }

    public String getEncrypt_id_card() {
        return encrypt_id_card;
    }

    public void setEncrypt_id_card(String encrypt_id_card) {
        this.encrypt_id_card = encrypt_id_card;
    }

}
