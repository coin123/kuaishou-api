package com.kuaishou.locallife.open.api.domain.locallife_trade;


/**
 * auto generate code
 */
public class PrepareAmount {
    private Integer original_amount;
    private Integer pay_amount;
    private Integer merchant_ticket_amount;
    private Integer payment_discount_amount;
    private Integer coupon_pay_amount;
    private Integer platform_ticket_amount;

    public Integer getOriginal_amount() {
        return original_amount;
    }

    public void setOriginal_amount(Integer original_amount) {
        this.original_amount = original_amount;
    }

    public Integer getPay_amount() {
        return pay_amount;
    }

    public void setPay_amount(Integer pay_amount) {
        this.pay_amount = pay_amount;
    }

    public Integer getMerchant_ticket_amount() {
        return merchant_ticket_amount;
    }

    public void setMerchant_ticket_amount(Integer merchant_ticket_amount) {
        this.merchant_ticket_amount = merchant_ticket_amount;
    }

    public Integer getPayment_discount_amount() {
        return payment_discount_amount;
    }

    public void setPayment_discount_amount(Integer payment_discount_amount) {
        this.payment_discount_amount = payment_discount_amount;
    }

    public Integer getCoupon_pay_amount() {
        return coupon_pay_amount;
    }

    public void setCoupon_pay_amount(Integer coupon_pay_amount) {
        this.coupon_pay_amount = coupon_pay_amount;
    }

    public Integer getPlatform_ticket_amount() {
        return platform_ticket_amount;
    }

    public void setPlatform_ticket_amount(Integer platform_ticket_amount) {
        this.platform_ticket_amount = platform_ticket_amount;
    }

}
