package com.kuaishou.locallife.open.api.response.locallife_third_code;


import com.kuaishou.locallife.open.api.domain.locallife_third_code.DataResp;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class IntegrationItemConsistencyQueryDocResponse extends KsLocalLifeResponse {

    private DataResp data;

    public DataResp getData() {
        return data;
    }

    public void setData(DataResp data) {
        this.data = data;
    }

}