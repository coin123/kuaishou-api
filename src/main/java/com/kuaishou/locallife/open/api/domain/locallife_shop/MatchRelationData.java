package com.kuaishou.locallife.open.api.domain.locallife_shop;


/**
 * auto generate code
 */
public class MatchRelationData {
    private String match_poi_id;
    private Integer match_relation_status;
    private String ext_poi_id;

    public String getMatch_poi_id() {
        return match_poi_id;
    }

    public void setMatch_poi_id(String match_poi_id) {
        this.match_poi_id = match_poi_id;
    }

    public Integer getMatch_relation_status() {
        return match_relation_status;
    }

    public void setMatch_relation_status(Integer match_relation_status) {
        this.match_relation_status = match_relation_status;
    }

    public String getExt_poi_id() {
        return ext_poi_id;
    }

    public void setExt_poi_id(String ext_poi_id) {
        this.ext_poi_id = ext_poi_id;
    }

}
