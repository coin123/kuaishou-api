package com.kuaishou.locallife.open.api.request.locallife_trade;

import java.util.HashMap;
import java.util.Map;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.response.locallife_trade.GoodlifeV1FulfilmentCertificateQueryResponse;

/**
 * auto generate code
 */
public class GoodlifeV1FulfilmentCertificateQueryRequest extends AbstractKsLocalLifeRequest<GoodlifeV1FulfilmentCertificateQueryResponse> {

    private String encrypted_code;
    private String order_id;

    public String getEncrypted_code() {
        return encrypted_code;
    }

    public void setEncrypted_code(String encrypted_code) {
        this.encrypted_code = encrypted_code;
    }
    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        bizParams.put("encrypted_code", this.getEncrypted_code());
        
        bizParams.put("order_id", this.getOrder_id());
        
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private String encrypted_code;
        private String order_id;

        public String getEncrypted_code() {
            return encrypted_code;
        }

        public void setEncrypted_code(String encrypted_code) {
            this.encrypted_code = encrypted_code;
        }

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

    }

    @Override
    public String getApiMethodName() {
        return "goodlife.v1.fulfilment.certificate.query";
    }

    @Override
    public Class<GoodlifeV1FulfilmentCertificateQueryResponse> getResponseClass() {
        return GoodlifeV1FulfilmentCertificateQueryResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.GET;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/goodlife/v1/fulfilment/certificate/query";
    }
}
