package com.kuaishou.locallife.open.api.response.locallife_trade;


import com.kuaishou.locallife.open.api.domain.locallife_trade.BatchQueryCertificateRespData;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class GoodlifeV1FulfilmentCertificateQueryResponse extends KsLocalLifeResponse {

    private BatchQueryCertificateRespData data;

    public BatchQueryCertificateRespData getData() {
        return data;
    }

    public void setData(BatchQueryCertificateRespData data) {
        this.data = data;
    }

}