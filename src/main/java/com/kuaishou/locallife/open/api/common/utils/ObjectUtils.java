package com.kuaishou.locallife.open.api.common.utils;

/**
 * @author qishengwang <qishengwang@kuaishou.com>
 * Created on 2021-01-07
 */
public class ObjectUtils {

    public static <T> T requireNonNull(T obj, String message) {
        if (obj == null)
            throw new NullPointerException(message);
        return obj;
    }
}
