package com.kuaishou.locallife.open.api.domain.locallife_third_code;

import java.util.List;

/**
 * auto generate code
 */
public class ItemList {
    private Long product_id;
    private String out_id;
    private List<SkuList> sku_list;

    public Long getProduct_id() {
        return product_id;
    }

    public void setProduct_id(Long product_id) {
        this.product_id = product_id;
    }

    public String getOut_id() {
        return out_id;
    }

    public void setOut_id(String out_id) {
        this.out_id = out_id;
    }

    public List<SkuList> getSku_list() {
        return sku_list;
    }

    public void setSku_list(List<SkuList> sku_list) {
        this.sku_list = sku_list;
    }

}
