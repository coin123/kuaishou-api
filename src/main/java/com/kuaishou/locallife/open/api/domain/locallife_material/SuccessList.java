package com.kuaishou.locallife.open.api.domain.locallife_material;


/**
 * auto generate code
 */
public class SuccessList {
    private String image_id;
    private String pic_url;
    private Boolean is_new;

    public String getImage_id() {
        return image_id;
    }

    public void setImage_id(String image_id) {
        this.image_id = image_id;
    }

    public String getPic_url() {
        return pic_url;
    }

    public void setPic_url(String pic_url) {
        this.pic_url = pic_url;
    }

    public Boolean getIs_new() {
        return is_new;
    }

    public void setIs_new(Boolean is_new) {
        this.is_new = is_new;
    }

}
