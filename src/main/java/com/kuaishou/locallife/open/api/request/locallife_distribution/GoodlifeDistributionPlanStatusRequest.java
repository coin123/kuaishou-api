package com.kuaishou.locallife.open.api.request.locallife_distribution;

import java.util.HashMap;
import java.util.Map;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.response.locallife_distribution.GoodlifeDistributionPlanStatusResponse;

/**
 * auto generate code
 */
public class GoodlifeDistributionPlanStatusRequest extends AbstractKsLocalLifeRequest<GoodlifeDistributionPlanStatusResponse> {

    private Long plan_id;
    private Integer plan_type;
    private Integer status;

    public Long getPlan_id() {
        return plan_id;
    }

    public void setPlan_id(Long plan_id) {
        this.plan_id = plan_id;
    }
    public Integer getPlan_type() {
        return plan_type;
    }

    public void setPlan_type(Integer plan_type) {
        this.plan_type = plan_type;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private Long plan_id;
        private Integer plan_type;
        private Integer status;

        public Long getPlan_id() {
            return plan_id;
        }

        public void setPlan_id(Long plan_id) {
            this.plan_id = plan_id;
        }

        public Integer getPlan_type() {
            return plan_type;
        }

        public void setPlan_type(Integer plan_type) {
            this.plan_type = plan_type;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

    }

    @Override
    public String getApiMethodName() {
        return "goodlife.distribution.plan.status";
    }

    @Override
    public Class<GoodlifeDistributionPlanStatusResponse> getResponseClass() {
        return GoodlifeDistributionPlanStatusResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.POST;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/goodlife/distribution/plan/status";
    }
}
