package com.kuaishou.locallife.open.api.domain.locallife_distribution;


/**
 * auto generate code
 */
public class Result {
    private Integer error_code;
    private String description;

    public Integer getError_code() {
        return error_code;
    }

    public void setError_code(Integer error_code) {
        this.error_code = error_code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
