package com.kuaishou.locallife.open.api.request.locallife_goods;

import java.util.HashMap;
import java.util.Map;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.response.locallife_goods.GoodlifeV1ItemFreeauditUpdateResponse;

/**
 * auto generate code
 */
public class GoodlifeV1ItemFreeauditUpdateRequest extends AbstractKsLocalLifeRequest<GoodlifeV1ItemFreeauditUpdateResponse> {

    private String out_id;
    private String out_sku_id;
    private Integer stock;

    public String getOut_id() {
        return out_id;
    }

    public void setOut_id(String out_id) {
        this.out_id = out_id;
    }
    public String getOut_sku_id() {
        return out_sku_id;
    }

    public void setOut_sku_id(String out_sku_id) {
        this.out_sku_id = out_sku_id;
    }
    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private String out_id;
        private String out_sku_id;
        private Integer stock;

        public String getOut_id() {
            return out_id;
        }

        public void setOut_id(String out_id) {
            this.out_id = out_id;
        }

        public String getOut_sku_id() {
            return out_sku_id;
        }

        public void setOut_sku_id(String out_sku_id) {
            this.out_sku_id = out_sku_id;
        }

        public Integer getStock() {
            return stock;
        }

        public void setStock(Integer stock) {
            this.stock = stock;
        }

    }

    @Override
    public String getApiMethodName() {
        return "goodlife.v1.item.freeaudit.update";
    }

    @Override
    public Class<GoodlifeV1ItemFreeauditUpdateResponse> getResponseClass() {
        return GoodlifeV1ItemFreeauditUpdateResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.POST;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/goodlife/v1/item/freeaudit/update";
    }
}
