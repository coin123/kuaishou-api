package com.kuaishou.locallife.open.api.domain.locallife_third_code;

import java.util.List;

/**
 * auto generate code
 */
public class DataResp {
    private Long error_code;
    private String description;
    private List<ItemResp> datas;

    public Long getError_code() {
        return error_code;
    }

    public void setError_code(Long error_code) {
        this.error_code = error_code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ItemResp> getDatas() {
        return datas;
    }

    public void setDatas(List<ItemResp> datas) {
        this.datas = datas;
    }

}
