package com.kuaishou.locallife.open.api.domain.locallife_marketing;


/**
 * auto generate code
 */
public class MeituanItemCardInfo {
    private String title;
    private Integer min_consumption;
    private Integer reduce;
    private Long expire_time;
    private String limit_info;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getMin_consumption() {
        return min_consumption;
    }

    public void setMin_consumption(Integer min_consumption) {
        this.min_consumption = min_consumption;
    }

    public Integer getReduce() {
        return reduce;
    }

    public void setReduce(Integer reduce) {
        this.reduce = reduce;
    }

    public Long getExpire_time() {
        return expire_time;
    }

    public void setExpire_time(Long expire_time) {
        this.expire_time = expire_time;
    }

    public String getLimit_info() {
        return limit_info;
    }

    public void setLimit_info(String limit_info) {
        this.limit_info = limit_info;
    }

}
