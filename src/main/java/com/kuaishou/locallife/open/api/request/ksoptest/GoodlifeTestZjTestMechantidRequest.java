package com.kuaishou.locallife.open.api.request.ksoptest;

import java.util.HashMap;
import java.util.Map;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.response.ksoptest.GoodlifeTestZjTestMechantidResponse;

/**
 * auto generate code
 */
public class GoodlifeTestZjTestMechantidRequest extends AbstractKsLocalLifeRequest<GoodlifeTestZjTestMechantidResponse> {

    private String out_sku_id;
    private String user_id;
    private String out_product_id;
    private Integer stock;

    public String getOut_sku_id() {
        return out_sku_id;
    }

    public void setOut_sku_id(String out_sku_id) {
        this.out_sku_id = out_sku_id;
    }
    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
    public String getOut_product_id() {
        return out_product_id;
    }

    public void setOut_product_id(String out_product_id) {
        this.out_product_id = out_product_id;
    }
    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private String out_sku_id;
        private String user_id;
        private String out_product_id;
        private Integer stock;

        public String getOut_sku_id() {
            return out_sku_id;
        }

        public void setOut_sku_id(String out_sku_id) {
            this.out_sku_id = out_sku_id;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getOut_product_id() {
            return out_product_id;
        }

        public void setOut_product_id(String out_product_id) {
            this.out_product_id = out_product_id;
        }

        public Integer getStock() {
            return stock;
        }

        public void setStock(Integer stock) {
            this.stock = stock;
        }

    }

    @Override
    public String getApiMethodName() {
        return "goodlife.test.zj.test.mechantid";
    }

    @Override
    public Class<GoodlifeTestZjTestMechantidResponse> getResponseClass() {
        return GoodlifeTestZjTestMechantidResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.POST;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/goodlife/test/zj/test/mechantid";
    }
}
