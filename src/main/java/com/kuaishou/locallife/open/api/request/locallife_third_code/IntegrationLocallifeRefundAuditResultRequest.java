package com.kuaishou.locallife.open.api.request.locallife_third_code;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.domain.locallife_third_code.NoticeList;
import com.kuaishou.locallife.open.api.response.locallife_third_code.IntegrationLocallifeRefundAuditResultResponse;

/**
 * auto generate code
 */
public class IntegrationLocallifeRefundAuditResultRequest extends AbstractKsLocalLifeRequest<IntegrationLocallifeRefundAuditResultResponse> {

    private String order_id;
    private List<NoticeList> notice_list;
    private String app_key;
    private Long account_id;

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }
    public List<NoticeList> getNotice_list() {
        return notice_list;
    }

    public void setNotice_list(List<NoticeList> notice_list) {
        this.notice_list = notice_list;
    }
    public String getApp_key() {
        return app_key;
    }

    public void setApp_key(String app_key) {
        this.app_key = app_key;
    }
    public Long getAccount_id() {
        return account_id;
    }

    public void setAccount_id(Long account_id) {
        this.account_id = account_id;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private String order_id;
        private List<NoticeList> notice_list;
        private String app_key;
        private Long account_id;

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public List<NoticeList> getNotice_list() {
            return notice_list;
        }

        public void setNotice_list(List<NoticeList> notice_list) {
            this.notice_list = notice_list;
        }

        public String getApp_key() {
            return app_key;
        }

        public void setApp_key(String app_key) {
            this.app_key = app_key;
        }

        public Long getAccount_id() {
            return account_id;
        }

        public void setAccount_id(Long account_id) {
            this.account_id = account_id;
        }

    }

    @Override
    public String getApiMethodName() {
        return "integration.locallife.refund.audit.result";
    }

    @Override
    public Class<IntegrationLocallifeRefundAuditResultResponse> getResponseClass() {
        return IntegrationLocallifeRefundAuditResultResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.POST;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/integration/locallife/refund/audit/result";
    }
}
