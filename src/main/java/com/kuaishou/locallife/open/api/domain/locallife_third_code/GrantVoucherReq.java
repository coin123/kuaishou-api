package com.kuaishou.locallife.open.api.domain.locallife_third_code;


/**
 * auto generate code
 */
public class GrantVoucherReq {
    private Long orderId;
    private Integer count;
    private Integer startTime;
    private Integer expireTime;
    private String skuId;
    private String thirdSkuId;
    private SkuInfo sku;
    private GrantVoucherAmount amount;
    private GrantVoucherTourist[] tourists;
    private String outOrderId;
    private Long accountId;
    private GrantVoucherContact contact;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getStartTime() {
        return startTime;
    }

    public void setStartTime(Integer startTime) {
        this.startTime = startTime;
    }

    public Integer getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Integer expireTime) {
        this.expireTime = expireTime;
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public String getThirdSkuId() {
        return thirdSkuId;
    }

    public void setThirdSkuId(String thirdSkuId) {
        this.thirdSkuId = thirdSkuId;
    }

    public SkuInfo getSku() {
        return sku;
    }

    public void setSku(SkuInfo sku) {
        this.sku = sku;
    }

    public GrantVoucherAmount getAmount() {
        return amount;
    }

    public void setAmount(GrantVoucherAmount amount) {
        this.amount = amount;
    }

    public GrantVoucherTourist[] getTourists() {
        return tourists;
    }

    public void setTourists(GrantVoucherTourist[] tourists) {
        this.tourists = tourists;
    }

    public String getOutOrderId() {
        return outOrderId;
    }

    public void setOutOrderId(String outOrderId) {
        this.outOrderId = outOrderId;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public GrantVoucherContact getContact() {
        return contact;
    }

    public void setContact(GrantVoucherContact contact) {
        this.contact = contact;
    }

}
