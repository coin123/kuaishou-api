package com.kuaishou.locallife.open.api.response.openapi;


import com.kuaishou.locallife.open.api.domain.openapi.GatewayGreyResponse;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class GoodlifeGatewayGrayTestResponse extends KsLocalLifeResponse {

    private GatewayGreyResponse data;

    public GatewayGreyResponse getData() {
        return data;
    }

    public void setData(GatewayGreyResponse data) {
        this.data = data;
    }

}