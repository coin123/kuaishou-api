package com.kuaishou.locallife.open.api.domain.locallife_shop;


/**
 * auto generate code
 */
public class StoreAssociationInfo {
    private String ext_poi_id;
    private String poi_id;

    public String getExt_poi_id() {
        return ext_poi_id;
    }

    public void setExt_poi_id(String ext_poi_id) {
        this.ext_poi_id = ext_poi_id;
    }

    public String getPoi_id() {
        return poi_id;
    }

    public void setPoi_id(String poi_id) {
        this.poi_id = poi_id;
    }

}
