package com.kuaishou.locallife.open.api.response.locallife_goods;


import com.kuaishou.locallife.open.api.domain.locallife_goods.GetItemTemplateResponse;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class GoodlifeV1ItemTemplateQueryResponse extends KsLocalLifeResponse {

    private GetItemTemplateResponse data;

    public GetItemTemplateResponse getData() {
        return data;
    }

    public void setData(GetItemTemplateResponse data) {
        this.data = data;
    }

}