package com.kuaishou.locallife.open.api.request.locallife_trade;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.response.locallife_trade.NamekFulfilmentQueryCertificateResponse;

/**
 * auto generate code
 */
public class NamekFulfilmentQueryCertificateRequest extends AbstractKsLocalLifeRequest<NamekFulfilmentQueryCertificateResponse> {

    private String encypted_code;
    private List<String> codes;
    private String order_id;

    public String getEncypted_code() {
        return encypted_code;
    }

    public void setEncypted_code(String encypted_code) {
        this.encypted_code = encypted_code;
    }
    public List<String> getCodes() {
        return codes;
    }

    public void setCodes(List<String> codes) {
        this.codes = codes;
    }
    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        bizParams.put("encypted_code", this.getEncypted_code());
        
        
        bizParams.put("codes", this.getCodes() == null ? "" : this.getCodes().toString());
        bizParams.put("order_id", this.getOrder_id());
        
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private String encypted_code;
        private List<String> codes;
        private String order_id;

        public String getEncypted_code() {
            return encypted_code;
        }

        public void setEncypted_code(String encypted_code) {
            this.encypted_code = encypted_code;
        }

        public List<String> getCodes() {
            return codes;
        }

        public void setCodes(List<String> codes) {
            this.codes = codes;
        }

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

    }

    @Override
    public String getApiMethodName() {
        return "namek.fulfilment.query.certificate";
    }

    @Override
    public Class<NamekFulfilmentQueryCertificateResponse> getResponseClass() {
        return NamekFulfilmentQueryCertificateResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.GET;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/namek/fulfilment/query/certificate";
    }
}
