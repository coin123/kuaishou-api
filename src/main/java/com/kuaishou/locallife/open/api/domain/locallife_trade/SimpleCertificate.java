package com.kuaishou.locallife.open.api.domain.locallife_trade;


/**
 * auto generate code
 */
public class SimpleCertificate {
    private String encrypted_code;
    private Long expire_time;
    private PrepareAmount amount;
    private Sku sku;

    public String getEncrypted_code() {
        return encrypted_code;
    }

    public void setEncrypted_code(String encrypted_code) {
        this.encrypted_code = encrypted_code;
    }

    public Long getExpire_time() {
        return expire_time;
    }

    public void setExpire_time(Long expire_time) {
        this.expire_time = expire_time;
    }

    public PrepareAmount getAmount() {
        return amount;
    }

    public void setAmount(PrepareAmount amount) {
        this.amount = amount;
    }

    public Sku getSku() {
        return sku;
    }

    public void setSku(Sku sku) {
        this.sku = sku;
    }

}
