package com.kuaishou.locallife.open.api;

/**
 * 快手电商api异常
 *
 * @author shuxiaohui <shuxiaohui@kuaishou.com>
 * Created on 2020-03-30
 */
public class KsLocalLifeApiException extends Exception {
    // 错误码
    private int errorCode;
    // 错误信息
    private String errorMsg;

    public KsLocalLifeApiException() {
    }

    public KsLocalLifeApiException(String message, Throwable cause) {
        super(message, cause);
    }

    public KsLocalLifeApiException(String message) {
        super(message);
    }

    public KsLocalLifeApiException(Throwable cause) {
        super(cause);
    }

    public KsLocalLifeApiException(int errorCode, String errorMsg) {
        super(errorCode + ":" + errorMsg);
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    public int getErrorCode() {
        return this.errorCode;
    }

    public String getErrorMsg() {
        return this.errorMsg;
    }
}
