package com.kuaishou.locallife.open.api.domain.openapi;


/**
 * auto generate code
 */
public class SignatureInfo {
    private String error_msg;
    private String plain;
    private String search_index;
    private Integer error_code;

    public String getError_msg() {
        return error_msg;
    }

    public void setError_msg(String error_msg) {
        this.error_msg = error_msg;
    }

    public String getPlain() {
        return plain;
    }

    public void setPlain(String plain) {
        this.plain = plain;
    }

    public String getSearch_index() {
        return search_index;
    }

    public void setSearch_index(String search_index) {
        this.search_index = search_index;
    }

    public Integer getError_code() {
        return error_code;
    }

    public void setError_code(Integer error_code) {
        this.error_code = error_code;
    }

}
