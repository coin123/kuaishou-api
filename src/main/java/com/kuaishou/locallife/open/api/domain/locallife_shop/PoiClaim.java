package com.kuaishou.locallife.open.api.domain.locallife_shop;


/**
 * auto generate code
 */
public class PoiClaim {
    private License license;
    private Qualification qualification;
    private LegalPerson legal_person;
    private OwnerInfo owner_info;
    private Industry industry;
    private String poi_id;
    private String major_industry_code;

    public License getLicense() {
        return license;
    }

    public void setLicense(License license) {
        this.license = license;
    }

    public Qualification getQualification() {
        return qualification;
    }

    public void setQualification(Qualification qualification) {
        this.qualification = qualification;
    }

    public LegalPerson getLegal_person() {
        return legal_person;
    }

    public void setLegal_person(LegalPerson legal_person) {
        this.legal_person = legal_person;
    }

    public OwnerInfo getOwner_info() {
        return owner_info;
    }

    public void setOwner_info(OwnerInfo owner_info) {
        this.owner_info = owner_info;
    }

    public Industry getIndustry() {
        return industry;
    }

    public void setIndustry(Industry industry) {
        this.industry = industry;
    }

    public String getPoi_id() {
        return poi_id;
    }

    public void setPoi_id(String poi_id) {
        this.poi_id = poi_id;
    }

    public String getMajor_industry_code() {
        return major_industry_code;
    }

    public void setMajor_industry_code(String major_industry_code) {
        this.major_industry_code = major_industry_code;
    }

}
