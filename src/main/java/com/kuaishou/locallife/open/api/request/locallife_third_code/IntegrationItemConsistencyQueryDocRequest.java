package com.kuaishou.locallife.open.api.request.locallife_third_code;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.domain.locallife_third_code.ItemList;
import com.kuaishou.locallife.open.api.response.locallife_third_code.IntegrationItemConsistencyQueryDocResponse;

/**
 * auto generate code
 */
public class IntegrationItemConsistencyQueryDocRequest extends AbstractKsLocalLifeRequest<IntegrationItemConsistencyQueryDocResponse> {

    private Long account_id;
    private List<ItemList> datas;

    public Long getAccount_id() {
        return account_id;
    }

    public void setAccount_id(Long account_id) {
        this.account_id = account_id;
    }
    public List<ItemList> getDatas() {
        return datas;
    }

    public void setDatas(List<ItemList> datas) {
        this.datas = datas;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private Long account_id;
        private List<ItemList> datas;

        public Long getAccount_id() {
            return account_id;
        }

        public void setAccount_id(Long account_id) {
            this.account_id = account_id;
        }

        public List<ItemList> getDatas() {
            return datas;
        }

        public void setDatas(List<ItemList> datas) {
            this.datas = datas;
        }

    }

    @Override
    public String getApiMethodName() {
        return "integration.item.consistency.query.doc";
    }

    @Override
    public Class<IntegrationItemConsistencyQueryDocResponse> getResponseClass() {
        return IntegrationItemConsistencyQueryDocResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.POST;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/integration/item/consistency/query/doc";
    }
}
