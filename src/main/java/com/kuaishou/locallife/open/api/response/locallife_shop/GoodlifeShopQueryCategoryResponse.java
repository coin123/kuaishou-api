package com.kuaishou.locallife.open.api.response.locallife_shop;


import com.kuaishou.locallife.open.api.domain.locallife_shop.QueryCategoryResData;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class GoodlifeShopQueryCategoryResponse extends KsLocalLifeResponse {

    private QueryCategoryResData data;

    public QueryCategoryResData getData() {
        return data;
    }

    public void setData(QueryCategoryResData data) {
        this.data = data;
    }

}