package com.kuaishou.locallife.open.api.domain.locallife_shop;


/**
 * auto generate code
 */
public class BusinessLicenseInfo {
    private String business_license_name;
    private String business_license_picurl;
    private String business_license_back_picurl;
    private String business_company_address;
    private String begin_date;
    private String business_license_type;
    private String end_date;
    private String business_license_code;

    public String getBusiness_license_name() {
        return business_license_name;
    }

    public void setBusiness_license_name(String business_license_name) {
        this.business_license_name = business_license_name;
    }

    public String getBusiness_license_picurl() {
        return business_license_picurl;
    }

    public void setBusiness_license_picurl(String business_license_picurl) {
        this.business_license_picurl = business_license_picurl;
    }

    public String getBusiness_license_back_picurl() {
        return business_license_back_picurl;
    }

    public void setBusiness_license_back_picurl(String business_license_back_picurl) {
        this.business_license_back_picurl = business_license_back_picurl;
    }

    public String getBusiness_company_address() {
        return business_company_address;
    }

    public void setBusiness_company_address(String business_company_address) {
        this.business_company_address = business_company_address;
    }

    public String getBegin_date() {
        return begin_date;
    }

    public void setBegin_date(String begin_date) {
        this.begin_date = begin_date;
    }

    public String getBusiness_license_type() {
        return business_license_type;
    }

    public void setBusiness_license_type(String business_license_type) {
        this.business_license_type = business_license_type;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getBusiness_license_code() {
        return business_license_code;
    }

    public void setBusiness_license_code(String business_license_code) {
        this.business_license_code = business_license_code;
    }

}
