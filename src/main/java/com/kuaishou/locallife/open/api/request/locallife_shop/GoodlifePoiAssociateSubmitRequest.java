package com.kuaishou.locallife.open.api.request.locallife_shop;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.domain.locallife_shop.StoreAssociationInfo;
import com.kuaishou.locallife.open.api.response.locallife_shop.GoodlifePoiAssociateSubmitResponse;

/**
 * auto generate code
 */
public class GoodlifePoiAssociateSubmitRequest extends AbstractKsLocalLifeRequest<GoodlifePoiAssociateSubmitResponse> {

    private List<StoreAssociationInfo> datas;

    public List<StoreAssociationInfo> getDatas() {
        return datas;
    }

    public void setDatas(List<StoreAssociationInfo> datas) {
        this.datas = datas;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private List<StoreAssociationInfo> datas;

        public List<StoreAssociationInfo> getDatas() {
            return datas;
        }

        public void setDatas(List<StoreAssociationInfo> datas) {
            this.datas = datas;
        }

    }

    @Override
    public String getApiMethodName() {
        return "goodlife.poi.associate.submit";
    }

    @Override
    public Class<GoodlifePoiAssociateSubmitResponse> getResponseClass() {
        return GoodlifePoiAssociateSubmitResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.POST;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/goodlife/poi/associate/submit";
    }
}
