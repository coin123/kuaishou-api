package com.kuaishou.locallife.open.api.domain.locallife_goods;


/**
 * auto generate code
 */
public class ItemType {
    private String text;
    private Integer value;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

}
