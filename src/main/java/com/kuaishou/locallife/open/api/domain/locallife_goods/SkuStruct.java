package com.kuaishou.locallife.open.api.domain.locallife_goods;

import java.util.Map;

/**
 * auto generate code
 */
public class SkuStruct {
    private String out_sku_id;
    private Integer origin_amount;
    private Integer actual_amount;
    private String sku_id;
    private String sku_name;
    private StockStruct stock;
    private Map attr_key_value_map;

    public String getOut_sku_id() {
        return out_sku_id;
    }

    public void setOut_sku_id(String out_sku_id) {
        this.out_sku_id = out_sku_id;
    }

    public Integer getOrigin_amount() {
        return origin_amount;
    }

    public void setOrigin_amount(Integer origin_amount) {
        this.origin_amount = origin_amount;
    }

    public Integer getActual_amount() {
        return actual_amount;
    }

    public void setActual_amount(Integer actual_amount) {
        this.actual_amount = actual_amount;
    }

    public String getSku_id() {
        return sku_id;
    }

    public void setSku_id(String sku_id) {
        this.sku_id = sku_id;
    }

    public String getSku_name() {
        return sku_name;
    }

    public void setSku_name(String sku_name) {
        this.sku_name = sku_name;
    }

    public StockStruct getStock() {
        return stock;
    }

    public void setStock(StockStruct stock) {
        this.stock = stock;
    }

    public Map getAttr_key_value_map() {
        return attr_key_value_map;
    }

    public void setAttr_key_value_map(Map attr_key_value_map) {
        this.attr_key_value_map = attr_key_value_map;
    }

}
