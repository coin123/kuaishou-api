package com.kuaishou.locallife.open.api.request.locallife_third_code;

import java.util.HashMap;
import java.util.Map;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.response.locallife_third_code.GoodlifeV1FulfilmentRefundAuditResponse;

/**
 * auto generate code
 */
public class GoodlifeV1FulfilmentRefundAuditRequest extends AbstractKsLocalLifeRequest<GoodlifeV1FulfilmentRefundAuditResponse> {

    private String certificate_id;
    private Integer result;
    private String reason;
    private String code;

    public String getCertificate_id() {
        return certificate_id;
    }

    public void setCertificate_id(String certificate_id) {
        this.certificate_id = certificate_id;
    }
    public Integer getResult() {
        return result;
    }

    public void setResult(Integer result) {
        this.result = result;
    }
    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private String certificate_id;
        private Integer result;
        private String reason;
        private String code;

        public String getCertificate_id() {
            return certificate_id;
        }

        public void setCertificate_id(String certificate_id) {
            this.certificate_id = certificate_id;
        }

        public Integer getResult() {
            return result;
        }

        public void setResult(Integer result) {
            this.result = result;
        }

        public String getReason() {
            return reason;
        }

        public void setReason(String reason) {
            this.reason = reason;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

    }

    @Override
    public String getApiMethodName() {
        return "goodlife.v1.fulfilment.refund.audit";
    }

    @Override
    public Class<GoodlifeV1FulfilmentRefundAuditResponse> getResponseClass() {
        return GoodlifeV1FulfilmentRefundAuditResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.POST;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/goodlife/v1/fulfilment/refund/audit";
    }
}
