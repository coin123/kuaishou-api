package com.kuaishou.locallife.open.api.domain.locallife_distribution;

import java.util.List;

/**
 * auto generate code
 */
public class PlanDetailData {
    private Integer error_code;
    private String description;
    private List<PlanDetail> plans;
    private DistributionPage page;

    public Integer getError_code() {
        return error_code;
    }

    public void setError_code(Integer error_code) {
        this.error_code = error_code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<PlanDetail> getPlans() {
        return plans;
    }

    public void setPlans(List<PlanDetail> plans) {
        this.plans = plans;
    }

    public DistributionPage getPage() {
        return page;
    }

    public void setPage(DistributionPage page) {
        this.page = page;
    }

}
