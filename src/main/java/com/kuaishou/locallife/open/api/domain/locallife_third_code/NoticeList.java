package com.kuaishou.locallife.open.api.domain.locallife_third_code;


/**
 * auto generate code
 */
public class NoticeList {
    private String certificate_id;
    private String code;
    private Long apply_time;
    private Long audit_time;
    private Integer audit_result;
    private Integer refund_type;
    private String refund_reason;

    public String getCertificate_id() {
        return certificate_id;
    }

    public void setCertificate_id(String certificate_id) {
        this.certificate_id = certificate_id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getApply_time() {
        return apply_time;
    }

    public void setApply_time(Long apply_time) {
        this.apply_time = apply_time;
    }

    public Long getAudit_time() {
        return audit_time;
    }

    public void setAudit_time(Long audit_time) {
        this.audit_time = audit_time;
    }

    public Integer getAudit_result() {
        return audit_result;
    }

    public void setAudit_result(Integer audit_result) {
        this.audit_result = audit_result;
    }

    public Integer getRefund_type() {
        return refund_type;
    }

    public void setRefund_type(Integer refund_type) {
        this.refund_type = refund_type;
    }

    public String getRefund_reason() {
        return refund_reason;
    }

    public void setRefund_reason(String refund_reason) {
        this.refund_reason = refund_reason;
    }

}
