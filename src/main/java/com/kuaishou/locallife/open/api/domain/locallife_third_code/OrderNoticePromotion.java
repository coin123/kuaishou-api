package com.kuaishou.locallife.open.api.domain.locallife_third_code;


/**
 * auto generate code
 */
public class OrderNoticePromotion {
    private String promotionId;
    private String activityProductId;
    private Integer promotionType;

    public String getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(String promotionId) {
        this.promotionId = promotionId;
    }

    public String getActivityProductId() {
        return activityProductId;
    }

    public void setActivityProductId(String activityProductId) {
        this.activityProductId = activityProductId;
    }

    public Integer getPromotionType() {
        return promotionType;
    }

    public void setPromotionType(Integer promotionType) {
        this.promotionType = promotionType;
    }

}
