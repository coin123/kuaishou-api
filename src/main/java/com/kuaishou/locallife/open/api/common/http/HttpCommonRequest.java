package com.kuaishou.locallife.open.api.common.http;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.kuaishou.locallife.open.api.common.HttpRequestMethod;

/**
 * 通用请求dto
 *
 * @author shuxiaohui <shuxiaohui@kuaishou.com>
 * Created on 2020-03-31
 */
public class HttpCommonRequest implements Serializable {
    private String apiMethodName;
    private Map<String, String> apiParams;

    private String apiReqJson;
    private Map<String, String> headerParams;
    /**
     * API path
     */
    private String apiSpecialPath;
    private HttpRequestMethod httpRequestMethod;

    public HttpCommonRequest() {
    }

    public HttpCommonRequest(String apiMethodName, String contentType, long apiMethodVersion,
            Map<String, String> apiParams,
            Map<String, String> headerParams, long timestamp, String apiSpecialPath,
            HttpRequestMethod httpRequestMethod) {
        this.apiMethodName = apiMethodName;
        this.apiParams = apiParams;
        this.headerParams = headerParams;
        this.apiSpecialPath = apiSpecialPath;
        this.httpRequestMethod = httpRequestMethod;
    }

    public String getApiMethodName() {
        return apiMethodName;
    }

    public void setApiMethodName(String apiMethodName) {
        this.apiMethodName = apiMethodName;
    }

    public Map<String, String> getApiParams() {
        return apiParams;
    }

    public void setApiParams(Map<String, String> apiParams) {
        this.apiParams = apiParams;
    }

    public Map<String, String> getHeaderParams() {
        if (null == this.headerParams) {
            this.headerParams = new HashMap<String, String>();
        }
        return headerParams;
    }

    public void setHeaderParams(Map<String, String> headerParams) {
        this.headerParams = headerParams;
    }

    public String getApiSpecialPath() {
        return apiSpecialPath;
    }

    public void setApiSpecialPath(String apiSpecialPath) {
        this.apiSpecialPath = apiSpecialPath;
    }

    public HttpRequestMethod getHttpRequestMethod() {
        return httpRequestMethod;
    }

    public void setHttpRequestMethod(HttpRequestMethod httpRequestMethod) {
        this.httpRequestMethod = httpRequestMethod;
    }

    public String getApiReqJson() {
        return apiReqJson;
    }

    public void setApiReqJson(String apiReqJson) {
        this.apiReqJson = apiReqJson;
    }
}
