package com.kuaishou.locallife.open.api.response.locallife_goods;

import java.util.List;

import com.kuaishou.locallife.open.api.domain.locallife_goods.ErrorInfoPb;
import com.kuaishou.locallife.open.api.domain.locallife_goods.ResponseData;
import com.kuaishou.locallife.open.api.domain.locallife_goods.SkuDO;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class GoodlifeV1ItemSaveResponse extends KsLocalLifeResponse {

    private ResponseData data;
    private Long item_id;
    private List<SkuDO> sku;
    private ErrorInfoPb error_info;

    public ResponseData getData() {
        return data;
    }

    public void setData(ResponseData data) {
        this.data = data;
    }

    public Long getItem_id() {
        return item_id;
    }

    public void setItem_id(Long item_id) {
        this.item_id = item_id;
    }

    public List<SkuDO> getSku() {
        return sku;
    }

    public void setSku(List<SkuDO> sku) {
        this.sku = sku;
    }

    public ErrorInfoPb getError_info() {
        return error_info;
    }

    public void setError_info(ErrorInfoPb error_info) {
        this.error_info = error_info;
    }

}