package com.kuaishou.locallife.open.api.domain.locallife_distribution;

import java.util.List;

/**
 * auto generate code
 */
public class DataPlanIdList {
    private Integer error_code;
    private String description;
    private List<PlanVo> plans;
    private List<FailReason> reasons;

    public Integer getError_code() {
        return error_code;
    }

    public void setError_code(Integer error_code) {
        this.error_code = error_code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<PlanVo> getPlans() {
        return plans;
    }

    public void setPlans(List<PlanVo> plans) {
        this.plans = plans;
    }

    public List<FailReason> getReasons() {
        return reasons;
    }

    public void setReasons(List<FailReason> reasons) {
        this.reasons = reasons;
    }

}
