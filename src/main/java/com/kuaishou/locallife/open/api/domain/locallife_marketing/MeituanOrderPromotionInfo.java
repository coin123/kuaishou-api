package com.kuaishou.locallife.open.api.domain.locallife_marketing;


/**
 * auto generate code
 */
public class MeituanOrderPromotionInfo {
    private String promotionid;
    private Integer activityproductid;
    private Integer promotiontype;
    private Boolean isDefault;
    private String limitnotice;
    private MeituanOrderPromotionDetail promotiondetail;

    public String getPromotionid() {
        return promotionid;
    }

    public void setPromotionid(String promotionid) {
        this.promotionid = promotionid;
    }

    public Integer getActivityproductid() {
        return activityproductid;
    }

    public void setActivityproductid(Integer activityproductid) {
        this.activityproductid = activityproductid;
    }

    public Integer getPromotiontype() {
        return promotiontype;
    }

    public void setPromotiontype(Integer promotiontype) {
        this.promotiontype = promotiontype;
    }

    public Boolean getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

    public String getLimitnotice() {
        return limitnotice;
    }

    public void setLimitnotice(String limitnotice) {
        this.limitnotice = limitnotice;
    }

    public MeituanOrderPromotionDetail getPromotiondetail() {
        return promotiondetail;
    }

    public void setPromotiondetail(MeituanOrderPromotionDetail promotiondetail) {
        this.promotiondetail = promotiondetail;
    }

}
