package com.kuaishou.locallife.open.api.domain.locallife_material;

import java.util.List;

/**
 * auto generate code
 */
public class UploadByUrlsResponseData {
    private Long error_code;
    private String description;
    private List<SuccessList> success_list;
    private List<FailedList> failed_list;

    public Long getError_code() {
        return error_code;
    }

    public void setError_code(Long error_code) {
        this.error_code = error_code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<SuccessList> getSuccess_list() {
        return success_list;
    }

    public void setSuccess_list(List<SuccessList> success_list) {
        this.success_list = success_list;
    }

    public List<FailedList> getFailed_list() {
        return failed_list;
    }

    public void setFailed_list(List<FailedList> failed_list) {
        this.failed_list = failed_list;
    }

}
