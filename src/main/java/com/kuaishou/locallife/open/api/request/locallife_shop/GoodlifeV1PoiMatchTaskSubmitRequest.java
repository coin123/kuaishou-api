package com.kuaishou.locallife.open.api.request.locallife_shop;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.domain.locallife_shop.ExternalPoiInfo;
import com.kuaishou.locallife.open.api.response.locallife_shop.GoodlifeV1PoiMatchTaskSubmitResponse;

/**
 * auto generate code
 */
public class GoodlifeV1PoiMatchTaskSubmitRequest extends AbstractKsLocalLifeRequest<GoodlifeV1PoiMatchTaskSubmitResponse> {

    private List<ExternalPoiInfo> datas;

    public List<ExternalPoiInfo> getDatas() {
        return datas;
    }

    public void setDatas(List<ExternalPoiInfo> datas) {
        this.datas = datas;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private List<ExternalPoiInfo> datas;

        public List<ExternalPoiInfo> getDatas() {
            return datas;
        }

        public void setDatas(List<ExternalPoiInfo> datas) {
            this.datas = datas;
        }

    }

    @Override
    public String getApiMethodName() {
        return "goodlife.v1.poi.match.task.submit";
    }

    @Override
    public Class<GoodlifeV1PoiMatchTaskSubmitResponse> getResponseClass() {
        return GoodlifeV1PoiMatchTaskSubmitResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.POST;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/goodlife/v1/poi/match/task/submit";
    }
}
