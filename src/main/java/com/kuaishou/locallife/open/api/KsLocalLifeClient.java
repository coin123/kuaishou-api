package com.kuaishou.locallife.open.api;

import com.kuaishou.locallife.open.api.common.http.HttpCommonRequest;
import com.kuaishou.locallife.open.api.common.http.HttpCommonResponse;

/**
 * 快手本地生活api client接口
 *
 * @author shuxiaohui <shuxiaohui@kuaishou.com>
 * Created on 2023-01-30
 */
public interface KsLocalLifeClient {

    /**
     * api执行方法
     *
     * @param request api请求
     * @param <T> response返回泛型
     * @return api对应的response
     * @throws KsLocalLifeApiException api调用异常
     */
    <T extends KsLocalLifeResponse> T execute(KsLocalLifeRequest<T> request) throws KsLocalLifeApiException;

    /**
     * 通用的api执行方法
     */
    HttpCommonResponse execute(HttpCommonRequest request) throws KsLocalLifeApiException;

}
