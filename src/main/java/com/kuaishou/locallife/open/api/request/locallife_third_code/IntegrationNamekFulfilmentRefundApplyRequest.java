package com.kuaishou.locallife.open.api.request.locallife_third_code;

import java.util.HashMap;
import java.util.Map;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.domain.locallife_third_code.RefundAuditRequestData;
import com.kuaishou.locallife.open.api.response.locallife_third_code.IntegrationNamekFulfilmentRefundApplyResponse;

/**
 * auto generate code
 */
public class IntegrationNamekFulfilmentRefundApplyRequest extends AbstractKsLocalLifeRequest<IntegrationNamekFulfilmentRefundApplyResponse> {

    private String appKey;
    private RefundAuditRequestData data;

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }
    public RefundAuditRequestData getData() {
        return data;
    }

    public void setData(RefundAuditRequestData data) {
        this.data = data;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private String appKey;
        private RefundAuditRequestData data;

        public String getAppKey() {
            return appKey;
        }

        public void setAppKey(String appKey) {
            this.appKey = appKey;
        }

        public RefundAuditRequestData getData() {
            return data;
        }

        public void setData(RefundAuditRequestData data) {
            this.data = data;
        }

    }

    @Override
    public String getApiMethodName() {
        return "integration.namek.fulfilment.refund.apply";
    }

    @Override
    public Class<IntegrationNamekFulfilmentRefundApplyResponse> getResponseClass() {
        return IntegrationNamekFulfilmentRefundApplyResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.POST;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/integration/namek/fulfilment/refund/apply";
    }
}
