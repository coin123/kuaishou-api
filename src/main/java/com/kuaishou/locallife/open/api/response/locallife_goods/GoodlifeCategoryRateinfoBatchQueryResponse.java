package com.kuaishou.locallife.open.api.response.locallife_goods;


import com.kuaishou.locallife.open.api.domain.locallife_goods.BatchGetCategoryRateInfoResponse;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class GoodlifeCategoryRateinfoBatchQueryResponse extends KsLocalLifeResponse {

    private BatchGetCategoryRateInfoResponse data;

    public BatchGetCategoryRateInfoResponse getData() {
        return data;
    }

    public void setData(BatchGetCategoryRateInfoResponse data) {
        this.data = data;
    }

}