package com.kuaishou.locallife.open.api.request.openapi;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.domain.openapi.PlainTextInfo;
import com.kuaishou.locallife.open.api.response.openapi.GoodlifeSecuritySignatureBatchResponse;

/**
 * auto generate code
 */
public class GoodlifeSecuritySignatureBatchRequest extends AbstractKsLocalLifeRequest<GoodlifeSecuritySignatureBatchResponse> {

    private List<PlainTextInfo> plain_text_list;

    public List<PlainTextInfo> getPlain_text_list() {
        return plain_text_list;
    }

    public void setPlain_text_list(List<PlainTextInfo> plain_text_list) {
        this.plain_text_list = plain_text_list;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private List<PlainTextInfo> plain_text_list;

        public List<PlainTextInfo> getPlain_text_list() {
            return plain_text_list;
        }

        public void setPlain_text_list(List<PlainTextInfo> plain_text_list) {
            this.plain_text_list = plain_text_list;
        }

    }

    @Override
    public String getApiMethodName() {
        return "goodlife.security.signature.batch";
    }

    @Override
    public Class<GoodlifeSecuritySignatureBatchResponse> getResponseClass() {
        return GoodlifeSecuritySignatureBatchResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.POST;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/goodlife/security/signature/batch";
    }
}
