package com.kuaishou.locallife.open.api.request.openapi;

import java.util.HashMap;
import java.util.Map;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.response.openapi.GoodlifeGatewayGrayTestResponse;

/**
 * auto generate code
 */
public class GoodlifeGatewayGrayTestRequest extends AbstractKsLocalLifeRequest<GoodlifeGatewayGrayTestResponse> {



    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {

    }

    @Override
    public String getApiMethodName() {
        return "goodlife.gateway.gray.test";
    }

    @Override
    public Class<GoodlifeGatewayGrayTestResponse> getResponseClass() {
        return GoodlifeGatewayGrayTestResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.GET;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/goodlife/gateway/gray/test";
    }
}
