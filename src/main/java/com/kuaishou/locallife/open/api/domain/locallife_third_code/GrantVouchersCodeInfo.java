package com.kuaishou.locallife.open.api.domain.locallife_third_code;


/**
 * auto generate code
 */
public class GrantVouchersCodeInfo {
    private String code;
    private String codeRedirect;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeRedirect() {
        return codeRedirect;
    }

    public void setCodeRedirect(String codeRedirect) {
        this.codeRedirect = codeRedirect;
    }

}
