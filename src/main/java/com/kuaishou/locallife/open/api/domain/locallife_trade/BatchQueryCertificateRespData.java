package com.kuaishou.locallife.open.api.domain.locallife_trade;

import java.util.List;

/**
 * auto generate code
 */
public class BatchQueryCertificateRespData {
    private Long error_code;
    private String description;
    private List<Certificate> certificates;

    public Long getError_code() {
        return error_code;
    }

    public void setError_code(Long error_code) {
        this.error_code = error_code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Certificate> getCertificates() {
        return certificates;
    }

    public void setCertificates(List<Certificate> certificates) {
        this.certificates = certificates;
    }

}
