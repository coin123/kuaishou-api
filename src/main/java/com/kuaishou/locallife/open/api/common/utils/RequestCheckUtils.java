package com.kuaishou.locallife.open.api.common.utils;

import static com.kuaishou.locallife.open.api.common.Constants.LocalErrorCode.ERROR_CODE_PARAM_BLANK;
import static com.kuaishou.locallife.open.api.common.Constants.LocalErrorCode.ERROR_CODE_PARAM_INVALID;

import java.util.Collection;
import java.util.List;

import com.kuaishou.locallife.open.api.KsLocalLifeApiException;

/**
 * 通用请求check校验工具类
 *
 * @author shuxiaohui <shuxiaohui@kuaishou.com>
 * Created on 2020-03-31
 */
public class RequestCheckUtils {

    public static void checkNotNull(Object value, String fieldName) throws KsLocalLifeApiException {
        if (value == null) {
            throw new KsLocalLifeApiException(ERROR_CODE_PARAM_BLANK, "Missing required arguments:" + fieldName + "");
        }
    }

    public static void checkNotBlank(Object value, String fieldName) throws KsLocalLifeApiException {
        if (value == null || value instanceof String && KsStringUtils.isBlank((String) value)) {
            throw new KsLocalLifeApiException(ERROR_CODE_PARAM_BLANK, "Missing required arguments:" + fieldName + "");
        }
    }

    public static void checkNotEmpty(Object value, String fieldName) throws KsLocalLifeApiException {
        if (value == null) {
            throw new KsLocalLifeApiException(ERROR_CODE_PARAM_BLANK, "Missing required arguments:" + fieldName + "");
        } else if (value instanceof String && KsStringUtils.isBlank((String) value)) {
            throw new KsLocalLifeApiException(ERROR_CODE_PARAM_BLANK, "Missing required arguments:" + fieldName + "");
        }
    }

    public static void checkNotEmptyCollection(Collection value, String fieldName) throws KsLocalLifeApiException {
        if (value == null) {
            throw new KsLocalLifeApiException(ERROR_CODE_PARAM_BLANK, "Missing required arguments:" + fieldName + "");
        } else if (value.size() == 0) {
            throw new KsLocalLifeApiException(ERROR_CODE_PARAM_BLANK, "not empty arguments:" + fieldName + "");
        }
    }

    public static void checkMaxLength(String value, int maxLength, String fieldName) throws KsLocalLifeApiException {
        if (value != null && value.length() > maxLength) {
            throw new KsLocalLifeApiException(ERROR_CODE_PARAM_INVALID,
                    "Invalid arguments:string length of " + fieldName + " can not be larger than " + maxLength + ".");
        }
    }

    public static void checkMaxListSize(String value, int maxSize, String fieldName) throws KsLocalLifeApiException {
        if (value != null) {
            String[] list = value.split(",");
            if (list.length > maxSize) {
                throw new KsLocalLifeApiException(ERROR_CODE_PARAM_INVALID,
                        "Invalid arguments:the array size of " + fieldName + " must be less than " + maxSize + ".");
            }
        }

    }

    public static void checkMaxListSize(List<String> list, int maxSize, String fieldName)
            throws KsLocalLifeApiException {
        if (list != null && list.size() > maxSize) {
            throw new KsLocalLifeApiException(ERROR_CODE_PARAM_INVALID,
                    "Invalid arguments:the array size of " + fieldName + " must be less than " + maxSize + ".");
        }
    }

    public static void checkMaxValue(Long value, long maxValue, String fieldName) throws KsLocalLifeApiException {
        if (value != null && value > maxValue) {
            throw new KsLocalLifeApiException(ERROR_CODE_PARAM_INVALID,
                    "Invalid arguments:the value of " + fieldName + " can not be larger than " + maxValue + ".");
        }
    }

    public static void checkMinValue(Long value, long minValue, String fieldName) throws KsLocalLifeApiException {
        if (value != null && value < minValue) {
            throw new KsLocalLifeApiException(ERROR_CODE_PARAM_INVALID,
                    "Invalid arguments:the value of " + fieldName + " can not be less than " + minValue + ".");
        }
    }

    private RequestCheckUtils() {
    }
}
