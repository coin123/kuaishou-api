package com.kuaishou.locallife.open.api.request.locallife_marketing;

import java.util.HashMap;
import java.util.Map;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.response.locallife_marketing.IntegrationMarketItemInfoQueryResponse;

/**
 * auto generate code
 */
public class IntegrationMarketItemInfoQueryRequest extends AbstractKsLocalLifeRequest<IntegrationMarketItemInfoQueryResponse> {

    private String appkey;
    private String lat;
    private String lng;
    private String[] out_product_id_list;
    private Integer os;
    private String mtaccesstoken;

    public String getAppkey() {
        return appkey;
    }

    public void setAppkey(String appkey) {
        this.appkey = appkey;
    }
    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }
    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }
    public String[] getOut_product_id_list() {
        return out_product_id_list;
    }

    public void setOut_product_id_list(String[] out_product_id_list) {
        this.out_product_id_list = out_product_id_list;
    }
    public Integer getOs() {
        return os;
    }

    public void setOs(Integer os) {
        this.os = os;
    }
    public String getMtaccesstoken() {
        return mtaccesstoken;
    }

    public void setMtaccesstoken(String mtaccesstoken) {
        this.mtaccesstoken = mtaccesstoken;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private String appkey;
        private String lat;
        private String lng;
        private String[] out_product_id_list;
        private Integer os;
        private String mtaccesstoken;

        public String getAppkey() {
            return appkey;
        }

        public void setAppkey(String appkey) {
            this.appkey = appkey;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getLng() {
            return lng;
        }

        public void setLng(String lng) {
            this.lng = lng;
        }

        public String[] getOut_product_id_list() {
            return out_product_id_list;
        }

        public void setOut_product_id_list(String[] out_product_id_list) {
            this.out_product_id_list = out_product_id_list;
        }

        public Integer getOs() {
            return os;
        }

        public void setOs(Integer os) {
            this.os = os;
        }

        public String getMtaccesstoken() {
            return mtaccesstoken;
        }

        public void setMtaccesstoken(String mtaccesstoken) {
            this.mtaccesstoken = mtaccesstoken;
        }

    }

    @Override
    public String getApiMethodName() {
        return "integration.market.item.info.query";
    }

    @Override
    public Class<IntegrationMarketItemInfoQueryResponse> getResponseClass() {
        return IntegrationMarketItemInfoQueryResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.POST;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/integration/market/item/info/query";
    }
}
