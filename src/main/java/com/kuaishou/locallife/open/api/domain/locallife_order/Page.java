package com.kuaishou.locallife.open.api.domain.locallife_order;


/**
 * auto generate code
 */
public class Page {
    private Integer page_num;
    private Integer page_size;
    private Integer total;

    public Integer getPage_num() {
        return page_num;
    }

    public void setPage_num(Integer page_num) {
        this.page_num = page_num;
    }

    public Integer getPage_size() {
        return page_size;
    }

    public void setPage_size(Integer page_size) {
        this.page_size = page_size;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

}
