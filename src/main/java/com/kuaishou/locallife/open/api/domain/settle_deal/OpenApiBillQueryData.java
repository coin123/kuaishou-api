package com.kuaishou.locallife.open.api.domain.settle_deal;


/**
 * auto generate code
 */
public class OpenApiBillQueryData {
    private Long error_code;
    private String description;
    private Boolean has_more;
    private Integer cursor;
    private OpenApiBillQueryledgerRecord[] ledger_records;

    public Long getError_code() {
        return error_code;
    }

    public void setError_code(Long error_code) {
        this.error_code = error_code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getHas_more() {
        return has_more;
    }

    public void setHas_more(Boolean has_more) {
        this.has_more = has_more;
    }

    public Integer getCursor() {
        return cursor;
    }

    public void setCursor(Integer cursor) {
        this.cursor = cursor;
    }

    public OpenApiBillQueryledgerRecord[] getLedger_records() {
        return ledger_records;
    }

    public void setLedger_records(OpenApiBillQueryledgerRecord[] ledger_records) {
        this.ledger_records = ledger_records;
    }

}
