package com.kuaishou.locallife.open.api.domain.settle_deal;


/**
 * auto generate code
 */
public class OpenApiBillQueryledgerRecord {
    private String id;
    private String ledger_id;
    private String order_id;
    private String account_id;
    private OpenApiBillQueryledgerAnount amount;
    private Integer status;
    private Long ledger_time;
    private OpenApiBillQueryledgerCertificate[] certificate_list;
    private OpenApiBillQueryledgerOrderAttribute order_attribute;
    private OpenApiBillQueryledgerGoods goods;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLedger_id() {
        return ledger_id;
    }

    public void setLedger_id(String ledger_id) {
        this.ledger_id = ledger_id;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getAccount_id() {
        return account_id;
    }

    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }

    public OpenApiBillQueryledgerAnount getAmount() {
        return amount;
    }

    public void setAmount(OpenApiBillQueryledgerAnount amount) {
        this.amount = amount;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getLedger_time() {
        return ledger_time;
    }

    public void setLedger_time(Long ledger_time) {
        this.ledger_time = ledger_time;
    }

    public OpenApiBillQueryledgerCertificate[] getCertificate_list() {
        return certificate_list;
    }

    public void setCertificate_list(OpenApiBillQueryledgerCertificate[] certificate_list) {
        this.certificate_list = certificate_list;
    }

    public OpenApiBillQueryledgerOrderAttribute getOrder_attribute() {
        return order_attribute;
    }

    public void setOrder_attribute(OpenApiBillQueryledgerOrderAttribute order_attribute) {
        this.order_attribute = order_attribute;
    }

    public OpenApiBillQueryledgerGoods getGoods() {
        return goods;
    }

    public void setGoods(OpenApiBillQueryledgerGoods goods) {
        this.goods = goods;
    }

}
