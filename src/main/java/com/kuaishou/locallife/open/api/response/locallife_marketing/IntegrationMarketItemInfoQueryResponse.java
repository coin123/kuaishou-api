package com.kuaishou.locallife.open.api.response.locallife_marketing;


import com.kuaishou.locallife.open.api.domain.locallife_marketing.MeituanItemQuoteInfoResp;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class IntegrationMarketItemInfoQueryResponse extends KsLocalLifeResponse {

    private MeituanItemQuoteInfoResp data;

    public MeituanItemQuoteInfoResp getData() {
        return data;
    }

    public void setData(MeituanItemQuoteInfoResp data) {
        this.data = data;
    }

}