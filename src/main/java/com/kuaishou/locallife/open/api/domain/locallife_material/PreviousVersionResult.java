package com.kuaishou.locallife.open.api.domain.locallife_material;

/**
 * @author gaojiapei <gaojiapei@kuaishou.com>
 * Created on 2023-04-14
 * 兼容之前手动配置的版本
 */
public class PreviousVersionResult {
    private Integer error_code;
    private String description;
    private String full_path_pic;
    private String relative_path_pic;

    public PreviousVersionResult() {
    }

    public Integer getError_code() {
        return this.error_code;
    }

    public void setError_code(Integer error_code) {
        this.error_code = error_code;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFull_path_pic() {
        return this.full_path_pic;
    }

    public void setFull_path_pic(String full_path_pic) {
        this.full_path_pic = full_path_pic;
    }

    public String getRelative_path_pic() {
        return this.relative_path_pic;
    }

    public void setRelative_path_pic(String relative_path_pic) {
        this.relative_path_pic = relative_path_pic;
    }
}
