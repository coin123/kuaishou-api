package com.kuaishou.locallife.open.api.domain.settle_deal;


/**
 * auto generate code
 */
public class OpenApiBillQueryledgerGoods {
    private String sku_id;
    private Long sold_start_time;
    private String group_name;

    public String getSku_id() {
        return sku_id;
    }

    public void setSku_id(String sku_id) {
        this.sku_id = sku_id;
    }

    public Long getSold_start_time() {
        return sold_start_time;
    }

    public void setSold_start_time(Long sold_start_time) {
        this.sold_start_time = sold_start_time;
    }

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }

}
