package com.kuaishou.locallife.open.api.domain.locallife_shop;


/**
 * auto generate code
 */
public class QueryTaskData {
    private String match_poi_id;
    private String match_message;
    private Integer match_result;
    private String ext_poi_id;
    private Integer match_status;

    public String getMatch_poi_id() {
        return match_poi_id;
    }

    public void setMatch_poi_id(String match_poi_id) {
        this.match_poi_id = match_poi_id;
    }

    public String getMatch_message() {
        return match_message;
    }

    public void setMatch_message(String match_message) {
        this.match_message = match_message;
    }

    public Integer getMatch_result() {
        return match_result;
    }

    public void setMatch_result(Integer match_result) {
        this.match_result = match_result;
    }

    public String getExt_poi_id() {
        return ext_poi_id;
    }

    public void setExt_poi_id(String ext_poi_id) {
        this.ext_poi_id = ext_poi_id;
    }

    public Integer getMatch_status() {
        return match_status;
    }

    public void setMatch_status(Integer match_status) {
        this.match_status = match_status;
    }

}
