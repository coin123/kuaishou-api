package com.kuaishou.locallife.open.api.domain.locallife_marketing;


/**
 * auto generate code
 */
public class MeituanItemPriceInfo {
    private Integer origin_price;
    private Integer sale_price;
    private Integer buy_price;

    public Integer getOrigin_price() {
        return origin_price;
    }

    public void setOrigin_price(Integer origin_price) {
        this.origin_price = origin_price;
    }

    public Integer getSale_price() {
        return sale_price;
    }

    public void setSale_price(Integer sale_price) {
        this.sale_price = sale_price;
    }

    public Integer getBuy_price() {
        return buy_price;
    }

    public void setBuy_price(Integer buy_price) {
        this.buy_price = buy_price;
    }

}
