package com.kuaishou.locallife.open.api.response.locallife_shop;


import com.kuaishou.locallife.open.api.domain.locallife_shop.OpenApiPoiShopPoiRespData;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class GoodlifeV1ShopPoiQueryResponse extends KsLocalLifeResponse {

    private OpenApiPoiShopPoiRespData data;

    public OpenApiPoiShopPoiRespData getData() {
        return data;
    }

    public void setData(OpenApiPoiShopPoiRespData data) {
        this.data = data;
    }

}