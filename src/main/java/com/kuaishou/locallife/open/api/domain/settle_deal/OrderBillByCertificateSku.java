package com.kuaishou.locallife.open.api.domain.settle_deal;


/**
 * auto generate code
 */
public class OrderBillByCertificateSku {
    private String sku_id;
    private String title;
    private Long market_price;
    private Long sold_start_time;

    public String getSku_id() {
        return sku_id;
    }

    public void setSku_id(String sku_id) {
        this.sku_id = sku_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getMarket_price() {
        return market_price;
    }

    public void setMarket_price(Long market_price) {
        this.market_price = market_price;
    }

    public Long getSold_start_time() {
        return sold_start_time;
    }

    public void setSold_start_time(Long sold_start_time) {
        this.sold_start_time = sold_start_time;
    }

}
