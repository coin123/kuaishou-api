package com.kuaishou.locallife.open.api.domain.locallife_goods;


/**
 * auto generate code
 */
public class SkuDO {
    private String out_sku_id;
    private Long sku_id;

    public String getOut_sku_id() {
        return out_sku_id;
    }

    public void setOut_sku_id(String out_sku_id) {
        this.out_sku_id = out_sku_id;
    }

    public Long getSku_id() {
        return sku_id;
    }

    public void setSku_id(Long sku_id) {
        this.sku_id = sku_id;
    }

}
