package com.kuaishou.locallife.open.api.domain.locallife_third_code;


/**
 * auto generate code
 */
public class GranVoucherAmount {
    private Long original_amount;
    private Long pay_amount;
    private Long ticket_amount;
    private Long merchant_ticket_amount;
    private Long fee_amount;
    private Long commission_amount;
    private Long payment_discount_amount;
    private Long coupon_pay_amount;
    private Long order_discount_amount;

    public Long getOriginal_amount() {
        return original_amount;
    }

    public void setOriginal_amount(Long original_amount) {
        this.original_amount = original_amount;
    }

    public Long getPay_amount() {
        return pay_amount;
    }

    public void setPay_amount(Long pay_amount) {
        this.pay_amount = pay_amount;
    }

    public Long getTicket_amount() {
        return ticket_amount;
    }

    public void setTicket_amount(Long ticket_amount) {
        this.ticket_amount = ticket_amount;
    }

    public Long getMerchant_ticket_amount() {
        return merchant_ticket_amount;
    }

    public void setMerchant_ticket_amount(Long merchant_ticket_amount) {
        this.merchant_ticket_amount = merchant_ticket_amount;
    }

    public Long getFee_amount() {
        return fee_amount;
    }

    public void setFee_amount(Long fee_amount) {
        this.fee_amount = fee_amount;
    }

    public Long getCommission_amount() {
        return commission_amount;
    }

    public void setCommission_amount(Long commission_amount) {
        this.commission_amount = commission_amount;
    }

    public Long getPayment_discount_amount() {
        return payment_discount_amount;
    }

    public void setPayment_discount_amount(Long payment_discount_amount) {
        this.payment_discount_amount = payment_discount_amount;
    }

    public Long getCoupon_pay_amount() {
        return coupon_pay_amount;
    }

    public void setCoupon_pay_amount(Long coupon_pay_amount) {
        this.coupon_pay_amount = coupon_pay_amount;
    }

    public Long getOrder_discount_amount() {
        return order_discount_amount;
    }

    public void setOrder_discount_amount(Long order_discount_amount) {
        this.order_discount_amount = order_discount_amount;
    }

}
