package com.kuaishou.locallife.open.api.domain.locallife_goods;


/**
 * auto generate code
 */
public class Result {
    private String error_msg;
    private Integer result_code;
    private Boolean is_success;

    public String getError_msg() {
        return error_msg;
    }

    public void setError_msg(String error_msg) {
        this.error_msg = error_msg;
    }

    public Integer getResult_code() {
        return result_code;
    }

    public void setResult_code(Integer result_code) {
        this.result_code = result_code;
    }

    public Boolean getIs_success() {
        return is_success;
    }

    public void setIs_success(Boolean is_success) {
        this.is_success = is_success;
    }

}
