package com.kuaishou.locallife.open.api.domain.locallife_material;


/**
 * auto generate code
 */
public class FailedList {
    private Long error_code;
    private String error_msg;
    private String image_id;

    public Long getError_code() {
        return error_code;
    }

    public void setError_code(Long error_code) {
        this.error_code = error_code;
    }

    public String getError_msg() {
        return error_msg;
    }

    public void setError_msg(String error_msg) {
        this.error_msg = error_msg;
    }

    public String getImage_id() {
        return image_id;
    }

    public void setImage_id(String image_id) {
        this.image_id = image_id;
    }

}
