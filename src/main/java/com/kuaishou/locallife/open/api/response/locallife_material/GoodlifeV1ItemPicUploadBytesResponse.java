package com.kuaishou.locallife.open.api.response.locallife_material;


import com.kuaishou.locallife.open.api.domain.locallife_material.ImgUploadByBytesRespData;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class GoodlifeV1ItemPicUploadBytesResponse extends KsLocalLifeResponse {

    private ImgUploadByBytesRespData data;

    public ImgUploadByBytesRespData getData() {
        return data;
    }

    public void setData(ImgUploadByBytesRespData data) {
        this.data = data;
    }

}