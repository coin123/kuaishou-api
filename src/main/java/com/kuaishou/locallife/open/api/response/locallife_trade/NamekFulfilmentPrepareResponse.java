package com.kuaishou.locallife.open.api.response.locallife_trade;


import com.kuaishou.locallife.open.api.domain.locallife_trade.AssetPrepareData;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class NamekFulfilmentPrepareResponse extends KsLocalLifeResponse {

    private AssetPrepareData data;

    public AssetPrepareData getData() {
        return data;
    }

    public void setData(AssetPrepareData data) {
        this.data = data;
    }

}