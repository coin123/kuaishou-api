package com.kuaishou.locallife.open.api.request.locallife_trade;

import java.util.HashMap;
import java.util.Map;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.response.locallife_trade.GoodlifeV1FulfilmentCertificateCancelResponse;

/**
 * auto generate code
 */
public class GoodlifeV1FulfilmentCertificateCancelRequest extends AbstractKsLocalLifeRequest<GoodlifeV1FulfilmentCertificateCancelResponse> {

    private String verify_id;
    private String certificate_id;
    private String new_code;

    public String getVerify_id() {
        return verify_id;
    }

    public void setVerify_id(String verify_id) {
        this.verify_id = verify_id;
    }
    public String getCertificate_id() {
        return certificate_id;
    }

    public void setCertificate_id(String certificate_id) {
        this.certificate_id = certificate_id;
    }
    public String getNew_code() {
        return new_code;
    }

    public void setNew_code(String new_code) {
        this.new_code = new_code;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private String verify_id;
        private String certificate_id;
        private String new_code;

        public String getVerify_id() {
            return verify_id;
        }

        public void setVerify_id(String verify_id) {
            this.verify_id = verify_id;
        }

        public String getCertificate_id() {
            return certificate_id;
        }

        public void setCertificate_id(String certificate_id) {
            this.certificate_id = certificate_id;
        }

        public String getNew_code() {
            return new_code;
        }

        public void setNew_code(String new_code) {
            this.new_code = new_code;
        }

    }

    @Override
    public String getApiMethodName() {
        return "goodlife.v1.fulfilment.certificate.cancel";
    }

    @Override
    public Class<GoodlifeV1FulfilmentCertificateCancelResponse> getResponseClass() {
        return GoodlifeV1FulfilmentCertificateCancelResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.POST;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/goodlife/v1/fulfilment/certificate/cancel";
    }
}
