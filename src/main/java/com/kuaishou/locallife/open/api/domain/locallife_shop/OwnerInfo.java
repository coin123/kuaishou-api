package com.kuaishou.locallife.open.api.domain.locallife_shop;


/**
 * auto generate code
 */
public class OwnerInfo {
    private String phone;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

}
