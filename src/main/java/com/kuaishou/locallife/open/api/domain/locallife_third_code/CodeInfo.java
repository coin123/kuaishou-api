package com.kuaishou.locallife.open.api.domain.locallife_third_code;


/**
 * auto generate code
 */
public class CodeInfo {
    private String code;
    private String code_redirect;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode_redirect() {
        return code_redirect;
    }

    public void setCode_redirect(String code_redirect) {
        this.code_redirect = code_redirect;
    }

}
