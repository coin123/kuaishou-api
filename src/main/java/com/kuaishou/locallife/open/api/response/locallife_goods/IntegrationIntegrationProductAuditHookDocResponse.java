package com.kuaishou.locallife.open.api.response.locallife_goods;


import com.kuaishou.locallife.open.api.domain.locallife_goods.ResponseData;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class IntegrationIntegrationProductAuditHookDocResponse extends KsLocalLifeResponse {

    private ResponseData data;

    public ResponseData getData() {
        return data;
    }

    public void setData(ResponseData data) {
        this.data = data;
    }

}