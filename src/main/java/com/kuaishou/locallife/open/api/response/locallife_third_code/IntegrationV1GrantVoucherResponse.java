package com.kuaishou.locallife.open.api.response.locallife_third_code;


import com.kuaishou.locallife.open.api.domain.locallife_third_code.GrantVoucherResp;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class IntegrationV1GrantVoucherResponse extends KsLocalLifeResponse {

    private GrantVoucherResp data;

    public GrantVoucherResp getData() {
        return data;
    }

    public void setData(GrantVoucherResp data) {
        this.data = data;
    }

}