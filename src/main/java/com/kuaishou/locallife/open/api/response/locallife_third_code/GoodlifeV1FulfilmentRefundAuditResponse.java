package com.kuaishou.locallife.open.api.response.locallife_third_code;


import com.kuaishou.locallife.open.api.domain.locallife_third_code.AuditData;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class GoodlifeV1FulfilmentRefundAuditResponse extends KsLocalLifeResponse {

    private AuditData data;

    public AuditData getData() {
        return data;
    }

    public void setData(AuditData data) {
        this.data = data;
    }

}