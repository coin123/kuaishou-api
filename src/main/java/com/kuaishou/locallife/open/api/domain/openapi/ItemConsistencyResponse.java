package com.kuaishou.locallife.open.api.domain.openapi;

import java.util.List;

/**
 * auto generate code
 */
public class ItemConsistencyResponse {
    private Integer error_code;
    private String description;
    private List<ItemResponse> itemList;

    public Integer getError_code() {
        return error_code;
    }

    public void setError_code(Integer error_code) {
        this.error_code = error_code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ItemResponse> getItemList() {
        return itemList;
    }

    public void setItemList(List<ItemResponse> itemList) {
        this.itemList = itemList;
    }

}
