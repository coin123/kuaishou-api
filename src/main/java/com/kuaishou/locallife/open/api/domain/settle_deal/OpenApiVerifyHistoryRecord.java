package com.kuaishou.locallife.open.api.domain.settle_deal;


/**
 * auto generate code
 */
public class OpenApiVerifyHistoryRecord {
    private Integer cursor;
    private String verify_id;
    private String certificate_id;
    private Long verify_time;
    private Boolean can_cancel;
    private Integer verify_type;
    private Integer status;
    private String code;
    private Long cancel_time;
    private OpenApiVerifyHistoryAmount amount;
    private OpenApiVerifyHistorySku sku;

    public Integer getCursor() {
        return cursor;
    }

    public void setCursor(Integer cursor) {
        this.cursor = cursor;
    }

    public String getVerify_id() {
        return verify_id;
    }

    public void setVerify_id(String verify_id) {
        this.verify_id = verify_id;
    }

    public String getCertificate_id() {
        return certificate_id;
    }

    public void setCertificate_id(String certificate_id) {
        this.certificate_id = certificate_id;
    }

    public Long getVerify_time() {
        return verify_time;
    }

    public void setVerify_time(Long verify_time) {
        this.verify_time = verify_time;
    }

    public Boolean getCan_cancel() {
        return can_cancel;
    }

    public void setCan_cancel(Boolean can_cancel) {
        this.can_cancel = can_cancel;
    }

    public Integer getVerify_type() {
        return verify_type;
    }

    public void setVerify_type(Integer verify_type) {
        this.verify_type = verify_type;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getCancel_time() {
        return cancel_time;
    }

    public void setCancel_time(Long cancel_time) {
        this.cancel_time = cancel_time;
    }

    public OpenApiVerifyHistoryAmount getAmount() {
        return amount;
    }

    public void setAmount(OpenApiVerifyHistoryAmount amount) {
        this.amount = amount;
    }

    public OpenApiVerifyHistorySku getSku() {
        return sku;
    }

    public void setSku(OpenApiVerifyHistorySku sku) {
        this.sku = sku;
    }

}
