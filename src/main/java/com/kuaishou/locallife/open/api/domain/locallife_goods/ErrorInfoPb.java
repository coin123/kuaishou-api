package com.kuaishou.locallife.open.api.domain.locallife_goods;


/**
 * auto generate code
 */
public class ErrorInfoPb {
    private String banned_poi_ids;

    public String getBanned_poi_ids() {
        return banned_poi_ids;
    }

    public void setBanned_poi_ids(String banned_poi_ids) {
        this.banned_poi_ids = banned_poi_ids;
    }

}
