package com.kuaishou.locallife.open.api.domain.locallife_distribution;

import java.util.List;

/**
 * auto generate code
 */
public class PlanParam {
    private Long item_id;
    private Integer distributor_rate;
    private List<Long> distributors;

    public Long getItem_id() {
        return item_id;
    }

    public void setItem_id(Long item_id) {
        this.item_id = item_id;
    }

    public Integer getDistributor_rate() {
        return distributor_rate;
    }

    public void setDistributor_rate(Integer distributor_rate) {
        this.distributor_rate = distributor_rate;
    }

    public List<Long> getDistributors() {
        return distributors;
    }

    public void setDistributors(List<Long> distributors) {
        this.distributors = distributors;
    }

}
