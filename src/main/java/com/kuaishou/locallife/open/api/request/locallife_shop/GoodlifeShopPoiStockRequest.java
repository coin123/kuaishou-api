package com.kuaishou.locallife.open.api.request.locallife_shop;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.domain.locallife_shop.BankCardInfo;
import com.kuaishou.locallife.open.api.domain.locallife_shop.BusinessLicenseInfo;
import com.kuaishou.locallife.open.api.domain.locallife_shop.LegalPersonInfo;
import com.kuaishou.locallife.open.api.response.locallife_shop.GoodlifeShopPoiStockResponse;

/**
 * auto generate code
 */
public class GoodlifeShopPoiStockRequest extends AbstractKsLocalLifeRequest<GoodlifeShopPoiStockResponse> {

    private BusinessLicenseInfo business_license_info;
    private BankCardInfo bind_bank_card_info;
    private String subject_type;
    private LegalPersonInfo legal_info;
    private List<Long> poi_ids;
    private String poi_name;
    private String idempotent_id;

    public BusinessLicenseInfo getBusiness_license_info() {
        return business_license_info;
    }

    public void setBusiness_license_info(BusinessLicenseInfo business_license_info) {
        this.business_license_info = business_license_info;
    }
    public BankCardInfo getBind_bank_card_info() {
        return bind_bank_card_info;
    }

    public void setBind_bank_card_info(BankCardInfo bind_bank_card_info) {
        this.bind_bank_card_info = bind_bank_card_info;
    }
    public String getSubject_type() {
        return subject_type;
    }

    public void setSubject_type(String subject_type) {
        this.subject_type = subject_type;
    }
    public LegalPersonInfo getLegal_info() {
        return legal_info;
    }

    public void setLegal_info(LegalPersonInfo legal_info) {
        this.legal_info = legal_info;
    }
    public List<Long> getPoi_ids() {
        return poi_ids;
    }

    public void setPoi_ids(List<Long> poi_ids) {
        this.poi_ids = poi_ids;
    }
    public String getPoi_name() {
        return poi_name;
    }

    public void setPoi_name(String poi_name) {
        this.poi_name = poi_name;
    }
    public String getIdempotent_id() {
        return idempotent_id;
    }

    public void setIdempotent_id(String idempotent_id) {
        this.idempotent_id = idempotent_id;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private BusinessLicenseInfo business_license_info;
        private BankCardInfo bind_bank_card_info;
        private String subject_type;
        private LegalPersonInfo legal_info;
        private List<Long> poi_ids;
        private String poi_name;
        private String idempotent_id;

        public BusinessLicenseInfo getBusiness_license_info() {
            return business_license_info;
        }

        public void setBusiness_license_info(BusinessLicenseInfo business_license_info) {
            this.business_license_info = business_license_info;
        }

        public BankCardInfo getBind_bank_card_info() {
            return bind_bank_card_info;
        }

        public void setBind_bank_card_info(BankCardInfo bind_bank_card_info) {
            this.bind_bank_card_info = bind_bank_card_info;
        }

        public String getSubject_type() {
            return subject_type;
        }

        public void setSubject_type(String subject_type) {
            this.subject_type = subject_type;
        }

        public LegalPersonInfo getLegal_info() {
            return legal_info;
        }

        public void setLegal_info(LegalPersonInfo legal_info) {
            this.legal_info = legal_info;
        }

        public List<Long> getPoi_ids() {
            return poi_ids;
        }

        public void setPoi_ids(List<Long> poi_ids) {
            this.poi_ids = poi_ids;
        }

        public String getPoi_name() {
            return poi_name;
        }

        public void setPoi_name(String poi_name) {
            this.poi_name = poi_name;
        }

        public String getIdempotent_id() {
            return idempotent_id;
        }

        public void setIdempotent_id(String idempotent_id) {
            this.idempotent_id = idempotent_id;
        }

    }

    @Override
    public String getApiMethodName() {
        return "goodlife.shop.poi.stock";
    }

    @Override
    public Class<GoodlifeShopPoiStockResponse> getResponseClass() {
        return GoodlifeShopPoiStockResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.POST;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/goodlife/shop/poi/stock";
    }
}
