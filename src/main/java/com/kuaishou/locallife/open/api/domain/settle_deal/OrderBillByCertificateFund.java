package com.kuaishou.locallife.open.api.domain.settle_deal;


/**
 * auto generate code
 */
public class OrderBillByCertificateFund {
    private Long goods;
    private Long talent_commission;
    private Long total_agent_merchant;
    private Long total_merchant_platform_service;

    public Long getGoods() {
        return goods;
    }

    public void setGoods(Long goods) {
        this.goods = goods;
    }

    public Long getTalent_commission() {
        return talent_commission;
    }

    public void setTalent_commission(Long talent_commission) {
        this.talent_commission = talent_commission;
    }

    public Long getTotal_agent_merchant() {
        return total_agent_merchant;
    }

    public void setTotal_agent_merchant(Long total_agent_merchant) {
        this.total_agent_merchant = total_agent_merchant;
    }

    public Long getTotal_merchant_platform_service() {
        return total_merchant_platform_service;
    }

    public void setTotal_merchant_platform_service(Long total_merchant_platform_service) {
        this.total_merchant_platform_service = total_merchant_platform_service;
    }

}
