package com.kuaishou.locallife.open.api.response.locallife_material;


import com.kuaishou.locallife.open.api.domain.locallife_material.UploadByUrlsResponseData;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class GoodlifeV1ItemPicUploadUrlsResponse extends KsLocalLifeResponse {

    private UploadByUrlsResponseData data;

    public UploadByUrlsResponseData getData() {
        return data;
    }

    public void setData(UploadByUrlsResponseData data) {
        this.data = data;
    }

}