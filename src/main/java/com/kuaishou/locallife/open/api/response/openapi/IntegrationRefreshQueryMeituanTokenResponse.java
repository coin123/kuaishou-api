package com.kuaishou.locallife.open.api.response.openapi;


import com.kuaishou.locallife.open.api.domain.openapi.MeituanRefreshApiData;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class IntegrationRefreshQueryMeituanTokenResponse extends KsLocalLifeResponse {

    private MeituanRefreshApiData data;

    public MeituanRefreshApiData getData() {
        return data;
    }

    public void setData(MeituanRefreshApiData data) {
        this.data = data;
    }

}