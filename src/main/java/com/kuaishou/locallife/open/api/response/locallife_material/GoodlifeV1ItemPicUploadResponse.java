package com.kuaishou.locallife.open.api.response.locallife_material;


import com.kuaishou.locallife.open.api.domain.locallife_material.PicInfoPb;
import com.kuaishou.locallife.open.api.domain.locallife_material.Result;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class GoodlifeV1ItemPicUploadResponse extends KsLocalLifeResponse {

    private Result data;

    public Result getData() {
        return data;
    }

    public void setData(Result data) {
        this.data = data;
    }

}