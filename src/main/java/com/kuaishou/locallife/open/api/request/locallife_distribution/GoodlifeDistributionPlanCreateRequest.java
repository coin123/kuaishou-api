package com.kuaishou.locallife.open.api.request.locallife_distribution;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.domain.locallife_distribution.PlanParam;
import com.kuaishou.locallife.open.api.response.locallife_distribution.GoodlifeDistributionPlanCreateResponse;

/**
 * auto generate code
 */
public class GoodlifeDistributionPlanCreateRequest extends AbstractKsLocalLifeRequest<GoodlifeDistributionPlanCreateResponse> {

    private Integer plan_type;
    private List<PlanParam> plans;

    public Integer getPlan_type() {
        return plan_type;
    }

    public void setPlan_type(Integer plan_type) {
        this.plan_type = plan_type;
    }
    public List<PlanParam> getPlans() {
        return plans;
    }

    public void setPlans(List<PlanParam> plans) {
        this.plans = plans;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private Integer plan_type;
        private List<PlanParam> plans;

        public Integer getPlan_type() {
            return plan_type;
        }

        public void setPlan_type(Integer plan_type) {
            this.plan_type = plan_type;
        }

        public List<PlanParam> getPlans() {
            return plans;
        }

        public void setPlans(List<PlanParam> plans) {
            this.plans = plans;
        }

    }

    @Override
    public String getApiMethodName() {
        return "goodlife.distribution.plan.create";
    }

    @Override
    public Class<GoodlifeDistributionPlanCreateResponse> getResponseClass() {
        return GoodlifeDistributionPlanCreateResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.POST;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/goodlife/distribution/plan/create";
    }
}
