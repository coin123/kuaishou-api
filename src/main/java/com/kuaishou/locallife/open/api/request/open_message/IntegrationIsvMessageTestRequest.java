package com.kuaishou.locallife.open.api.request.open_message;

import java.util.HashMap;
import java.util.Map;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.response.open_message.IntegrationIsvMessageTestResponse;

/**
 * auto generate code
 */
public class IntegrationIsvMessageTestRequest extends AbstractKsLocalLifeRequest<IntegrationIsvMessageTestResponse> {

    private String encryptedMessage;
    private String appKey;
    private String messageTag;

    public String getEncryptedMessage() {
        return encryptedMessage;
    }

    public void setEncryptedMessage(String encryptedMessage) {
        this.encryptedMessage = encryptedMessage;
    }
    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }
    public String getMessageTag() {
        return messageTag;
    }

    public void setMessageTag(String messageTag) {
        this.messageTag = messageTag;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private String encryptedMessage;
        private String appKey;
        private String messageTag;

        public String getEncryptedMessage() {
            return encryptedMessage;
        }

        public void setEncryptedMessage(String encryptedMessage) {
            this.encryptedMessage = encryptedMessage;
        }

        public String getAppKey() {
            return appKey;
        }

        public void setAppKey(String appKey) {
            this.appKey = appKey;
        }

        public String getMessageTag() {
            return messageTag;
        }

        public void setMessageTag(String messageTag) {
            this.messageTag = messageTag;
        }

    }

    @Override
    public String getApiMethodName() {
        return "integration.isv.message.test";
    }

    @Override
    public Class<IntegrationIsvMessageTestResponse> getResponseClass() {
        return IntegrationIsvMessageTestResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.POST;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/integration/isv/message/test";
    }
}
