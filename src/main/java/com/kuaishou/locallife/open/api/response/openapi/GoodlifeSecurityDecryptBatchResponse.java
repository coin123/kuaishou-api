package com.kuaishou.locallife.open.api.response.openapi;


import com.kuaishou.locallife.open.api.domain.openapi.BatchDecryptResponse;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class GoodlifeSecurityDecryptBatchResponse extends KsLocalLifeResponse {

    private BatchDecryptResponse data;

    public BatchDecryptResponse getData() {
        return data;
    }

    public void setData(BatchDecryptResponse data) {
        this.data = data;
    }

}