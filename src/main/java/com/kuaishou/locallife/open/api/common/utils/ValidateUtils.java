package com.kuaishou.locallife.open.api.common.utils;

/**
 * @author qishengwang <qishengwang@kuaishou.com>
 * Created on 2021-01-07
 */
public class ValidateUtils {

    public static <T extends CharSequence> T notBlank(final T chars, final String message, final Object... values) {
        ObjectUtils.requireNonNull(chars, String.format(message, values));
        if (KsStringUtils.isBlank(chars)) {
            throw new IllegalArgumentException(String.format(message, values));
        }
        return chars;
    }
}
