package com.kuaishou.locallife.open.api.domain.locallife_third_code;

import java.util.List;

/**
 * auto generate code
 */
public class OrderNoticeReq {
    private Long orderId;
    private String skuId;
    private String thirdSkuId;
    private Integer count;
    private Long itemId;
    private String outId;
    private String outSkuId;
    private OrderNoticeAmount amountDO;
    private List<OrderNoticePromotion> promotionDOS;
    private MeituanUserInfoApi userInfo;
    private String lag;
    private String lng;
    private String idempotentId;
    private Long accountId;
    private OrderNoticeContact contact;
    private OrderNoticeTourists[] tourists;
    private Integer grouponType;
    private Integer timesCount;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public String getThirdSkuId() {
        return thirdSkuId;
    }

    public void setThirdSkuId(String thirdSkuId) {
        this.thirdSkuId = thirdSkuId;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public String getOutId() {
        return outId;
    }

    public void setOutId(String outId) {
        this.outId = outId;
    }

    public String getOutSkuId() {
        return outSkuId;
    }

    public void setOutSkuId(String outSkuId) {
        this.outSkuId = outSkuId;
    }

    public OrderNoticeAmount getAmountDO() {
        return amountDO;
    }

    public void setAmountDO(OrderNoticeAmount amountDO) {
        this.amountDO = amountDO;
    }

    public List<OrderNoticePromotion> getPromotionDOS() {
        return promotionDOS;
    }

    public void setPromotionDOS(List<OrderNoticePromotion> promotionDOS) {
        this.promotionDOS = promotionDOS;
    }

    public MeituanUserInfoApi getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(MeituanUserInfoApi userInfo) {
        this.userInfo = userInfo;
    }

    public String getLag() {
        return lag;
    }

    public void setLag(String lag) {
        this.lag = lag;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getIdempotentId() {
        return idempotentId;
    }

    public void setIdempotentId(String idempotentId) {
        this.idempotentId = idempotentId;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public OrderNoticeContact getContact() {
        return contact;
    }

    public void setContact(OrderNoticeContact contact) {
        this.contact = contact;
    }

    public OrderNoticeTourists[] getTourists() {
        return tourists;
    }

    public void setTourists(OrderNoticeTourists[] tourists) {
        this.tourists = tourists;
    }

    public Integer getGrouponType() {
        return grouponType;
    }

    public void setGrouponType(Integer grouponType) {
        this.grouponType = grouponType;
    }

    public Integer getTimesCount() {
        return timesCount;
    }

    public void setTimesCount(Integer timesCount) {
        this.timesCount = timesCount;
    }

}
