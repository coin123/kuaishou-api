package com.kuaishou.locallife.open.api.response.locallife_third_code;


import com.kuaishou.locallife.open.api.domain.locallife_third_code.Extra;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class GoodlifeV1FulfilmentRefundCreateResponse extends KsLocalLifeResponse {

    private Extra data;
    private String logid;

    public Extra getData() {
        return data;
    }

    public void setData(Extra data) {
        this.data = data;
    }

    public String getLogid() {
        return logid;
    }

    public void setLogid(String logid) {
        this.logid = logid;
    }

}