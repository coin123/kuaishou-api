package com.kuaishou.locallife.open.api.request.locallife_shop;

import java.util.HashMap;
import java.util.Map;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.response.locallife_shop.GoodlifeV1PoiMatchRelationQueryResponse;

/**
 * auto generate code
 */
public class GoodlifeV1PoiMatchRelationQueryRequest extends AbstractKsLocalLifeRequest<GoodlifeV1PoiMatchRelationQueryResponse> {

    private String ext_poi_ids;

    public String getExt_poi_ids() {
        return ext_poi_ids;
    }

    public void setExt_poi_ids(String ext_poi_ids) {
        this.ext_poi_ids = ext_poi_ids;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        bizParams.put("ext_poi_ids", this.getExt_poi_ids());
        
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private String ext_poi_ids;

        public String getExt_poi_ids() {
            return ext_poi_ids;
        }

        public void setExt_poi_ids(String ext_poi_ids) {
            this.ext_poi_ids = ext_poi_ids;
        }

    }

    @Override
    public String getApiMethodName() {
        return "goodlife.v1.poi.match.relation.query";
    }

    @Override
    public Class<GoodlifeV1PoiMatchRelationQueryResponse> getResponseClass() {
        return GoodlifeV1PoiMatchRelationQueryResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.GET;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/goodlife/v1/poi/match/relation/query";
    }
}
