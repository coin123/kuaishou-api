package com.kuaishou.locallife.open.api.domain.ksoptest;


/**
 * auto generate code
 */
public class Result {
    private String error_msg;
    private Integer result_code;
    private String gjp_test;

    public String getError_msg() {
        return error_msg;
    }

    public void setError_msg(String error_msg) {
        this.error_msg = error_msg;
    }

    public Integer getResult_code() {
        return result_code;
    }

    public void setResult_code(Integer result_code) {
        this.result_code = result_code;
    }

    public String getGjp_test() {
        return gjp_test;
    }

    public void setGjp_test(String gjp_test) {
        this.gjp_test = gjp_test;
    }

}
