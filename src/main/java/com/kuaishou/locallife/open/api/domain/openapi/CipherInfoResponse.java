package com.kuaishou.locallife.open.api.domain.openapi;


/**
 * auto generate code
 */
public class CipherInfoResponse {
    private String error_msg;
    private String decrypt_text;
    private Integer error_code;
    private String cipher_text;

    public String getError_msg() {
        return error_msg;
    }

    public void setError_msg(String error_msg) {
        this.error_msg = error_msg;
    }

    public String getDecrypt_text() {
        return decrypt_text;
    }

    public void setDecrypt_text(String decrypt_text) {
        this.decrypt_text = decrypt_text;
    }

    public Integer getError_code() {
        return error_code;
    }

    public void setError_code(Integer error_code) {
        this.error_code = error_code;
    }

    public String getCipher_text() {
        return cipher_text;
    }

    public void setCipher_text(String cipher_text) {
        this.cipher_text = cipher_text;
    }

}
