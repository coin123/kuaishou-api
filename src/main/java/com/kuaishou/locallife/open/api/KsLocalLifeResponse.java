package com.kuaishou.locallife.open.api;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;
import com.kuaishou.locallife.open.api.common.dto.KsLocalLifeGatewayExtraDTO;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;

/**
 * api返回response
 *
 * @author shuxiaohui <shuxiaohui@kuaishou.com>
 * Created on 2020-03-30
 */
public abstract class KsLocalLifeResponse implements Serializable {
    private static final long serialVersionUID = 6415647615856266000L;

    private static final int SUCCESS_CODE = 0;

    /**
     * 网关响应
     */
    @SerializedName("extra")
    private KsLocalLifeGatewayExtraDTO extra;

    /**
     * 请求参数requests
     */
    private Requests requests;

    public KsLocalLifeResponse() {
    }

    public boolean isSuccess() {
        return SUCCESS_CODE == this.extra.getErrorCode();
    }

    public KsLocalLifeGatewayExtraDTO getExtra() {
        return extra;
    }

    public void setExtra(KsLocalLifeGatewayExtraDTO extra) {
        this.extra = extra;
    }

    public Requests getRequests() {
        return requests;
    }

    public void setRequests(Requests requests) {
        this.requests = requests;
    }

    public static class Requests implements Serializable {
        /**
         * http请求request
         */
        private String http;

        public String getHttp() {
            return http;
        }

        public void setHttp(String http) {
            this.http = http;
        }
    }

}
