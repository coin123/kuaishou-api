package com.kuaishou.locallife.open.api.request.locallife_material;

import java.util.HashMap;
import java.util.Map;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.response.locallife_material.GoodlifeV1ItemPicUploadResponse;

/**
 * auto generate code
 */
public class GoodlifeV1ItemPicUploadRequest extends AbstractKsLocalLifeRequest<GoodlifeV1ItemPicUploadResponse> {

    private Integer upload_type;

    public Integer getUpload_type() {
        return upload_type;
    }

    public void setUpload_type(Integer upload_type) {
        this.upload_type = upload_type;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private Integer upload_type;

        public Integer getUpload_type() {
            return upload_type;
        }

        public void setUpload_type(Integer upload_type) {
            this.upload_type = upload_type;
        }

    }

    @Override
    public String getApiMethodName() {
        return "goodlife.v1.item.pic.upload";
    }

    @Override
    public Class<GoodlifeV1ItemPicUploadResponse> getResponseClass() {
        return GoodlifeV1ItemPicUploadResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.POST;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/goodlife/v1/item/pic/upload";
    }
}
