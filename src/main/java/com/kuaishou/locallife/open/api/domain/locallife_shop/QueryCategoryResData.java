package com.kuaishou.locallife.open.api.domain.locallife_shop;

import java.util.List;

/**
 * auto generate code
 */
public class QueryCategoryResData {
    private List<CategoryInfo> category_infos;
    private String description;
    private Integer error_code;

    public List<CategoryInfo> getCategory_infos() {
        return category_infos;
    }

    public void setCategory_infos(List<CategoryInfo> category_infos) {
        this.category_infos = category_infos;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getError_code() {
        return error_code;
    }

    public void setError_code(Integer error_code) {
        this.error_code = error_code;
    }

}
