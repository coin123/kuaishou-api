package com.kuaishou.locallife.open.api.domain.locallife_shop;


/**
 * auto generate code
 */
public class LegalPerson {
    private String id_card_front_url;
    private String id_card_back_url;

    public String getId_card_front_url() {
        return id_card_front_url;
    }

    public void setId_card_front_url(String id_card_front_url) {
        this.id_card_front_url = id_card_front_url;
    }

    public String getId_card_back_url() {
        return id_card_back_url;
    }

    public void setId_card_back_url(String id_card_back_url) {
        this.id_card_back_url = id_card_back_url;
    }

}
