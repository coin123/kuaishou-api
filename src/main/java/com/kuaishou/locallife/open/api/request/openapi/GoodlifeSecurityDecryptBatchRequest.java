package com.kuaishou.locallife.open.api.request.openapi;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.domain.openapi.CipherInfoRequest;
import com.kuaishou.locallife.open.api.response.openapi.GoodlifeSecurityDecryptBatchResponse;

/**
 * auto generate code
 */
public class GoodlifeSecurityDecryptBatchRequest extends AbstractKsLocalLifeRequest<GoodlifeSecurityDecryptBatchResponse> {

    private List<CipherInfoRequest> cipher_infos;

    public List<CipherInfoRequest> getCipher_infos() {
        return cipher_infos;
    }

    public void setCipher_infos(List<CipherInfoRequest> cipher_infos) {
        this.cipher_infos = cipher_infos;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private List<CipherInfoRequest> cipher_infos;

        public List<CipherInfoRequest> getCipher_infos() {
            return cipher_infos;
        }

        public void setCipher_infos(List<CipherInfoRequest> cipher_infos) {
            this.cipher_infos = cipher_infos;
        }

    }

    @Override
    public String getApiMethodName() {
        return "goodlife.security.decrypt.batch";
    }

    @Override
    public Class<GoodlifeSecurityDecryptBatchResponse> getResponseClass() {
        return GoodlifeSecurityDecryptBatchResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.POST;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/goodlife/security/decrypt/batch";
    }
}
