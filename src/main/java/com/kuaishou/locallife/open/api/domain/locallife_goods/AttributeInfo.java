package com.kuaishou.locallife.open.api.domain.locallife_goods;


/**
 * auto generate code
 */
public class AttributeInfo {
    private String value_type;
    private String value_demo;
    private String name;
    private String key;
    private Boolean required;
    private String desc;
    private Boolean multi;

    public String getValue_type() {
        return value_type;
    }

    public void setValue_type(String value_type) {
        this.value_type = value_type;
    }

    public String getValue_demo() {
        return value_demo;
    }

    public void setValue_demo(String value_demo) {
        this.value_demo = value_demo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Boolean getRequired() {
        return required;
    }

    public void setRequired(Boolean required) {
        this.required = required;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Boolean getMulti() {
        return multi;
    }

    public void setMulti(Boolean multi) {
        this.multi = multi;
    }

}
