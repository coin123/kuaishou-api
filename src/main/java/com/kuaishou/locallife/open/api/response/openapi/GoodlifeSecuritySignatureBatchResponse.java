package com.kuaishou.locallife.open.api.response.openapi;


import com.kuaishou.locallife.open.api.domain.openapi.BatchSignatureResponse;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class GoodlifeSecuritySignatureBatchResponse extends KsLocalLifeResponse {

    private BatchSignatureResponse data;

    public BatchSignatureResponse getData() {
        return data;
    }

    public void setData(BatchSignatureResponse data) {
        this.data = data;
    }

}