package com.kuaishou.locallife.open.api.domain.locallife_marketing;


/**
 * auto generate code
 */
public class MeituanItemCampaignInfo {
    private String title;
    private Integer reduce;
    private String limit_info;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getReduce() {
        return reduce;
    }

    public void setReduce(Integer reduce) {
        this.reduce = reduce;
    }

    public String getLimit_info() {
        return limit_info;
    }

    public void setLimit_info(String limit_info) {
        this.limit_info = limit_info;
    }

}
