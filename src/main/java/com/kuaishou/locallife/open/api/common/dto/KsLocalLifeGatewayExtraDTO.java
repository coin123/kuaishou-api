package com.kuaishou.locallife.open.api.common.dto;

import com.google.gson.annotations.SerializedName;

/**
 * @author gaojiapei <gaojiapei@kuaishou.com>
 * Created on 2023-02-05
 */
public class KsLocalLifeGatewayExtraDTO {

    /**
     * 返回0为成功
     */
    public static final int SUCCESS_VALUE = 0;
    public static final String SUCCESS_MSG = "成功";

    /**
     * 返回错误码
     */
    @SerializedName("error_code")
    private int errorCode;

    /**
     * 返回错误码描述
     */
    @SerializedName("description")
    private String description;

    /**
     * 返回子错误码
     */
    @SerializedName("sub_error_code")
    private int subErrorCode;

    /**
     * 返回子错误码描述
     */
    @SerializedName("sub_description")
    private String subDescription;

    /**
     * 请求Id
     */
    @SerializedName("logid")
    private String logId;

    /**
     * 接口响应时间戳 单位毫秒
     */
    @SerializedName("now")
    private String now;

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getSubErrorCode() {
        return subErrorCode;
    }

    public void setSubErrorCode(int subErrorCode) {
        this.subErrorCode = subErrorCode;
    }

    public String getSubDescription() {
        return subDescription;
    }

    public void setSubDescription(String subDescription) {
        this.subDescription = subDescription;
    }

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public String getNow() {
        return now;
    }

    public void setNow(String now) {
        this.now = now;
    }
}
