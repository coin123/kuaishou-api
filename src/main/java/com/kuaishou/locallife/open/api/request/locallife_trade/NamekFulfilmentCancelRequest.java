package com.kuaishou.locallife.open.api.request.locallife_trade;

import java.util.HashMap;
import java.util.Map;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.domain.locallife_trade.TradeOpenApiCancelVerifyReq;
import com.kuaishou.locallife.open.api.response.locallife_trade.NamekFulfilmentCancelResponse;

/**
 * auto generate code
 */
public class NamekFulfilmentCancelRequest extends AbstractKsLocalLifeRequest<NamekFulfilmentCancelResponse> {

    private TradeOpenApiCancelVerifyReq cancelVerifyRequest;

    public TradeOpenApiCancelVerifyReq getCancelVerifyRequest() {
        return cancelVerifyRequest;
    }

    public void setCancelVerifyRequest(TradeOpenApiCancelVerifyReq cancelVerifyRequest) {
        this.cancelVerifyRequest = cancelVerifyRequest;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private TradeOpenApiCancelVerifyReq cancelVerifyRequest;

        public TradeOpenApiCancelVerifyReq getCancelVerifyRequest() {
            return cancelVerifyRequest;
        }

        public void setCancelVerifyRequest(TradeOpenApiCancelVerifyReq cancelVerifyRequest) {
            this.cancelVerifyRequest = cancelVerifyRequest;
        }

    }

    @Override
    public String getApiMethodName() {
        return "namek.fulfilment.cancel";
    }

    @Override
    public Class<NamekFulfilmentCancelResponse> getResponseClass() {
        return NamekFulfilmentCancelResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.POST;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/namek/fulfilment/cancel";
    }
}
