package com.kuaishou.locallife.open.api.domain.locallife_third_code;


/**
 * auto generate code
 */
public class GrantVoucherAmount {
    private Long originalAmount;
    private Long payAmount;
    private Long ticketAmount;
    private Long merchantTicketAmount;
    private Long feeAmount;
    private Long commissionAmount;
    private Long paymentDiscountAmount;
    private Long couponPayAmount;
    private Long orderDiscountAmount;
    private Long distributorRate;
    private Long marketPrice;

    public Long getOriginalAmount() {
        return originalAmount;
    }

    public void setOriginalAmount(Long originalAmount) {
        this.originalAmount = originalAmount;
    }

    public Long getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(Long payAmount) {
        this.payAmount = payAmount;
    }

    public Long getTicketAmount() {
        return ticketAmount;
    }

    public void setTicketAmount(Long ticketAmount) {
        this.ticketAmount = ticketAmount;
    }

    public Long getMerchantTicketAmount() {
        return merchantTicketAmount;
    }

    public void setMerchantTicketAmount(Long merchantTicketAmount) {
        this.merchantTicketAmount = merchantTicketAmount;
    }

    public Long getFeeAmount() {
        return feeAmount;
    }

    public void setFeeAmount(Long feeAmount) {
        this.feeAmount = feeAmount;
    }

    public Long getCommissionAmount() {
        return commissionAmount;
    }

    public void setCommissionAmount(Long commissionAmount) {
        this.commissionAmount = commissionAmount;
    }

    public Long getPaymentDiscountAmount() {
        return paymentDiscountAmount;
    }

    public void setPaymentDiscountAmount(Long paymentDiscountAmount) {
        this.paymentDiscountAmount = paymentDiscountAmount;
    }

    public Long getCouponPayAmount() {
        return couponPayAmount;
    }

    public void setCouponPayAmount(Long couponPayAmount) {
        this.couponPayAmount = couponPayAmount;
    }

    public Long getOrderDiscountAmount() {
        return orderDiscountAmount;
    }

    public void setOrderDiscountAmount(Long orderDiscountAmount) {
        this.orderDiscountAmount = orderDiscountAmount;
    }

    public Long getDistributorRate() {
        return distributorRate;
    }

    public void setDistributorRate(Long distributorRate) {
        this.distributorRate = distributorRate;
    }

    public Long getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(Long marketPrice) {
        this.marketPrice = marketPrice;
    }

}
