package com.kuaishou.locallife.open.api.domain.settle_deal;


/**
 * auto generate code
 */
public class OpenApiBillQueryResponse {
    private Long error_code;
    private String description;
    private String bill_url;

    public Long getError_code() {
        return error_code;
    }

    public void setError_code(Long error_code) {
        this.error_code = error_code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBill_url() {
        return bill_url;
    }

    public void setBill_url(String bill_url) {
        this.bill_url = bill_url;
    }

}
