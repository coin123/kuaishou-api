package com.kuaishou.locallife.open.api.request.locallife_trade;

import java.util.HashMap;
import java.util.Map;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.domain.locallife_trade.CodeWithTime;
import com.kuaishou.locallife.open.api.response.locallife_trade.NamekFulfilmentVerifyResponse;

/**
 * auto generate code
 */
public class NamekFulfilmentVerifyRequest extends AbstractKsLocalLifeRequest<NamekFulfilmentVerifyResponse> {

    private String verify_token;
    private String poi_id;
    private String[] encrypted_codes;
    private String[] codes;
    private String order_id;
    private CodeWithTime[] code_with_time_list;

    public String getVerify_token() {
        return verify_token;
    }

    public void setVerify_token(String verify_token) {
        this.verify_token = verify_token;
    }
    public String getPoi_id() {
        return poi_id;
    }

    public void setPoi_id(String poi_id) {
        this.poi_id = poi_id;
    }
    public String[] getEncrypted_codes() {
        return encrypted_codes;
    }

    public void setEncrypted_codes(String[] encrypted_codes) {
        this.encrypted_codes = encrypted_codes;
    }
    public String[] getCodes() {
        return codes;
    }

    public void setCodes(String[] codes) {
        this.codes = codes;
    }
    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }
    public CodeWithTime[] getCode_with_time_list() {
        return code_with_time_list;
    }

    public void setCode_with_time_list(CodeWithTime[] code_with_time_list) {
        this.code_with_time_list = code_with_time_list;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private String verify_token;
        private String poi_id;
        private String[] encrypted_codes;
        private String[] codes;
        private String order_id;
        private CodeWithTime[] code_with_time_list;

        public String getVerify_token() {
            return verify_token;
        }

        public void setVerify_token(String verify_token) {
            this.verify_token = verify_token;
        }

        public String getPoi_id() {
            return poi_id;
        }

        public void setPoi_id(String poi_id) {
            this.poi_id = poi_id;
        }

        public String[] getEncrypted_codes() {
            return encrypted_codes;
        }

        public void setEncrypted_codes(String[] encrypted_codes) {
            this.encrypted_codes = encrypted_codes;
        }

        public String[] getCodes() {
            return codes;
        }

        public void setCodes(String[] codes) {
            this.codes = codes;
        }

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public CodeWithTime[] getCode_with_time_list() {
            return code_with_time_list;
        }

        public void setCode_with_time_list(CodeWithTime[] code_with_time_list) {
            this.code_with_time_list = code_with_time_list;
        }

    }

    @Override
    public String getApiMethodName() {
        return "namek.fulfilment.verify";
    }

    @Override
    public Class<NamekFulfilmentVerifyResponse> getResponseClass() {
        return NamekFulfilmentVerifyResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.POST;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/namek/fulfilment/verify";
    }
}
