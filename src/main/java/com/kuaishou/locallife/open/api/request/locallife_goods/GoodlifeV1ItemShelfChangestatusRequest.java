package com.kuaishou.locallife.open.api.request.locallife_goods;

import java.util.HashMap;
import java.util.Map;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.response.locallife_goods.GoodlifeV1ItemShelfChangestatusResponse;

/**
 * auto generate code
 */
public class GoodlifeV1ItemShelfChangestatusRequest extends AbstractKsLocalLifeRequest<GoodlifeV1ItemShelfChangestatusResponse> {

    private String product_id;
    private Long op_type;
    private String out_id;

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }
    public Long getOp_type() {
        return op_type;
    }

    public void setOp_type(Long op_type) {
        this.op_type = op_type;
    }
    public String getOut_id() {
        return out_id;
    }

    public void setOut_id(String out_id) {
        this.out_id = out_id;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private String product_id;
        private Long op_type;
        private String out_id;

        public String getProduct_id() {
            return product_id;
        }

        public void setProduct_id(String product_id) {
            this.product_id = product_id;
        }

        public Long getOp_type() {
            return op_type;
        }

        public void setOp_type(Long op_type) {
            this.op_type = op_type;
        }

        public String getOut_id() {
            return out_id;
        }

        public void setOut_id(String out_id) {
            this.out_id = out_id;
        }

    }

    @Override
    public String getApiMethodName() {
        return "goodlife.v1.item.shelf.changestatus";
    }

    @Override
    public Class<GoodlifeV1ItemShelfChangestatusResponse> getResponseClass() {
        return GoodlifeV1ItemShelfChangestatusResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.POST;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/goodlife/v1/item/shelf/changestatus";
    }
}
