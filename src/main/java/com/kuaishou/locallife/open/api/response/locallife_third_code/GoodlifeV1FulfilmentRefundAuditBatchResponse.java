package com.kuaishou.locallife.open.api.response.locallife_third_code;


import com.kuaishou.locallife.open.api.domain.locallife_third_code.RefundAuditBatchRespData;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class GoodlifeV1FulfilmentRefundAuditBatchResponse extends KsLocalLifeResponse {

    private RefundAuditBatchRespData data;

    public RefundAuditBatchRespData getData() {
        return data;
    }

    public void setData(RefundAuditBatchRespData data) {
        this.data = data;
    }

}