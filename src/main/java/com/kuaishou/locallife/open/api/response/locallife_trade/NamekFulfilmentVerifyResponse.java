package com.kuaishou.locallife.open.api.response.locallife_trade;


import com.kuaishou.locallife.open.api.domain.locallife_trade.Data;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class NamekFulfilmentVerifyResponse extends KsLocalLifeResponse {

    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}