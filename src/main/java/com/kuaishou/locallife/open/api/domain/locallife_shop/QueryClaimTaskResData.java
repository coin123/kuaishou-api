package com.kuaishou.locallife.open.api.domain.locallife_shop;

import java.util.List;

/**
 * auto generate code
 */
public class QueryClaimTaskResData {
    private List<TaskResult> task_results;
    private String description;
    private Integer error_code;

    public List<TaskResult> getTask_results() {
        return task_results;
    }

    public void setTask_results(List<TaskResult> task_results) {
        this.task_results = task_results;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getError_code() {
        return error_code;
    }

    public void setError_code(Integer error_code) {
        this.error_code = error_code;
    }

}
