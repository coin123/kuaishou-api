package com.kuaishou.locallife.open.api.domain.locallife_order;


/**
 * auto generate code
 */
public class CertificateInfos {
    private String order_item_id;
    private String certificate_id;
    private Integer item_status;
    private Long item_update_time;
    private Integer refund_amount;
    private Long refund_time;

    public String getOrder_item_id() {
        return order_item_id;
    }

    public void setOrder_item_id(String order_item_id) {
        this.order_item_id = order_item_id;
    }

    public String getCertificate_id() {
        return certificate_id;
    }

    public void setCertificate_id(String certificate_id) {
        this.certificate_id = certificate_id;
    }

    public Integer getItem_status() {
        return item_status;
    }

    public void setItem_status(Integer item_status) {
        this.item_status = item_status;
    }

    public Long getItem_update_time() {
        return item_update_time;
    }

    public void setItem_update_time(Long item_update_time) {
        this.item_update_time = item_update_time;
    }

    public Integer getRefund_amount() {
        return refund_amount;
    }

    public void setRefund_amount(Integer refund_amount) {
        this.refund_amount = refund_amount;
    }

    public Long getRefund_time() {
        return refund_time;
    }

    public void setRefund_time(Long refund_time) {
        this.refund_time = refund_time;
    }

}
