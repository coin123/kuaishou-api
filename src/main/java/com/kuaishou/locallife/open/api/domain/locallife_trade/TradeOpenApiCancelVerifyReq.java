package com.kuaishou.locallife.open.api.domain.locallife_trade;


/**
 * auto generate code
 */
public class TradeOpenApiCancelVerifyReq {
    private String verify_id;
    private String certificate_id;

    public String getVerify_id() {
        return verify_id;
    }

    public void setVerify_id(String verify_id) {
        this.verify_id = verify_id;
    }

    public String getCertificate_id() {
        return certificate_id;
    }

    public void setCertificate_id(String certificate_id) {
        this.certificate_id = certificate_id;
    }

}
