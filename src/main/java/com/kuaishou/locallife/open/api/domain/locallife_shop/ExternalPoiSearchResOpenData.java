package com.kuaishou.locallife.open.api.domain.locallife_shop;


/**
 * auto generate code
 */
public class ExternalPoiSearchResOpenData {
    private ExternalPoiSearchResData data;
    private String description;
    private Integer error_code;

    public ExternalPoiSearchResData getData() {
        return data;
    }

    public void setData(ExternalPoiSearchResData data) {
        this.data = data;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getError_code() {
        return error_code;
    }

    public void setError_code(Integer error_code) {
        this.error_code = error_code;
    }

}
