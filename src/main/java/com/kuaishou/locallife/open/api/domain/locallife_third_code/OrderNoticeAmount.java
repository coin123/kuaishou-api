package com.kuaishou.locallife.open.api.domain.locallife_third_code;


/**
 * auto generate code
 */
public class OrderNoticeAmount {
    private Long orderDiscountAmount;
    private Long payAmount;
    private Long originalAmount;
    private Long ticketAmount;
    private Long merchantTicketAmount;
    private Long marketPrice;

    public Long getOrderDiscountAmount() {
        return orderDiscountAmount;
    }

    public void setOrderDiscountAmount(Long orderDiscountAmount) {
        this.orderDiscountAmount = orderDiscountAmount;
    }

    public Long getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(Long payAmount) {
        this.payAmount = payAmount;
    }

    public Long getOriginalAmount() {
        return originalAmount;
    }

    public void setOriginalAmount(Long originalAmount) {
        this.originalAmount = originalAmount;
    }

    public Long getTicketAmount() {
        return ticketAmount;
    }

    public void setTicketAmount(Long ticketAmount) {
        this.ticketAmount = ticketAmount;
    }

    public Long getMerchantTicketAmount() {
        return merchantTicketAmount;
    }

    public void setMerchantTicketAmount(Long merchantTicketAmount) {
        this.merchantTicketAmount = merchantTicketAmount;
    }

    public Long getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(Long marketPrice) {
        this.marketPrice = marketPrice;
    }

}
