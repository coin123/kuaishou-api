package com.kuaishou.locallife.open.api.common.http;

/**
 * @author gaojiapei <gaojiapei@kuaishou.com>
 * Created on 2023-02-02
 */
public class HttpCommonResponse {

    private String response;

    private String requestUrl;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }
}
