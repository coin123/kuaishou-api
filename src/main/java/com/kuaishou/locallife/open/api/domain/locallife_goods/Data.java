package com.kuaishou.locallife.open.api.domain.locallife_goods;

import java.util.List;

/**
 * auto generate code
 */
public class Data {
    private Long error_code;
    private String description;
    private List<CategoryinfoBO> category_infos;

    public Long getError_code() {
        return error_code;
    }

    public void setError_code(Long error_code) {
        this.error_code = error_code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<CategoryinfoBO> getCategory_infos() {
        return category_infos;
    }

    public void setCategory_infos(List<CategoryinfoBO> category_infos) {
        this.category_infos = category_infos;
    }

}
