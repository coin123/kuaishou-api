package com.kuaishou.locallife.open.api.domain.locallife_marketing;


/**
 * auto generate code
 */
public class MeituanOrderQuoteResp {
    private Long error_code;
    private String description;
    private MeituanOrderProductInfo productInfo;
    private MeituanOrderPromotion promotionInfo;

    public Long getError_code() {
        return error_code;
    }

    public void setError_code(Long error_code) {
        this.error_code = error_code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public MeituanOrderProductInfo getProductInfo() {
        return productInfo;
    }

    public void setProductInfo(MeituanOrderProductInfo productInfo) {
        this.productInfo = productInfo;
    }

    public MeituanOrderPromotion getPromotionInfo() {
        return promotionInfo;
    }

    public void setPromotionInfo(MeituanOrderPromotion promotionInfo) {
        this.promotionInfo = promotionInfo;
    }

}
