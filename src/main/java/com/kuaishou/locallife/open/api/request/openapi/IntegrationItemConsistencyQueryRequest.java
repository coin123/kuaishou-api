package com.kuaishou.locallife.open.api.request.openapi;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.domain.openapi.Item;
import com.kuaishou.locallife.open.api.response.openapi.IntegrationItemConsistencyQueryResponse;

/**
 * auto generate code
 */
public class IntegrationItemConsistencyQueryRequest extends AbstractKsLocalLifeRequest<IntegrationItemConsistencyQueryResponse> {

    private String appkey;
    private Long accountId;
    private List<Item> itemList;

    public String getAppkey() {
        return appkey;
    }

    public void setAppkey(String appkey) {
        this.appkey = appkey;
    }
    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }
    public List<Item> getItemList() {
        return itemList;
    }

    public void setItemList(List<Item> itemList) {
        this.itemList = itemList;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private String appkey;
        private Long accountId;
        private List<Item> itemList;

        public String getAppkey() {
            return appkey;
        }

        public void setAppkey(String appkey) {
            this.appkey = appkey;
        }

        public Long getAccountId() {
            return accountId;
        }

        public void setAccountId(Long accountId) {
            this.accountId = accountId;
        }

        public List<Item> getItemList() {
            return itemList;
        }

        public void setItemList(List<Item> itemList) {
            this.itemList = itemList;
        }

    }

    @Override
    public String getApiMethodName() {
        return "integration.item.consistency.query";
    }

    @Override
    public Class<IntegrationItemConsistencyQueryResponse> getResponseClass() {
        return IntegrationItemConsistencyQueryResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.POST;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/integration/item/consistency/query";
    }
}
