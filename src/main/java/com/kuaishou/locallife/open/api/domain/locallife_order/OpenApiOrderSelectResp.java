package com.kuaishou.locallife.open.api.domain.locallife_order;


/**
 * auto generate code
 */
public class OpenApiOrderSelectResp {
    private Long error_code;
    private String description;
    private OpenApiOrderInfo[] orders;
    private Page page;

    public Long getError_code() {
        return error_code;
    }

    public void setError_code(Long error_code) {
        this.error_code = error_code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public OpenApiOrderInfo[] getOrders() {
        return orders;
    }

    public void setOrders(OpenApiOrderInfo[] orders) {
        this.orders = orders;
    }

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

}
