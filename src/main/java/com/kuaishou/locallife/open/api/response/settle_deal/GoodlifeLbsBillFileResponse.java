package com.kuaishou.locallife.open.api.response.settle_deal;


import com.kuaishou.locallife.open.api.domain.settle_deal.OpenApiBillQueryResponse;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class GoodlifeLbsBillFileResponse extends KsLocalLifeResponse {

    private OpenApiBillQueryResponse data;

    public OpenApiBillQueryResponse getData() {
        return data;
    }

    public void setData(OpenApiBillQueryResponse data) {
        this.data = data;
    }

}