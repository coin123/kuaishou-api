package com.kuaishou.locallife.open.api.domain.locallife_shop;


/**
 * auto generate code
 */
public class AssociationResult {
    private String association_message;
    private String ext_poi_id;
    private String poi_id;
    private String association_status;

    public String getAssociation_message() {
        return association_message;
    }

    public void setAssociation_message(String association_message) {
        this.association_message = association_message;
    }

    public String getExt_poi_id() {
        return ext_poi_id;
    }

    public void setExt_poi_id(String ext_poi_id) {
        this.ext_poi_id = ext_poi_id;
    }

    public String getPoi_id() {
        return poi_id;
    }

    public void setPoi_id(String poi_id) {
        this.poi_id = poi_id;
    }

    public String getAssociation_status() {
        return association_status;
    }

    public void setAssociation_status(String association_status) {
        this.association_status = association_status;
    }

}
