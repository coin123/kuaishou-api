package com.kuaishou.locallife.open.api.request.locallife_third_code;

import java.util.HashMap;
import java.util.Map;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.domain.locallife_third_code.GrantsVoucherCodeInfo;
import com.kuaishou.locallife.open.api.response.locallife_third_code.GoodlifeV1FulfilmentCreateCallbackResponse;

/**
 * auto generate code
 */
public class GoodlifeV1FulfilmentCreateCallbackRequest extends AbstractKsLocalLifeRequest<GoodlifeV1FulfilmentCreateCallbackResponse> {

    private String order_id;
    private String[] codes;
    private GrantsVoucherCodeInfo[] codes_list;
    private String out_order_id;
    private Integer create_result;
    private Integer fail_reason;

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }
    public String[] getCodes() {
        return codes;
    }

    public void setCodes(String[] codes) {
        this.codes = codes;
    }
    public GrantsVoucherCodeInfo[] getCodes_list() {
        return codes_list;
    }

    public void setCodes_list(GrantsVoucherCodeInfo[] codes_list) {
        this.codes_list = codes_list;
    }
    public String getOut_order_id() {
        return out_order_id;
    }

    public void setOut_order_id(String out_order_id) {
        this.out_order_id = out_order_id;
    }
    public Integer getCreate_result() {
        return create_result;
    }

    public void setCreate_result(Integer create_result) {
        this.create_result = create_result;
    }
    public Integer getFail_reason() {
        return fail_reason;
    }

    public void setFail_reason(Integer fail_reason) {
        this.fail_reason = fail_reason;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private String order_id;
        private String[] codes;
        private GrantsVoucherCodeInfo[] codes_list;
        private String out_order_id;
        private Integer create_result;
        private Integer fail_reason;

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public String[] getCodes() {
            return codes;
        }

        public void setCodes(String[] codes) {
            this.codes = codes;
        }

        public GrantsVoucherCodeInfo[] getCodes_list() {
            return codes_list;
        }

        public void setCodes_list(GrantsVoucherCodeInfo[] codes_list) {
            this.codes_list = codes_list;
        }

        public String getOut_order_id() {
            return out_order_id;
        }

        public void setOut_order_id(String out_order_id) {
            this.out_order_id = out_order_id;
        }

        public Integer getCreate_result() {
            return create_result;
        }

        public void setCreate_result(Integer create_result) {
            this.create_result = create_result;
        }

        public Integer getFail_reason() {
            return fail_reason;
        }

        public void setFail_reason(Integer fail_reason) {
            this.fail_reason = fail_reason;
        }

    }

    @Override
    public String getApiMethodName() {
        return "goodlife.v1.fulfilment.create.callback";
    }

    @Override
    public Class<GoodlifeV1FulfilmentCreateCallbackResponse> getResponseClass() {
        return GoodlifeV1FulfilmentCreateCallbackResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.POST;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/goodlife/v1/fulfilment/create/callback";
    }
}
