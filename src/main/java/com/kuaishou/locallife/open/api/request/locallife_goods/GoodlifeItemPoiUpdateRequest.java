package com.kuaishou.locallife.open.api.request.locallife_goods;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.response.locallife_goods.GoodlifeItemPoiUpdateResponse;

/**
 * auto generate code
 */
public class GoodlifeItemPoiUpdateRequest extends AbstractKsLocalLifeRequest<GoodlifeItemPoiUpdateResponse> {

    private List<String> reduce_poi_ids;
    private Long product_id;
    private List<String> add_poi_ids;

    public List<String> getReduce_poi_ids() {
        return reduce_poi_ids;
    }

    public void setReduce_poi_ids(List<String> reduce_poi_ids) {
        this.reduce_poi_ids = reduce_poi_ids;
    }
    public Long getProduct_id() {
        return product_id;
    }

    public void setProduct_id(Long product_id) {
        this.product_id = product_id;
    }
    public List<String> getAdd_poi_ids() {
        return add_poi_ids;
    }

    public void setAdd_poi_ids(List<String> add_poi_ids) {
        this.add_poi_ids = add_poi_ids;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private List<String> reduce_poi_ids;
        private Long product_id;
        private List<String> add_poi_ids;

        public List<String> getReduce_poi_ids() {
            return reduce_poi_ids;
        }

        public void setReduce_poi_ids(List<String> reduce_poi_ids) {
            this.reduce_poi_ids = reduce_poi_ids;
        }

        public Long getProduct_id() {
            return product_id;
        }

        public void setProduct_id(Long product_id) {
            this.product_id = product_id;
        }

        public List<String> getAdd_poi_ids() {
            return add_poi_ids;
        }

        public void setAdd_poi_ids(List<String> add_poi_ids) {
            this.add_poi_ids = add_poi_ids;
        }

    }

    @Override
    public String getApiMethodName() {
        return "goodlife.item.poi.update";
    }

    @Override
    public Class<GoodlifeItemPoiUpdateResponse> getResponseClass() {
        return GoodlifeItemPoiUpdateResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.POST;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/goodlife/item/poi/update";
    }
}
