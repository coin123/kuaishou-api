package com.kuaishou.locallife.open.api;

import java.util.HashMap;
import java.util.Map;

/**
 * 自动生成请求模板类
 * @author gaojiapei <gaojiapei@kuaishou.com>
 * Created on 2023-02-02
 */
public abstract class AbstractKsLocalLifeRequest<T extends KsLocalLifeResponse> implements KsLocalLifeRequest<T> {
    @Override
    public Map<String, String> getHeaderParams() {
        return new HashMap<>();
    }

    /**
     * 自行实现 用与Get方式调用
     */
    public abstract Map<String, String> getBusinessParams();

    /**
     * Post请求
     */
    @Override
    public String getApiReqJson() {
        return generateObjJson();
    }

    public abstract String generateObjJson();

    @Override
    public Map<String, String> getApiParams() {
        Map<String, String> apiParamsMap = new HashMap<String, String>();
        // 业务参数放进去
        apiParamsMap.putAll(getBusinessParams());

        return apiParamsMap;
    }

    @Override
    public String getRequestSpecialPath() {
        // eg:integration.callback.locallife.trade.poi.change -> /integration/callback/locallife/trade/poi/change
        String apiName = this.getApiMethodName();
        String uri = apiName.replace(".", "/");
        return "/" + uri;
    }
}
