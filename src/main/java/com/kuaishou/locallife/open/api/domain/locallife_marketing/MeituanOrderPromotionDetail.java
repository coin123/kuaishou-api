package com.kuaishou.locallife.open.api.domain.locallife_marketing;


/**
 * auto generate code
 */
public class MeituanOrderPromotionDetail {
    private String title;
    private String tag;
    private String reduce;
    private Boolean isavailable;
    private String unavailablereason;
    private String minconsumption;
    private String text;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getReduce() {
        return reduce;
    }

    public void setReduce(String reduce) {
        this.reduce = reduce;
    }

    public Boolean getIsavailable() {
        return isavailable;
    }

    public void setIsavailable(Boolean isavailable) {
        this.isavailable = isavailable;
    }

    public String getUnavailablereason() {
        return unavailablereason;
    }

    public void setUnavailablereason(String unavailablereason) {
        this.unavailablereason = unavailablereason;
    }

    public String getMinconsumption() {
        return minconsumption;
    }

    public void setMinconsumption(String minconsumption) {
        this.minconsumption = minconsumption;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
