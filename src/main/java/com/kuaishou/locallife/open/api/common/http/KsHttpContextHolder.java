package com.kuaishou.locallife.open.api.common.http;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.kuaishou.locallife.open.api.KsLocalLifeResponse.Requests;

/**
 * 请求上下文包装holder
 *
 * @author shuxiaohui <shuxiaohui@kuaishou.com>
 * Created on 2020-03-31
 */
public class KsHttpContextHolder {

    // 请求完整url
    private String requestUrl;

    // 请求响应信息
    private String responseBody;

    // 请求响应字节流
    private byte[] responseByes;

    private Map<String, List<String>> responseHeaders = new HashMap<String, List<String>>();

    // 系统顶级必须参数
    private Map<String, String> serverMustParams;

    // 业务必须参数
    private Map<String, String> businessParams;

    /**
     * 请求参数requests
     */
    private Requests requests;



    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }

    public Map<String, String> getServerMustParams() {
        return serverMustParams;
    }

    public void setServerMustParams(Map<String, String> serverMustParams) {
        this.serverMustParams = serverMustParams;
    }

    public Map<String, String> getBusinessParams() {
        return businessParams;
    }

    public void setBusinessParams(Map<String, String> businessParams) {
        this.businessParams = businessParams;
    }

    public String getResponseBody() {
        return responseBody;
    }

    public void setResponseBody(String responseBody) {
        this.responseBody = responseBody;
    }

    public byte[] getResponseByes() {
        return responseByes;
    }

    public void setResponseByes(byte[] responseByes) {
        this.responseByes = responseByes;
    }

    public Map<String, List<String>> getResponseHeaders() {
        return responseHeaders;
    }

    public void setResponseHeaders(Map<String, List<String>> responseHeaders) {
        this.responseHeaders = responseHeaders;
    }

    public Requests getRequests() {
        return requests;
    }

    public void setRequests(Requests requests) {
        this.requests = requests;
    }

    public Map<String, String> getAllParams() {
        Map<String, String> params = new HashMap<String, String>();
        if (this.serverMustParams != null && !this.serverMustParams.isEmpty()) {
            params.putAll(this.serverMustParams);
        }

        if (this.businessParams != null && !this.businessParams.isEmpty()) {
            params.putAll(this.businessParams);
        }

        return params;
    }
}
