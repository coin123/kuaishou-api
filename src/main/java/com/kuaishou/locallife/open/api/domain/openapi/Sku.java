package com.kuaishou.locallife.open.api.domain.openapi;


/**
 * auto generate code
 */
public class Sku {
    private Long skuId;
    private String outSkuId;

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public String getOutSkuId() {
        return outSkuId;
    }

    public void setOutSkuId(String outSkuId) {
        this.outSkuId = outSkuId;
    }

}
