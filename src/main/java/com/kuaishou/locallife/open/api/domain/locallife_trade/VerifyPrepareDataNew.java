package com.kuaishou.locallife.open.api.domain.locallife_trade;

import java.util.List;

/**
 * auto generate code
 */
public class VerifyPrepareDataNew {
    private Long error_code;
    private String description;
    private String verify_token;
    private String order_id;
    private List<SimpleCertificate> certificates;

    public Long getError_code() {
        return error_code;
    }

    public void setError_code(Long error_code) {
        this.error_code = error_code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVerify_token() {
        return verify_token;
    }

    public void setVerify_token(String verify_token) {
        this.verify_token = verify_token;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public List<SimpleCertificate> getCertificates() {
        return certificates;
    }

    public void setCertificates(List<SimpleCertificate> certificates) {
        this.certificates = certificates;
    }

}
