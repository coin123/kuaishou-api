package com.kuaishou.locallife.open.api.domain.locallife_shop;


/**
 * auto generate code
 */
public class LegalPersonInfo {
    private String legal_cert_address;
    private String legal_cert_type;
    private String legal_cert_no;
    private String legal_cert_period_end;
    private String legal_name;
    private String legal_cert_back_picurl;
    private String legal_cert_period_start;
    private String legal_cert_front_picurl;

    public String getLegal_cert_address() {
        return legal_cert_address;
    }

    public void setLegal_cert_address(String legal_cert_address) {
        this.legal_cert_address = legal_cert_address;
    }

    public String getLegal_cert_type() {
        return legal_cert_type;
    }

    public void setLegal_cert_type(String legal_cert_type) {
        this.legal_cert_type = legal_cert_type;
    }

    public String getLegal_cert_no() {
        return legal_cert_no;
    }

    public void setLegal_cert_no(String legal_cert_no) {
        this.legal_cert_no = legal_cert_no;
    }

    public String getLegal_cert_period_end() {
        return legal_cert_period_end;
    }

    public void setLegal_cert_period_end(String legal_cert_period_end) {
        this.legal_cert_period_end = legal_cert_period_end;
    }

    public String getLegal_name() {
        return legal_name;
    }

    public void setLegal_name(String legal_name) {
        this.legal_name = legal_name;
    }

    public String getLegal_cert_back_picurl() {
        return legal_cert_back_picurl;
    }

    public void setLegal_cert_back_picurl(String legal_cert_back_picurl) {
        this.legal_cert_back_picurl = legal_cert_back_picurl;
    }

    public String getLegal_cert_period_start() {
        return legal_cert_period_start;
    }

    public void setLegal_cert_period_start(String legal_cert_period_start) {
        this.legal_cert_period_start = legal_cert_period_start;
    }

    public String getLegal_cert_front_picurl() {
        return legal_cert_front_picurl;
    }

    public void setLegal_cert_front_picurl(String legal_cert_front_picurl) {
        this.legal_cert_front_picurl = legal_cert_front_picurl;
    }

}
