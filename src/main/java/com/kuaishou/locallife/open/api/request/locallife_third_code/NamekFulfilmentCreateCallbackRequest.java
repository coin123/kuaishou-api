package com.kuaishou.locallife.open.api.request.locallife_third_code;

import java.util.HashMap;
import java.util.Map;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.response.locallife_third_code.NamekFulfilmentCreateCallbackResponse;

/**
 * auto generate code
 */
public class NamekFulfilmentCreateCallbackRequest extends AbstractKsLocalLifeRequest<NamekFulfilmentCreateCallbackResponse> {

    private String order_id;
    private String[] codes;

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }
    public String[] getCodes() {
        return codes;
    }

    public void setCodes(String[] codes) {
        this.codes = codes;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private String order_id;
        private String[] codes;

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public String[] getCodes() {
            return codes;
        }

        public void setCodes(String[] codes) {
            this.codes = codes;
        }

    }

    @Override
    public String getApiMethodName() {
        return "namek.fulfilment.create.callback";
    }

    @Override
    public Class<NamekFulfilmentCreateCallbackResponse> getResponseClass() {
        return NamekFulfilmentCreateCallbackResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.POST;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/namek/fulfilment/create/callback";
    }
}
