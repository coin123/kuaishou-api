package com.kuaishou.locallife.open.api.response.locallife_order;


import com.kuaishou.locallife.open.api.domain.locallife_order.OpenApiOrderSelectResp;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class GoodlifeV1TradeOrderQueryResponse extends KsLocalLifeResponse {

    private OpenApiOrderSelectResp data;

    public OpenApiOrderSelectResp getData() {
        return data;
    }

    public void setData(OpenApiOrderSelectResp data) {
        this.data = data;
    }

}