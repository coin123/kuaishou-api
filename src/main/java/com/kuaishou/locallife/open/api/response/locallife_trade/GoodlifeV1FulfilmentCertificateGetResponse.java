package com.kuaishou.locallife.open.api.response.locallife_trade;


import com.kuaishou.locallife.open.api.domain.locallife_trade.CertificateGetRespNew;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class GoodlifeV1FulfilmentCertificateGetResponse extends KsLocalLifeResponse {

    private CertificateGetRespNew data;

    public CertificateGetRespNew getData() {
        return data;
    }

    public void setData(CertificateGetRespNew data) {
        this.data = data;
    }

}