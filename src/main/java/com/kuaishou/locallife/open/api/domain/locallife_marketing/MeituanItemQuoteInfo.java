package com.kuaishou.locallife.open.api.domain.locallife_marketing;


/**
 * auto generate code
 */
public class MeituanItemQuoteInfo {
    private String out_product_id;
    private MeituanItemPriceInfo price_info;
    private Boolean canbuy;
    private MeituanItemPromotionInfo promotion_info;

    public String getOut_product_id() {
        return out_product_id;
    }

    public void setOut_product_id(String out_product_id) {
        this.out_product_id = out_product_id;
    }

    public MeituanItemPriceInfo getPrice_info() {
        return price_info;
    }

    public void setPrice_info(MeituanItemPriceInfo price_info) {
        this.price_info = price_info;
    }

    public Boolean getCanbuy() {
        return canbuy;
    }

    public void setCanbuy(Boolean canbuy) {
        this.canbuy = canbuy;
    }

    public MeituanItemPromotionInfo getPromotion_info() {
        return promotion_info;
    }

    public void setPromotion_info(MeituanItemPromotionInfo promotion_info) {
        this.promotion_info = promotion_info;
    }

}
