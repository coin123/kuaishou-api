package com.kuaishou.locallife.open.api.response.ksoptest;


import com.kuaishou.locallife.open.api.domain.ksoptest.Result;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class GoodlifeTestItemShelfChangestatusResponse extends KsLocalLifeResponse {

    private Result data;

    public Result getData() {
        return data;
    }

    public void setData(Result data) {
        this.data = data;
    }

}