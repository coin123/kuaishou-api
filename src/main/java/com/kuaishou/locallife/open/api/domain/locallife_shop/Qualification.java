package com.kuaishou.locallife.open.api.domain.locallife_shop;


/**
 * auto generate code
 */
public class Qualification {
    private String qualification_url;

    public String getQualification_url() {
        return qualification_url;
    }

    public void setQualification_url(String qualification_url) {
        this.qualification_url = qualification_url;
    }

}
