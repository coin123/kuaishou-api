package com.kuaishou.locallife.open.api.response.open_message;


import com.kuaishou.locallife.open.api.domain.open_message.MessageRespDataTest;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class IntegrationIsvMessageTestResponse extends KsLocalLifeResponse {

    private MessageRespDataTest data;

    public MessageRespDataTest getData() {
        return data;
    }

    public void setData(MessageRespDataTest data) {
        this.data = data;
    }

}