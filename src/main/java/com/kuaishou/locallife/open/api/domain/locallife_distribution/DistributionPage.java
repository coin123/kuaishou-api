package com.kuaishou.locallife.open.api.domain.locallife_distribution;


/**
 * auto generate code
 */
public class DistributionPage {
    private Integer page;
    private Integer size;
    private Long total;

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

}
