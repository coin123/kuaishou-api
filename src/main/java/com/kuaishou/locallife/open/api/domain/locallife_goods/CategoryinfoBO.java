package com.kuaishou.locallife.open.api.domain.locallife_goods;

import java.util.List;

/**
 * auto generate code
 */
public class CategoryinfoBO {
    private Long category_id;
    private String name;
    private Long parent_id;
    private Integer level;
    private Boolean is_leaf;
    private List<ItemType> item_type;

    public Long getCategory_id() {
        return category_id;
    }

    public void setCategory_id(Long category_id) {
        this.category_id = category_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getParent_id() {
        return parent_id;
    }

    public void setParent_id(Long parent_id) {
        this.parent_id = parent_id;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Boolean getIs_leaf() {
        return is_leaf;
    }

    public void setIs_leaf(Boolean is_leaf) {
        this.is_leaf = is_leaf;
    }

    public List<ItemType> getItem_type() {
        return item_type;
    }

    public void setItem_type(List<ItemType> item_type) {
        this.item_type = item_type;
    }

}
