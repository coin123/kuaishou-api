package com.kuaishou.locallife.open.api.request.openapi;

import java.util.HashMap;
import java.util.Map;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.response.openapi.IntegrationMeituanCheckUserLoginResponse;

/**
 * auto generate code
 */
public class IntegrationMeituanCheckUserLoginRequest extends AbstractKsLocalLifeRequest<IntegrationMeituanCheckUserLoginResponse> {

    private String mtaccesstoken;
    private String mobile;
    private String appKey;

    public String getMtaccesstoken() {
        return mtaccesstoken;
    }

    public void setMtaccesstoken(String mtaccesstoken) {
        this.mtaccesstoken = mtaccesstoken;
    }
    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private String mtaccesstoken;
        private String mobile;
        private String appKey;

        public String getMtaccesstoken() {
            return mtaccesstoken;
        }

        public void setMtaccesstoken(String mtaccesstoken) {
            this.mtaccesstoken = mtaccesstoken;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getAppKey() {
            return appKey;
        }

        public void setAppKey(String appKey) {
            this.appKey = appKey;
        }

    }

    @Override
    public String getApiMethodName() {
        return "integration.meituan.check.user.login";
    }

    @Override
    public Class<IntegrationMeituanCheckUserLoginResponse> getResponseClass() {
        return IntegrationMeituanCheckUserLoginResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.POST;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/integration/meituan/check/user/login";
    }
}
