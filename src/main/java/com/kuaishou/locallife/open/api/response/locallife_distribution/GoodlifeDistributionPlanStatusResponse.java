package com.kuaishou.locallife.open.api.response.locallife_distribution;


import com.kuaishou.locallife.open.api.domain.locallife_distribution.Result;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class GoodlifeDistributionPlanStatusResponse extends KsLocalLifeResponse {

    private Result data;

    public Result getData() {
        return data;
    }

    public void setData(Result data) {
        this.data = data;
    }

}