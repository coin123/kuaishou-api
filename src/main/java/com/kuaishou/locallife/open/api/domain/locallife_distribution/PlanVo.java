package com.kuaishou.locallife.open.api.domain.locallife_distribution;


/**
 * auto generate code
 */
public class PlanVo {
    private Long item_id;
    private Long plan_id;

    public Long getItem_id() {
        return item_id;
    }

    public void setItem_id(Long item_id) {
        this.item_id = item_id;
    }

    public Long getPlan_id() {
        return plan_id;
    }

    public void setPlan_id(Long plan_id) {
        this.plan_id = plan_id;
    }

}
