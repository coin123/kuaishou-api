package com.kuaishou.locallife.open.api.domain.locallife_shop;

import java.util.List;

/**
 * auto generate code
 */
public class NewQualification {
    private List<String> qualification_urls;
    private Integer qualification_type;

    public List<String> getQualification_urls() {
        return qualification_urls;
    }

    public void setQualification_urls(List<String> qualification_urls) {
        this.qualification_urls = qualification_urls;
    }

    public Integer getQualification_type() {
        return qualification_type;
    }

    public void setQualification_type(Integer qualification_type) {
        this.qualification_type = qualification_type;
    }

}
