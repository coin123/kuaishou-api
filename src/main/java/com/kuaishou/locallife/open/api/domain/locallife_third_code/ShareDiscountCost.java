package com.kuaishou.locallife.open.api.domain.locallife_third_code;


/**
 * auto generate code
 */
public class ShareDiscountCost {
    private Long platform_cost;
    private Long third_platform_cost;

    public Long getPlatform_cost() {
        return platform_cost;
    }

    public void setPlatform_cost(Long platform_cost) {
        this.platform_cost = platform_cost;
    }

    public Long getThird_platform_cost() {
        return third_platform_cost;
    }

    public void setThird_platform_cost(Long third_platform_cost) {
        this.third_platform_cost = third_platform_cost;
    }

}
