package com.kuaishou.locallife.open.api.request.locallife_third_code;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.domain.locallife_third_code.Certificate;
import com.kuaishou.locallife.open.api.response.locallife_third_code.GoodlifeV1FulfilmentRefundCreateResponse;

/**
 * auto generate code
 */
public class GoodlifeV1FulfilmentRefundCreateRequest extends AbstractKsLocalLifeRequest<GoodlifeV1FulfilmentRefundCreateResponse> {

    private String idempotent_id;
    private String out_order_id;
    private String order_id;
    private Integer refund_type;
    private List<Certificate> certificates;
    private Long refund_amount;
    private String refund_reason;
    private String refund_ext;

    public String getIdempotent_id() {
        return idempotent_id;
    }

    public void setIdempotent_id(String idempotent_id) {
        this.idempotent_id = idempotent_id;
    }
    public String getOut_order_id() {
        return out_order_id;
    }

    public void setOut_order_id(String out_order_id) {
        this.out_order_id = out_order_id;
    }
    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }
    public Integer getRefund_type() {
        return refund_type;
    }

    public void setRefund_type(Integer refund_type) {
        this.refund_type = refund_type;
    }
    public List<Certificate> getCertificates() {
        return certificates;
    }

    public void setCertificates(List<Certificate> certificates) {
        this.certificates = certificates;
    }
    public Long getRefund_amount() {
        return refund_amount;
    }

    public void setRefund_amount(Long refund_amount) {
        this.refund_amount = refund_amount;
    }
    public String getRefund_reason() {
        return refund_reason;
    }

    public void setRefund_reason(String refund_reason) {
        this.refund_reason = refund_reason;
    }
    public String getRefund_ext() {
        return refund_ext;
    }

    public void setRefund_ext(String refund_ext) {
        this.refund_ext = refund_ext;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private String idempotent_id;
        private String out_order_id;
        private String order_id;
        private Integer refund_type;
        private List<Certificate> certificates;
        private Long refund_amount;
        private String refund_reason;
        private String refund_ext;

        public String getIdempotent_id() {
            return idempotent_id;
        }

        public void setIdempotent_id(String idempotent_id) {
            this.idempotent_id = idempotent_id;
        }

        public String getOut_order_id() {
            return out_order_id;
        }

        public void setOut_order_id(String out_order_id) {
            this.out_order_id = out_order_id;
        }

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public Integer getRefund_type() {
            return refund_type;
        }

        public void setRefund_type(Integer refund_type) {
            this.refund_type = refund_type;
        }

        public List<Certificate> getCertificates() {
            return certificates;
        }

        public void setCertificates(List<Certificate> certificates) {
            this.certificates = certificates;
        }

        public Long getRefund_amount() {
            return refund_amount;
        }

        public void setRefund_amount(Long refund_amount) {
            this.refund_amount = refund_amount;
        }

        public String getRefund_reason() {
            return refund_reason;
        }

        public void setRefund_reason(String refund_reason) {
            this.refund_reason = refund_reason;
        }

        public String getRefund_ext() {
            return refund_ext;
        }

        public void setRefund_ext(String refund_ext) {
            this.refund_ext = refund_ext;
        }

    }

    @Override
    public String getApiMethodName() {
        return "goodlife.v1.fulfilment.refund.create";
    }

    @Override
    public Class<GoodlifeV1FulfilmentRefundCreateResponse> getResponseClass() {
        return GoodlifeV1FulfilmentRefundCreateResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.POST;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/goodlife/v1/fulfilment/refund/create";
    }
}
