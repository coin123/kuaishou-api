package com.kuaishou.locallife.open.api.request.integration_doc;

import java.util.HashMap;
import java.util.Map;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.response.integration_doc.IntegrationOrderCreateConfirmDocResponse;

/**
 * auto generate code
 */
public class IntegrationOrderCreateConfirmDocRequest extends AbstractKsLocalLifeRequest<IntegrationOrderCreateConfirmDocResponse> {



    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {

    }

    @Override
    public String getApiMethodName() {
        return "integration.order.create.confirm.doc";
    }

    @Override
    public Class<IntegrationOrderCreateConfirmDocResponse> getResponseClass() {
        return IntegrationOrderCreateConfirmDocResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.GET;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/integration/order/create/confirm/doc";
    }
}
