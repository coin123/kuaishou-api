package com.kuaishou.locallife.open.api.request.locallife_order;

import java.util.HashMap;
import java.util.Map;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.response.locallife_order.GoodlifeV1TradeOrderQueryResponse;

/**
 * auto generate code
 */
public class GoodlifeV1TradeOrderQueryRequest extends AbstractKsLocalLifeRequest<GoodlifeV1TradeOrderQueryResponse> {

    private Integer page_size;
    private Integer page_num;
    private String order_id;
    private String ext_order_id;
    private String open_id;
    private String account_id;
    private Integer order_status;
    private Long create_order_start_time;
    private Long create_order_end_time;
    private Long update_order_start_time;
    private Long update_order_end_time;

    public Integer getPage_size() {
        return page_size;
    }

    public void setPage_size(Integer page_size) {
        this.page_size = page_size;
    }
    public Integer getPage_num() {
        return page_num;
    }

    public void setPage_num(Integer page_num) {
        this.page_num = page_num;
    }
    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }
    public String getExt_order_id() {
        return ext_order_id;
    }

    public void setExt_order_id(String ext_order_id) {
        this.ext_order_id = ext_order_id;
    }
    public String getOpen_id() {
        return open_id;
    }

    public void setOpen_id(String open_id) {
        this.open_id = open_id;
    }
    public String getAccount_id() {
        return account_id;
    }

    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }
    public Integer getOrder_status() {
        return order_status;
    }

    public void setOrder_status(Integer order_status) {
        this.order_status = order_status;
    }
    public Long getCreate_order_start_time() {
        return create_order_start_time;
    }

    public void setCreate_order_start_time(Long create_order_start_time) {
        this.create_order_start_time = create_order_start_time;
    }
    public Long getCreate_order_end_time() {
        return create_order_end_time;
    }

    public void setCreate_order_end_time(Long create_order_end_time) {
        this.create_order_end_time = create_order_end_time;
    }
    public Long getUpdate_order_start_time() {
        return update_order_start_time;
    }

    public void setUpdate_order_start_time(Long update_order_start_time) {
        this.update_order_start_time = update_order_start_time;
    }
    public Long getUpdate_order_end_time() {
        return update_order_end_time;
    }

    public void setUpdate_order_end_time(Long update_order_end_time) {
        this.update_order_end_time = update_order_end_time;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        
        bizParams.put("page_size", this.getPage_size() == null ? "" : this.getPage_size().toString());
        
        bizParams.put("page_num", this.getPage_num() == null ? "" : this.getPage_num().toString());
        bizParams.put("order_id", this.getOrder_id());
        
        bizParams.put("ext_order_id", this.getExt_order_id());
        
        bizParams.put("open_id", this.getOpen_id());
        
        bizParams.put("account_id", this.getAccount_id());
        
        
        bizParams.put("order_status", this.getOrder_status() == null ? "" : this.getOrder_status().toString());
        
        bizParams.put("create_order_start_time", this.getCreate_order_start_time() == null ? "" : this.getCreate_order_start_time().toString());
        
        bizParams.put("create_order_end_time", this.getCreate_order_end_time() == null ? "" : this.getCreate_order_end_time().toString());
        
        bizParams.put("update_order_start_time", this.getUpdate_order_start_time() == null ? "" : this.getUpdate_order_start_time().toString());
        
        bizParams.put("update_order_end_time", this.getUpdate_order_end_time() == null ? "" : this.getUpdate_order_end_time().toString());
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private Integer page_size;
        private Integer page_num;
        private String order_id;
        private String ext_order_id;
        private String open_id;
        private String account_id;
        private Integer order_status;
        private Long create_order_start_time;
        private Long create_order_end_time;
        private Long update_order_start_time;
        private Long update_order_end_time;

        public Integer getPage_size() {
            return page_size;
        }

        public void setPage_size(Integer page_size) {
            this.page_size = page_size;
        }

        public Integer getPage_num() {
            return page_num;
        }

        public void setPage_num(Integer page_num) {
            this.page_num = page_num;
        }

        public String getOrder_id() {
            return order_id;
        }

        public void setOrder_id(String order_id) {
            this.order_id = order_id;
        }

        public String getExt_order_id() {
            return ext_order_id;
        }

        public void setExt_order_id(String ext_order_id) {
            this.ext_order_id = ext_order_id;
        }

        public String getOpen_id() {
            return open_id;
        }

        public void setOpen_id(String open_id) {
            this.open_id = open_id;
        }

        public String getAccount_id() {
            return account_id;
        }

        public void setAccount_id(String account_id) {
            this.account_id = account_id;
        }

        public Integer getOrder_status() {
            return order_status;
        }

        public void setOrder_status(Integer order_status) {
            this.order_status = order_status;
        }

        public Long getCreate_order_start_time() {
            return create_order_start_time;
        }

        public void setCreate_order_start_time(Long create_order_start_time) {
            this.create_order_start_time = create_order_start_time;
        }

        public Long getCreate_order_end_time() {
            return create_order_end_time;
        }

        public void setCreate_order_end_time(Long create_order_end_time) {
            this.create_order_end_time = create_order_end_time;
        }

        public Long getUpdate_order_start_time() {
            return update_order_start_time;
        }

        public void setUpdate_order_start_time(Long update_order_start_time) {
            this.update_order_start_time = update_order_start_time;
        }

        public Long getUpdate_order_end_time() {
            return update_order_end_time;
        }

        public void setUpdate_order_end_time(Long update_order_end_time) {
            this.update_order_end_time = update_order_end_time;
        }

    }

    @Override
    public String getApiMethodName() {
        return "goodlife.v1.trade.order.query";
    }

    @Override
    public Class<GoodlifeV1TradeOrderQueryResponse> getResponseClass() {
        return GoodlifeV1TradeOrderQueryResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.GET;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/goodlife/v1/trade/order/query";
    }
}
