package com.kuaishou.locallife.open.api.response.locallife_shop;


import com.kuaishou.locallife.open.api.domain.locallife_shop.ExternalPoiMatchResData;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class GoodlifeV1PoiMatchTaskSubmitResponse extends KsLocalLifeResponse {

    private ExternalPoiMatchResData data;

    public ExternalPoiMatchResData getData() {
        return data;
    }

    public void setData(ExternalPoiMatchResData data) {
        this.data = data;
    }

}