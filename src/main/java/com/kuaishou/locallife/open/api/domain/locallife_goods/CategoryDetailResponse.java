package com.kuaishou.locallife.open.api.domain.locallife_goods;

import java.util.List;

/**
 * auto generate code
 */
public class CategoryDetailResponse {
    private Long error_code;
    private String description;
    private List<CategoryDetailBO> item_list;
    private Long next_cursor;
    private Boolean has_more;

    public Long getError_code() {
        return error_code;
    }

    public void setError_code(Long error_code) {
        this.error_code = error_code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<CategoryDetailBO> getItem_list() {
        return item_list;
    }

    public void setItem_list(List<CategoryDetailBO> item_list) {
        this.item_list = item_list;
    }

    public Long getNext_cursor() {
        return next_cursor;
    }

    public void setNext_cursor(Long next_cursor) {
        this.next_cursor = next_cursor;
    }

    public Boolean getHas_more() {
        return has_more;
    }

    public void setHas_more(Boolean has_more) {
        this.has_more = has_more;
    }

}
