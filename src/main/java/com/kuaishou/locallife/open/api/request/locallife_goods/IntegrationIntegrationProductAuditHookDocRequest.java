package com.kuaishou.locallife.open.api.request.locallife_goods;

import java.util.HashMap;
import java.util.Map;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.response.locallife_goods.IntegrationIntegrationProductAuditHookDocResponse;

/**
 * auto generate code
 */
public class IntegrationIntegrationProductAuditHookDocRequest extends AbstractKsLocalLifeRequest<IntegrationIntegrationProductAuditHookDocResponse> {

    private String product_id;
    private String status;
    private String reason;
    private String out_id;
    private Long account_id;

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
    public String getOut_id() {
        return out_id;
    }

    public void setOut_id(String out_id) {
        this.out_id = out_id;
    }
    public Long getAccount_id() {
        return account_id;
    }

    public void setAccount_id(Long account_id) {
        this.account_id = account_id;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private String product_id;
        private String status;
        private String reason;
        private String out_id;
        private Long account_id;

        public String getProduct_id() {
            return product_id;
        }

        public void setProduct_id(String product_id) {
            this.product_id = product_id;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getReason() {
            return reason;
        }

        public void setReason(String reason) {
            this.reason = reason;
        }

        public String getOut_id() {
            return out_id;
        }

        public void setOut_id(String out_id) {
            this.out_id = out_id;
        }

        public Long getAccount_id() {
            return account_id;
        }

        public void setAccount_id(Long account_id) {
            this.account_id = account_id;
        }

    }

    @Override
    public String getApiMethodName() {
        return "integration.integration.product.audit.hook.doc";
    }

    @Override
    public Class<IntegrationIntegrationProductAuditHookDocResponse> getResponseClass() {
        return IntegrationIntegrationProductAuditHookDocResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.POST;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/integration/integration/product/audit/hook/doc";
    }
}
