package com.kuaishou.locallife.open.api.response.locallife_shop;


import com.kuaishou.locallife.open.api.domain.locallife_shop.MatchRelationResData;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class GoodlifeV1PoiMatchRelationQueryResponse extends KsLocalLifeResponse {

    private MatchRelationResData data;

    public MatchRelationResData getData() {
        return data;
    }

    public void setData(MatchRelationResData data) {
        this.data = data;
    }

}