package com.kuaishou.locallife.open.api.domain.locallife_order;


/**
 * auto generate code
 */
public class OpenApiOrderInfo {
    private String order_id;
    private String sku_id;
    private String open_id;
    private String sku_name;
    private String third_sku_id;
    private Integer count;
    private CertificateInfos[] certificate;
    private String poi_id;
    private Integer order_type;
    private Integer order_status;
    private Integer original_amount;
    private Integer pay_amount;
    private Integer payment_discount;
    private Long create_order_time;
    private Long pay_time;
    private Long update_order_time;
    private Contacts[] contacts;
    private Long product_id;
    private String out_id;
    private String out_sku_id;
    private String video_id;
    private String encrypt_video_id;
    private String room_id;
    private String author_id;
    private Integer platform_ticket_amount;

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getSku_id() {
        return sku_id;
    }

    public void setSku_id(String sku_id) {
        this.sku_id = sku_id;
    }

    public String getOpen_id() {
        return open_id;
    }

    public void setOpen_id(String open_id) {
        this.open_id = open_id;
    }

    public String getSku_name() {
        return sku_name;
    }

    public void setSku_name(String sku_name) {
        this.sku_name = sku_name;
    }

    public String getThird_sku_id() {
        return third_sku_id;
    }

    public void setThird_sku_id(String third_sku_id) {
        this.third_sku_id = third_sku_id;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public CertificateInfos[] getCertificate() {
        return certificate;
    }

    public void setCertificate(CertificateInfos[] certificate) {
        this.certificate = certificate;
    }

    public String getPoi_id() {
        return poi_id;
    }

    public void setPoi_id(String poi_id) {
        this.poi_id = poi_id;
    }

    public Integer getOrder_type() {
        return order_type;
    }

    public void setOrder_type(Integer order_type) {
        this.order_type = order_type;
    }

    public Integer getOrder_status() {
        return order_status;
    }

    public void setOrder_status(Integer order_status) {
        this.order_status = order_status;
    }

    public Integer getOriginal_amount() {
        return original_amount;
    }

    public void setOriginal_amount(Integer original_amount) {
        this.original_amount = original_amount;
    }

    public Integer getPay_amount() {
        return pay_amount;
    }

    public void setPay_amount(Integer pay_amount) {
        this.pay_amount = pay_amount;
    }

    public Integer getPayment_discount() {
        return payment_discount;
    }

    public void setPayment_discount(Integer payment_discount) {
        this.payment_discount = payment_discount;
    }

    public Long getCreate_order_time() {
        return create_order_time;
    }

    public void setCreate_order_time(Long create_order_time) {
        this.create_order_time = create_order_time;
    }

    public Long getPay_time() {
        return pay_time;
    }

    public void setPay_time(Long pay_time) {
        this.pay_time = pay_time;
    }

    public Long getUpdate_order_time() {
        return update_order_time;
    }

    public void setUpdate_order_time(Long update_order_time) {
        this.update_order_time = update_order_time;
    }

    public Contacts[] getContacts() {
        return contacts;
    }

    public void setContacts(Contacts[] contacts) {
        this.contacts = contacts;
    }

    public Long getProduct_id() {
        return product_id;
    }

    public void setProduct_id(Long product_id) {
        this.product_id = product_id;
    }

    public String getOut_id() {
        return out_id;
    }

    public void setOut_id(String out_id) {
        this.out_id = out_id;
    }

    public String getOut_sku_id() {
        return out_sku_id;
    }

    public void setOut_sku_id(String out_sku_id) {
        this.out_sku_id = out_sku_id;
    }

    public String getVideo_id() {
        return video_id;
    }

    public void setVideo_id(String video_id) {
        this.video_id = video_id;
    }

    public String getEncrypt_video_id() {
        return encrypt_video_id;
    }

    public void setEncrypt_video_id(String encrypt_video_id) {
        this.encrypt_video_id = encrypt_video_id;
    }

    public String getRoom_id() {
        return room_id;
    }

    public void setRoom_id(String room_id) {
        this.room_id = room_id;
    }

    public String getAuthor_id() {
        return author_id;
    }

    public void setAuthor_id(String author_id) {
        this.author_id = author_id;
    }

    public Integer getPlatform_ticket_amount() {
        return platform_ticket_amount;
    }

    public void setPlatform_ticket_amount(Integer platform_ticket_amount) {
        this.platform_ticket_amount = platform_ticket_amount;
    }

}
