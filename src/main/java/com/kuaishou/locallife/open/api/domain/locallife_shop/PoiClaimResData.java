package com.kuaishou.locallife.open.api.domain.locallife_shop;

import java.util.List;

/**
 * auto generate code
 */
public class PoiClaimResData {
    private String description;
    private Integer error_code;
    private List<ClaimTask> tasks;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getError_code() {
        return error_code;
    }

    public void setError_code(Integer error_code) {
        this.error_code = error_code;
    }

    public List<ClaimTask> getTasks() {
        return tasks;
    }

    public void setTasks(List<ClaimTask> tasks) {
        this.tasks = tasks;
    }

}
