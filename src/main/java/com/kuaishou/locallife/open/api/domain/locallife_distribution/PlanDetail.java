package com.kuaishou.locallife.open.api.domain.locallife_distribution;

import java.util.List;

/**
 * auto generate code
 */
public class PlanDetail {
    private Long plan_id;
    private Long item_id;
    private Double distributor_rate;
    private String item_title;
    private Integer status;
    private List<User> distributors;
    private String outer_item_id;

    public Long getPlan_id() {
        return plan_id;
    }

    public void setPlan_id(Long plan_id) {
        this.plan_id = plan_id;
    }

    public Long getItem_id() {
        return item_id;
    }

    public void setItem_id(Long item_id) {
        this.item_id = item_id;
    }

    public Double getDistributor_rate() {
        return distributor_rate;
    }

    public void setDistributor_rate(Double distributor_rate) {
        this.distributor_rate = distributor_rate;
    }

    public String getItem_title() {
        return item_title;
    }

    public void setItem_title(String item_title) {
        this.item_title = item_title;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<User> getDistributors() {
        return distributors;
    }

    public void setDistributors(List<User> distributors) {
        this.distributors = distributors;
    }

    public String getOuter_item_id() {
        return outer_item_id;
    }

    public void setOuter_item_id(String outer_item_id) {
        this.outer_item_id = outer_item_id;
    }

}
