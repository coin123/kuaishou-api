package com.kuaishou.locallife.open.api.request.locallife_trade;

import java.util.HashMap;
import java.util.Map;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.response.locallife_trade.NamekFulfilmentPrepareResponse;

/**
 * auto generate code
 */
public class NamekFulfilmentPrepareRequest extends AbstractKsLocalLifeRequest<NamekFulfilmentPrepareResponse> {

    private String encrypted_data;
    private String code;

    public String getEncrypted_data() {
        return encrypted_data;
    }

    public void setEncrypted_data(String encrypted_data) {
        this.encrypted_data = encrypted_data;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        bizParams.put("encrypted_data", this.getEncrypted_data());
        
        bizParams.put("code", this.getCode());
        
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private String encrypted_data;
        private String code;

        public String getEncrypted_data() {
            return encrypted_data;
        }

        public void setEncrypted_data(String encrypted_data) {
            this.encrypted_data = encrypted_data;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

    }

    @Override
    public String getApiMethodName() {
        return "namek.fulfilment.prepare";
    }

    @Override
    public Class<NamekFulfilmentPrepareResponse> getResponseClass() {
        return NamekFulfilmentPrepareResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.GET;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/namek/fulfilment/prepare";
    }
}
