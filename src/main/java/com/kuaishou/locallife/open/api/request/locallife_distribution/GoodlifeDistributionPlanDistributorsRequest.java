package com.kuaishou.locallife.open.api.request.locallife_distribution;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.response.locallife_distribution.GoodlifeDistributionPlanDistributorsResponse;

/**
 * auto generate code
 */
public class GoodlifeDistributionPlanDistributorsRequest extends AbstractKsLocalLifeRequest<GoodlifeDistributionPlanDistributorsResponse> {

    private Long plan_id;
    private Integer operate_type;
    private List<Long> distributors;

    public Long getPlan_id() {
        return plan_id;
    }

    public void setPlan_id(Long plan_id) {
        this.plan_id = plan_id;
    }
    public Integer getOperate_type() {
        return operate_type;
    }

    public void setOperate_type(Integer operate_type) {
        this.operate_type = operate_type;
    }
    public List<Long> getDistributors() {
        return distributors;
    }

    public void setDistributors(List<Long> distributors) {
        this.distributors = distributors;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private Long plan_id;
        private Integer operate_type;
        private List<Long> distributors;

        public Long getPlan_id() {
            return plan_id;
        }

        public void setPlan_id(Long plan_id) {
            this.plan_id = plan_id;
        }

        public Integer getOperate_type() {
            return operate_type;
        }

        public void setOperate_type(Integer operate_type) {
            this.operate_type = operate_type;
        }

        public List<Long> getDistributors() {
            return distributors;
        }

        public void setDistributors(List<Long> distributors) {
            this.distributors = distributors;
        }

    }

    @Override
    public String getApiMethodName() {
        return "goodlife.distribution.plan.distributors";
    }

    @Override
    public Class<GoodlifeDistributionPlanDistributorsResponse> getResponseClass() {
        return GoodlifeDistributionPlanDistributorsResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.POST;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/goodlife/distribution/plan/distributors";
    }
}
