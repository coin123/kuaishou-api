package com.kuaishou.locallife.open.api.response.locallife_marketing;


import com.kuaishou.locallife.open.api.domain.locallife_marketing.MeituanOrderQuoteResp;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class IntegrationMtOrderQuoteQueryResponse extends KsLocalLifeResponse {

    private MeituanOrderQuoteResp data;

    public MeituanOrderQuoteResp getData() {
        return data;
    }

    public void setData(MeituanOrderQuoteResp data) {
        this.data = data;
    }

}