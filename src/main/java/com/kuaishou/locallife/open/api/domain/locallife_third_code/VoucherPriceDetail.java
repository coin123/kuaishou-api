package com.kuaishou.locallife.open.api.domain.locallife_third_code;


/**
 * auto generate code
 */
public class VoucherPriceDetail {
    private Long original_price;
    private Long sell_price;

    public Long getOriginal_price() {
        return original_price;
    }

    public void setOriginal_price(Long original_price) {
        this.original_price = original_price;
    }

    public Long getSell_price() {
        return sell_price;
    }

    public void setSell_price(Long sell_price) {
        this.sell_price = sell_price;
    }

}
