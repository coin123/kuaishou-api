package com.kuaishou.locallife.open.api.response.locallife_third_code;


import com.kuaishou.locallife.open.api.domain.locallife_third_code.RefundResData;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class IntegrationIntegrationV1RefundApplyDocResponse extends KsLocalLifeResponse {

    private RefundResData data;

    public RefundResData getData() {
        return data;
    }

    public void setData(RefundResData data) {
        this.data = data;
    }

}