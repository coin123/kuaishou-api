package com.kuaishou.locallife.open.api.response.openapi;


import com.kuaishou.locallife.open.api.domain.openapi.ItemConsistencyResponse;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class IntegrationItemConsistencyQueryResponse extends KsLocalLifeResponse {

    private ItemConsistencyResponse data;

    public ItemConsistencyResponse getData() {
        return data;
    }

    public void setData(ItemConsistencyResponse data) {
        this.data = data;
    }

}