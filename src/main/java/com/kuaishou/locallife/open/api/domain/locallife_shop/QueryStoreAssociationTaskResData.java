package com.kuaishou.locallife.open.api.domain.locallife_shop;

import java.util.List;

/**
 * auto generate code
 */
public class QueryStoreAssociationTaskResData {
    private Integer association_task_status;
    private List<AssociationResult> data;
    private String description;
    private Integer error_code;
    private Long task_id;

    public Integer getAssociation_task_status() {
        return association_task_status;
    }

    public void setAssociation_task_status(Integer association_task_status) {
        this.association_task_status = association_task_status;
    }

    public List<AssociationResult> getData() {
        return data;
    }

    public void setData(List<AssociationResult> data) {
        this.data = data;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getError_code() {
        return error_code;
    }

    public void setError_code(Integer error_code) {
        this.error_code = error_code;
    }

    public Long getTask_id() {
        return task_id;
    }

    public void setTask_id(Long task_id) {
        this.task_id = task_id;
    }

}
