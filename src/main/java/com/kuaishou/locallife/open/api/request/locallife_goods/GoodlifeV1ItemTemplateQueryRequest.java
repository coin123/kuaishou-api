package com.kuaishou.locallife.open.api.request.locallife_goods;

import java.util.HashMap;
import java.util.Map;
import com.kuaishou.locallife.open.api.AbstractKsLocalLifeRequest;
import com.kuaishou.locallife.open.api.common.HttpRequestMethod;
import com.kuaishou.locallife.open.api.common.utils.GsonUtils;
import com.kuaishou.locallife.open.api.response.locallife_goods.GoodlifeV1ItemTemplateQueryResponse;

/**
 * auto generate code
 */
public class GoodlifeV1ItemTemplateQueryRequest extends AbstractKsLocalLifeRequest<GoodlifeV1ItemTemplateQueryResponse> {

    private Long category_leaf_id;
    private Integer product_type;

    public Long getCategory_leaf_id() {
        return category_leaf_id;
    }

    public void setCategory_leaf_id(Long category_leaf_id) {
        this.category_leaf_id = category_leaf_id;
    }
    public Integer getProduct_type() {
        return product_type;
    }

    public void setProduct_type(Integer product_type) {
        this.product_type = product_type;
    }

    /**
     * 用于GET请求
     */
    @Override
    public Map<String, String> getBusinessParams() {
        Map<String, String> bizParams = new HashMap<String, String>();
        
        bizParams.put("category_leaf_id", this.getCategory_leaf_id() == null ? "" : this.getCategory_leaf_id().toString());
        
        bizParams.put("product_type", this.getProduct_type() == null ? "" : this.getProduct_type().toString());
        return bizParams;
    }

    /**
     * 生成请求Json串,用于post请求
     */
    @Override
    public String generateObjJson() {
        return GsonUtils.toJSON(this);
    }

    public static class ParamDTO {
        private Long category_leaf_id;
        private Integer product_type;

        public Long getCategory_leaf_id() {
            return category_leaf_id;
        }

        public void setCategory_leaf_id(Long category_leaf_id) {
            this.category_leaf_id = category_leaf_id;
        }

        public Integer getProduct_type() {
            return product_type;
        }

        public void setProduct_type(Integer product_type) {
            this.product_type = product_type;
        }

    }

    @Override
    public String getApiMethodName() {
        return "goodlife.v1.item.template.query";
    }

    @Override
    public Class<GoodlifeV1ItemTemplateQueryResponse> getResponseClass() {
        return GoodlifeV1ItemTemplateQueryResponse.class;
    }

    @Override
    public HttpRequestMethod getHttpRequestMethod() {
        return HttpRequestMethod.GET;
    }

    @Override
    public String getRequestSpecialPath() {
        return "/goodlife/v1/item/template/query";
    }
}
