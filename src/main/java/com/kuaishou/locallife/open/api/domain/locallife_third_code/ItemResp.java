package com.kuaishou.locallife.open.api.domain.locallife_third_code;

import java.util.List;

/**
 * auto generate code
 */
public class ItemResp {
    private Long product_Id;
    private String out_id;
    private Boolean on_sale;
    private List<SkuResp> sku_list;

    public Long getProduct_Id() {
        return product_Id;
    }

    public void setProduct_Id(Long product_Id) {
        this.product_Id = product_Id;
    }

    public String getOut_id() {
        return out_id;
    }

    public void setOut_id(String out_id) {
        this.out_id = out_id;
    }

    public Boolean getOn_sale() {
        return on_sale;
    }

    public void setOn_sale(Boolean on_sale) {
        this.on_sale = on_sale;
    }

    public List<SkuResp> getSku_list() {
        return sku_list;
    }

    public void setSku_list(List<SkuResp> sku_list) {
        this.sku_list = sku_list;
    }

}
