package com.kuaishou.locallife.open.api.domain.locallife_distribution;

import java.util.List;

/**
 * auto generate code
 */
public class DistributorsData {
    private Integer error_code;
    private String description;
    private List<User> distributors;

    public Integer getError_code() {
        return error_code;
    }

    public void setError_code(Integer error_code) {
        this.error_code = error_code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<User> getDistributors() {
        return distributors;
    }

    public void setDistributors(List<User> distributors) {
        this.distributors = distributors;
    }

}
