package com.kuaishou.locallife.open.api.response.locallife_distribution;


import com.kuaishou.locallife.open.api.domain.locallife_distribution.PlanDetailData;
import com.kuaishou.locallife.open.api.KsLocalLifeResponse;

/**
 * auto generate code
 */
public class GoodlifeDistributionPlanQueryResponse extends KsLocalLifeResponse {

    private PlanDetailData data;

    public PlanDetailData getData() {
        return data;
    }

    public void setData(PlanDetailData data) {
        this.data = data;
    }

}