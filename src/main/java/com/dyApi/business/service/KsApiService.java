package com.dyApi.business.service;

import com.dyApi.business.service.dto.OrderConfirmInDto;
import com.dyApi.business.service.dto.SetCouponInDto;
import com.kuaishou.locallife.open.api.domain.locallife_third_code.RefundAuditRequestData;
import com.kuaishou.locallife.open.api.request.locallife_order.GoodlifeV1TradeOrderQueryRequest;
import com.kuaishou.locallife.open.api.request.locallife_trade.GoodlifeV1FulfilmentCertificateVerifyRequest;
import com.kuaishou.locallife.open.api.response.locallife_order.GoodlifeV1TradeOrderQueryResponse;

import java.util.Map;

public interface KsApiService {
    Map<String, Object> poi();

    Map<String, Object> orderConfirm(OrderConfirmInDto inDto);

    Map<String, Object> setCoupon(SetCouponInDto inDto);

    Map<String, Object> refund(RefundAuditRequestData inDto);

    GoodlifeV1TradeOrderQueryResponse order(GoodlifeV1TradeOrderQueryRequest inDto);
    Map<String, Object> verify(GoodlifeV1FulfilmentCertificateVerifyRequest inDto);

}
