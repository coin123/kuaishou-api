package com.dyApi.business.service.dto;


import com.kuaishou.locallife.open.api.domain.locallife_third_code.Contact;
import com.kuaishou.locallife.open.api.domain.locallife_third_code.Sku;
import com.kuaishou.locallife.open.api.domain.locallife_third_code.Tourist;
import lombok.Data;

import java.util.List;

@Data
public class SetCouponInDto {

    private Long order_id;

    private Integer count;

    private Integer start_time;

    private Integer expire_time;

    private String sku_id;

    private String third_sku_id;

    private Sku sku;

    private Amount amount;

    private List<Tourist> tourists;

    private Long account_id;

    private Contact contact;

}
