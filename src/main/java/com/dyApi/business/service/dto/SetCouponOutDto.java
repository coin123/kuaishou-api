package com.dyApi.business.service.dto;

import com.kuaishou.locallife.open.api.domain.locallife_third_code.CodeInfo;
import lombok.Data;

import java.util.List;

@Data
public class SetCouponOutDto extends com.kuaishou.locallife.open.api.domain.locallife_third_code.Data {

    private List<String> codes;

    private List<CodeInfo> codes_list;

}
