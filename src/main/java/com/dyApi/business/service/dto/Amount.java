package com.dyApi.business.service.dto;

import lombok.Data;

@Data
public class Amount {

    private Long original_amount;

    private Long pay_amount;

    private Long ticket_amount;

    private Long merchant_ticket_amount;

    private Long fee_amount;

    private Long commission_amount;

    private Long payment_discount_amount;

    private Long coupon_pay_amount;

    private Long order_discount_amount;

}
