package com.dyApi.business.service.dto;

import com.kuaishou.locallife.open.api.domain.locallife_third_code.Contact;
import com.kuaishou.locallife.open.api.domain.locallife_third_code.Tourist;
import lombok.Data;

import java.util.List;

@Data
public class OrderConfirmInDto {

    private Integer count;

    private String sku_id;

    private String third_sku_id;

    private Long product_id;

    private String out_id;

    private String out_sku_id;

    private Amount amount;

    private Long account_id;

    private String idempotent_id;

    private Contact contact;

    private List<Tourist> tourists;

}
