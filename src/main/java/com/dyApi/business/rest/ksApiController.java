package com.dyApi.business.rest;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.dyApi.annotation.Log;
import com.dyApi.business.service.KsApiService;
import com.dyApi.business.service.dto.OrderConfirmInDto;
import com.dyApi.business.service.dto.SetCouponInDto;
import com.dyApi.utils.RedisUtils;
import com.kuaishou.locallife.open.api.KsLocalLifeApiException;
import com.kuaishou.locallife.open.api.client.oauth.OAuthAccessTokenKsClient;
import com.kuaishou.locallife.open.api.domain.locallife_third_code.RefundAuditRequestData;
import com.kuaishou.locallife.open.api.request.locallife_order.GoodlifeV1TradeOrderQueryRequest;
import com.kuaishou.locallife.open.api.request.locallife_trade.GoodlifeV1FulfilmentCertificateVerifyRequest;
import com.kuaishou.locallife.open.api.response.oauth.KsAccessTokenPreviousVersionResponse;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.TimeUnit;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/ks")
public class ksApiController {
    @Value("${ks-api.appKey}")
    private String appKey;

    @Value("${ks-api.appSecret}")
    private String appSecret;
    @Autowired
    private KsApiService apiService;
    @Autowired
    private RedisUtils redisUtils;
    @Log("Code回调接口")
    @GetMapping("/getCode")
    @ResponseBody
    public ResponseEntity<Object> poi(String code, String state) {
        // TODO
        System.out.println(code);
        String  accessToken  = null;
//        System.out.println(state);
        OAuthAccessTokenKsClient client = new OAuthAccessTokenKsClient(appKey, appSecret);
        try {
            KsAccessTokenPreviousVersionResponse response = client.getAccessToken(code);
            System.out.println("response===="+response);
            if(response.getResult() == 1) {
                accessToken = response.getAccessToken();
                String refreshToken = response.getRefreshToken();
                redisUtils.set("ACCESS_TOKEN", accessToken, response.getExpiresIn(), TimeUnit.SECONDS);
                redisUtils.set("REFRESH_TOKEN", refreshToken, response.getRefreshTokenExpiresIn(), TimeUnit.SECONDS);
            }
        } catch (KsLocalLifeApiException e) {
            e.printStackTrace();
        }
        System.out.println("accessToken=="+accessToken);
        return new ResponseEntity<>(HttpStatus.OK);
    }


    /**
     * 商家门店（poi）信息查询
     */
    @Log("商家门店（poi）信息查询接口")
    @PostMapping("/poi")
    @ResponseBody
    public ResponseEntity<Object> poi() {
        return new ResponseEntity<>(apiService.poi(), HttpStatus.OK);
    }

    /**
     * 订单查询查询
     */
    @Log("订单查询接口")
    @PostMapping("/order")
    @ResponseBody
    public ResponseEntity<Object> order(@RequestBody GoodlifeV1TradeOrderQueryRequest inDto) {
        return new ResponseEntity<>(apiService.order(inDto), HttpStatus.OK);
    }


    /**
     * 下单确认接口
     */
    @Log("下单确认接口")
    @PostMapping("/orderConfirm")
    @ResponseBody
    public ResponseEntity<Object> orderConfirm(@RequestBody OrderConfirmInDto inDto) {
//        System.out.println("确认可以下单");
        return new ResponseEntity<>(apiService.orderConfirm(inDto), HttpStatus.OK);
    }

    /**
     * 发券接口
     */
    @Log("发券接口")
    @PostMapping("/setCoupon")
    @ResponseBody
    public ResponseEntity<Object> setCoupon(@RequestBody SetCouponInDto inDto) {
        return new ResponseEntity<>(apiService.setCoupon(inDto), HttpStatus.OK);
    }


    /**
     * 退款申请接口
     */
    @Log("退款申请接口")
    @PostMapping("/refund")
    @ResponseBody
    public ResponseEntity<Object> refund(@RequestBody RefundAuditRequestData inDto) {
        return new ResponseEntity<>(apiService.refund(inDto), HttpStatus.OK);
    }

    /**
     * 核销接口
     */
    @Log("核销接口")
    @PostMapping("/verify")
    public ResponseEntity<Object> verify(@RequestBody GoodlifeV1FulfilmentCertificateVerifyRequest inDto) {
        return new ResponseEntity<>(apiService.verify(inDto), HttpStatus.OK);
    }

}
